/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */

//
//  AppDelegate.m
//  Moodle Mobile
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright ___ORGANIZATIONNAME___ ___YEAR___. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"
#import "Reachability.h"
#import "igUtility.h"
#import  "IGAuthenticationVC.h"
#import "igTLoginViewController.h"
#import <FirebaseCore/FirebaseCore.h>
#import <FirebaseMessaging/FirebaseMessaging.h>
#import <FirebaseCore/FIRApp.h>
#import "Constants.h"
#import "igtRootViewController.h"
#import "igTTutorialRootViewController.h"
#import "igTermConditionVC.h"
#import "IGAnalytics.h"
#import "IGUserDefaults.h"

#ifndef NSFoundationVersionNumber_iOS_9_x_Max
#define NSFoundationVersionNumber_iOS_9_x_Max 1299
#endif

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@import UserNotifications;
#endif

@interface AppDelegate ()<UNUserNotificationCenterDelegate/*,FIRMessagingDelegate*/>

@property (nonatomic) Reachability *hostReachability;
@property (nonatomic) Reachability *internetReachability;

@end


@implementation AppDelegate

- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    /*self.viewController = [[MainViewController alloc] init];
    return [super application:application didFinishLaunchingWithOptions:launchOptions];*/
    
    self.isNetworkConnected = true;
    self.strCommentPayload = @"";
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    [self configureFireBase];
    [self registerForFireBaseRemoteNotification];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    //Change the host name here to change the server you want to monitor.
    NSString *remoteHostName = @"www.apple.com";
    
    
    
    
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    
    [self updateInterfaceWithReachability:self.internetReachability];
    [self.internetReachability startNotifier];
    [IGAnalytics initialize];
    
    if([IGUserDefaults sharedInstance].isUserLoggedIn)
    {
        UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        IGAuthenticationVC *rootVC = (IGAuthenticationVC*) [mainStoryBoard instantiateViewControllerWithIdentifier:@"IGAuthenticationViewController"];
        self.window.rootViewController = rootVC;
    }
    return YES;

    
//    return YES;
}

-(void)configureFireBase{
    [FIRApp configure];
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    if (deviceToken)
    {
        NSString *deviceTokenString = [NSString stringWithFormat:@"%@", deviceToken];
        deviceTokenString = [deviceTokenString stringByReplacingOccurrencesOfString:@"<" withString:@""];
        deviceTokenString = [deviceTokenString stringByReplacingOccurrencesOfString:@">" withString:@""];
        deviceTokenString = [deviceTokenString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSLog(@"Device Token: %@", deviceTokenString);
        
        //TODO: Implement in UserDefaults
        //[[IGUserDefaults sharedInstance] updateDeviceTokenOnServer];
    }
}


-(void)registerForFireBaseRemoteNotification{
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert
        | UNAuthorizationOptionSound
        | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
        
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        // For iOS 10 data message (sent via FCM)
     //   [FIRMessaging messaging].remoteMessageDelegate = self;
#endif
    }
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    // [END register_for_notifications]
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Error registering for Remote Notification: %@", [error localizedDescription]);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    //TODO: parse payload
    
    completionHandler(UIBackgroundFetchResultNewData);
}


/*!
 * Called by Reachability whenever status changes.
 */
- (void) reachabilityChanged:(NSNotification *)note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    [self updateInterfaceWithReachability:curReach];
}


- (void)updateInterfaceWithReachability:(Reachability *)reachability
{
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    switch (netStatus)
    {
        case NotReachable:        {
            self.isNetworkConnected = false;
            NSLog(@"disconnected");
            
            break;
        }
            
        case ReachableViaWWAN:        {
            self.isNetworkConnected = true;
            NSLog(@"connected");
            
            break;
        }
        case ReachableViaWiFi:        {
            self.isNetworkConnected = true;
            NSLog(@"connected");
            
            break;
        }
    }
    
}

- (BOOL)networkReachabilityAlert
{
    if (self.isNetworkConnected == false)
    {
        [igUtility showOKAlertWithTitle:@"Connection Error" message:@"Please check your network connection and try again"];
    }
    
    return self.isNetworkConnected;
}


-(void)openTermsAndCondition
{
    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    igTermConditionVC *tncVC = (igTermConditionVC*) [mainStoryBoard instantiateViewControllerWithIdentifier:@"igTermConditionVC"];
    [self animateAddRootViewController:tncVC completion:nil];
}

- (void)setupLoginControllerAsRootViewController
{
    UIStoryboard *storyboard = self.window.rootViewController.storyboard;
    
    igTLoginViewController *loginViewController = [storyboard instantiateViewControllerWithIdentifier:@"loginViewController"];
    [self animateAddRootViewController:loginViewController completion:nil];
    
    //self.window.rootViewController = loginViewController;
}

- (void)setupDashboardAsRootViewController
{
    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    igtRootViewController *rootVC = (igtRootViewController*) [mainStoryBoard instantiateViewControllerWithIdentifier:kRootViewController_Identifier];
    [self animateAddRootViewController:rootVC completion:nil];
    //self.window.rootViewController = rootVC;
}


- (void)setupTutorialViewController
{
    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    igTTutorialRootViewController *tncVC = (igTTutorialRootViewController*) [mainStoryBoard instantiateViewControllerWithIdentifier:@"GMTutorialRootViewController"];
    [self animateAddRootViewController:tncVC completion:nil];
    
}



- (void)animateAddRootViewController:(UIViewController *)viewController completion:(void(^)(BOOL finished))completion
{
    UIView *view = [self.window.rootViewController.view snapshotViewAfterScreenUpdates:NO];
    if(view != nil)
    {
        self.window.rootViewController = viewController;
        
        [self.window addSubview:view];
        
        [UIView transitionWithView:view
                          duration:0.3
                           options:UIViewAnimationOptionShowHideTransitionViews
                        animations:^{
                            view.alpha = 0.0f;
                        }
                        completion:^(BOOL finished) {
                            
                            
                            [view removeFromSuperview];
                            if (completion)
                            {
                                completion(finished);
                            }
                        }];
    }
    
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end

//view-title="{{ 'mm.login.connecttomoodle' | translate }}"
