//
//  igTTrainingModel.h
//  igTraining
//
//  Created by Niharika on 19/01/15.
//  Copyright (c) 2015 Infogain. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface igTTrainingFeedbackListModel : NSObject

@property (nonatomic, strong) NSString *trainingID;
@property (nonatomic, strong) NSString *startDate;
@property (nonatomic, strong) NSString *endDate;
@property (nonatomic, strong) NSString *trainer;
@property (nonatomic, strong) NSString *topic;
@property (nonatomic, strong) NSString *trainerID;

@end

