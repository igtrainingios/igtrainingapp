//
//  igTFeedbackRatingInfo.h
//  igTraining
//
//  Created by Sandeep on 02/01/17.
//  Copyright © 2017 Infogain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface igTFeedbackRatingInfo : NSObject

@property (nonatomic, strong) NSString *mappingID;
@property (nonatomic, strong) NSString *categoryID;
@property (nonatomic, strong) NSString *subCategoryID;
@property (nonatomic, strong) NSString *feedbackType;
@property (nonatomic, strong) NSString *feedbackSubType;
@property (nonatomic, strong) NSString *flag;
@property (nonatomic, strong) NSString *trainerID;
@property (nonatomic, strong) NSString *trainerName;

@end

