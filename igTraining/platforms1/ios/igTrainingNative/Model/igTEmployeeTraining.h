//
//  igTEmployeeTraining.h
//  igTraining
//
//  Created by Niharika on 09/03/15.
//  Copyright (c) 2015 Infogain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface igTEmployeeTraining : NSObject

@property (nonatomic, strong) NSString *sequenceId;
@property (nonatomic, strong) NSString *trainingID;
@property (nonatomic, strong) NSString *topic;
@property(assign) bool isNominated;
@property (nonatomic, strong) NSString *trainingMode;
@property (nonatomic, strong) NSString *prerequisite;
@property (nonatomic, strong) NSString *trainingOccurance;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *endDate;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *venue;
@property (nonatomic, strong) NSString *trainerName;
@property(assign) bool isTrainingDateGone;
@property (nonatomic, strong) NSString *startDateTime;
@property (nonatomic, strong) NSString *endDateTime;
@property (nonatomic, strong) NSString *webminarLocation;
@property (nonatomic, strong) NSString *nominationStatus;



@end
