//
//  igTTrainingListParser.h
//  igTraining
//
//  Created by Niharika on 09/03/15.
//  Copyright (c) 2015 Infogain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "igTEmployeeTraining.h"
#import "igTTrainingFeedbackListModel.h"

@interface igTTrainingListParser : NSObject
@property(nonatomic,strong)NSMutableArray *employeeTraining;
-(NSArray*)parseTraining:(NSArray*)responseArray;
-(NSArray*)parseFeedbackTraining:(NSArray*)responseArray;
-(NSDictionary*)parseFeedbackRating:(NSDictionary*)responseDictionary;
-(NSMutableArray*)getFeedbackTemplate:(NSDictionary*)responseDictionary TemplateID:(NSString*)templateID;

@end
