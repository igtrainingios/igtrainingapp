//
//  igTTrainingListParser.m
//  igTraining
//
//  Created by Niharika on 09/03/15.
//  Copyright (c) 2015 Infogain. All rights reserved.
//

#import "igTTrainingListParser.h"
#import "Constants.h"
#import "igTFeedbackRatingInfo.h"



@implementation igTTrainingListParser

-(NSArray*)parseTraining:(NSArray*)responseArray{
    self.employeeTraining=[[NSMutableArray alloc]init];
    NSMutableArray *trainingArray   = [[NSMutableArray alloc]init];

    for (NSDictionary *dict in responseArray)
    {
        igTEmployeeTraining *empTraining=[[igTEmployeeTraining alloc]init];

        [[dict objectForKey:kTrainingID] isEqual:[NSNull null]] ?@"" :(empTraining.trainingID=[dict objectForKey:kTrainingID]);
        [[dict objectForKey:kTrainingTopic] isEqual:[NSNull null]] ?@"" :(empTraining.topic=[dict objectForKey:kTrainingTopic]);
       empTraining.isNominated=[[dict objectForKey:kNominate] boolValue];
        [[dict objectForKey:kTrainingMode] isEqual:[NSNull null]] ?@"" :(empTraining.trainingMode=[dict objectForKey:kTrainingMode]);
        [[dict objectForKey:kPrerequisite] isEqual:[NSNull null]] ?@"" :(empTraining.prerequisite=[dict objectForKey:kPrerequisite]);
        [[dict objectForKey:kTrainingOccurance] isEqual:[NSNull null]] ?@"" :(empTraining.trainingOccurance=[dict objectForKey:kTrainingOccurance]);
        [[dict objectForKey:kStartDate] isEqual:[NSNull null]] ?@"" :(empTraining.date=[dict objectForKey:kStartDate]);
        [[dict objectForKey:kEndDate] isEqual:[NSNull null]] ?@"" :(empTraining.endDate=[dict objectForKey:kEndDate]);
        [[dict objectForKey:kTrainingTime] isEqual:[NSNull null]] ?@"" :(empTraining.time=[dict objectForKey:kTrainingTime]);
        [[dict objectForKey:kTrainingVenue] isEqual:[NSNull null]] ?@"" :(empTraining.venue=[dict objectForKey:kTrainingVenue]);
        [[dict objectForKey:kTrainerName] isEqual:[NSNull null]] ?@"" :(empTraining.trainerName=[dict objectForKey:kTrainerName]);
        empTraining.isTrainingDateGone= [[dict objectForKey:kDateFlag]boolValue];
        [[dict objectForKey:kStartDateTime] isEqual:[NSNull null]] ?@"" :(empTraining.startDateTime=[dict objectForKey:kStartDateTime]);
        
         [[dict objectForKey:kEndDateTime] isEqual:[NSNull null]] ?@"" :(empTraining.endDateTime=[dict objectForKey:kEndDateTime]);
       
        [[dict objectForKey:kLocation] isEqual:[NSNull null]] ?@"" :(empTraining.webminarLocation =[dict objectForKey:kLocation]);
        
         [[dict objectForKey:kNominationStatus] isEqual:[NSNull null]] ?@"" :(empTraining.nominationStatus=[dict objectForKey:kNominationStatus]);
        [trainingArray addObject:empTraining];

    }
    
    
    return trainingArray;

}


-(NSArray*)parseFeedbackTraining:(NSArray*)responseArray{
    self.employeeTraining=[[NSMutableArray alloc]init];
    NSMutableArray *trainingArray   = [[NSMutableArray alloc]init];

    for (NSDictionary *dict in responseArray)
    {
        igTTrainingFeedbackListModel *empTraining=[[igTTrainingFeedbackListModel alloc]init];
        
        [[dict objectForKey:kFeedbackTrainingID] isEqual:[NSNull null]] ?@"" :(empTraining.trainingID=[dict objectForKey:kFeedbackTrainingID]);
        
        [[dict objectForKey:kFeedbackTrainingTopic] isEqual:[NSNull null]] ?@"" :(empTraining.topic=[dict objectForKey:kFeedbackTrainingTopic]);

        [[dict objectForKey:kFeedbackTrainingStartDate] isEqual:[NSNull null]] ?@"" :(empTraining.startDate=[dict objectForKey:kFeedbackTrainingStartDate]);
        
        [[dict objectForKey:kFeedbackTrainingEndDate] isEqual:[NSNull null]] ?@"" :(empTraining.endDate=[dict objectForKey:kFeedbackTrainingEndDate]);
;
        [[dict objectForKey:kFeedbackTrainingTrainerName] isEqual:[NSNull null]] ?@"" :(empTraining.trainer=[dict objectForKey:kFeedbackTrainingTrainerName]);

        [[dict objectForKey:kFeedbackTrainingTrainerID] isEqual:[NSNull null]] ?@"" :(empTraining.trainerID =[dict objectForKey:kFeedbackTrainingTrainerID]);

        [trainingArray addObject:empTraining];
        
    }
    return trainingArray;
}


-(NSMutableArray*)parseTrainerFeedback:(NSArray*)arrTrainerFeedback
{
    NSMutableArray* trainingFeedbackTemplete = [[NSMutableArray alloc]init];

    for(NSDictionary* dict in arrTrainerFeedback)
    {
        igTFeedbackRatingInfo *empRatingInfo=[[igTFeedbackRatingInfo alloc]init];
        
        [[dict objectForKey:kMappingId] isEqual:[NSNull null]] ?@"" :(empRatingInfo.mappingID=[dict objectForKey:kMappingId]);
        
        [[dict objectForKey:kCategoryId] isEqual:[NSNull null]] ?@"" :(empRatingInfo.categoryID=[dict objectForKey:kCategoryId]);
        
        [[dict objectForKey:kSubCategoryId] isEqual:[NSNull null]] ?@"" :(empRatingInfo.subCategoryID=[dict objectForKey:kSubCategoryId]);
        
        [[dict objectForKey:kFeedbackType] isEqual:[NSNull null]] ?@"" :(empRatingInfo.feedbackType=[dict objectForKey:kFeedbackType]);
        ;
        [[dict objectForKey:kFeedbackSubType] isEqual:[NSNull null]] ?@"" :(empRatingInfo.feedbackSubType=[dict objectForKey:kFeedbackSubType]);
        
        [[dict objectForKey:kFlag] isEqual:[NSNull null]] ?@"" :(empRatingInfo.flag =[dict objectForKey:kFlag]);
        
        //empRatingInfo.trainerID = @"123";
         [[dict objectForKey:kTrainerID] isEqual:[NSNull null]] ?@"" :(empRatingInfo.trainerID =[dict objectForKey:kTrainerID]);
        
         [[dict objectForKey:kFacultyName] isEqual:[NSNull null]] ?@"" :(empRatingInfo.trainerName =[dict objectForKey:kFacultyName]);
        
        [trainingFeedbackTemplete addObject:empRatingInfo];
    }
    return trainingFeedbackTemplete;

}
-(NSDictionary*)parseFeedbackRating:(NSDictionary*)responseDictionary{

    NSMutableDictionary* dictFeedbackRatings = [[NSMutableDictionary alloc]init];
   // NSDictionary* dictFedbacks   = [responseDictionary objectForKey:kFeedbacks];
    for (NSString* key in responseDictionary)
    {
        if([key isEqualToString:@"TrainerCount"] || [key isEqualToString:@"Message"])
            continue;
        else if([key isEqualToString:kTrainerFeedback])
        {
            NSDictionary* dictTrainerFeedback = [responseDictionary objectForKey:key];
            for(int count = 0; count <dictTrainerFeedback.count; count++)
            {
                
                NSArray* arrTrainderFeedback   = [dictTrainerFeedback objectForKey:[NSString stringWithFormat:@"trainerFeedback%d",count+1]];
                NSMutableArray* arrFeedback = [self parseTrainerFeedback:arrTrainderFeedback];
                [dictFeedbackRatings setObject:arrFeedback forKey:[NSString stringWithFormat:@"trainerFeedback%d",count+1]];
            }

        }
        else{
            NSMutableArray* arrFeedbackTemplete = [self getFeedbackTemplate:responseDictionary TemplateID:key];
        
            [dictFeedbackRatings setObject:arrFeedbackTemplete forKey:key];
        }

    }
    return dictFeedbackRatings;

}

-(NSMutableArray*)getFeedbackTemplate:(NSDictionary*)responseDictionary TemplateID:(NSString*)templateID
{
    NSMutableArray* trainingFeedbackTemplete = [[NSMutableArray alloc]init];
  
        NSArray* arrFeedbackTemplete1Info = [responseDictionary objectForKey:templateID];
        
        if(arrFeedbackTemplete1Info != nil && arrFeedbackTemplete1Info.count > 0)
        {
            for(NSDictionary* dict in arrFeedbackTemplete1Info)
            {
                igTFeedbackRatingInfo *empRatingInfo=[[igTFeedbackRatingInfo alloc]init];
                
                [[dict objectForKey:kMappingId] isEqual:[NSNull null]] ?@"" :(empRatingInfo.mappingID=[dict objectForKey:kMappingId]);
                
                [[dict objectForKey:kCategoryId] isEqual:[NSNull null]] ?@"" :(empRatingInfo.categoryID=[dict objectForKey:kCategoryId]);
                
                [[dict objectForKey:kSubCategoryId] isEqual:[NSNull null]] ?@"" :(empRatingInfo.subCategoryID=[dict objectForKey:kSubCategoryId]);
                
                [[dict objectForKey:kFeedbackType] isEqual:[NSNull null]] ?@"" :(empRatingInfo.feedbackType=[dict objectForKey:kFeedbackType]);
                ;
                [[dict objectForKey:kFeedbackSubType] isEqual:[NSNull null]] ?@"" :(empRatingInfo.feedbackSubType=[dict objectForKey:kFeedbackSubType]);
                
                [[dict objectForKey:kFlag] isEqual:[NSNull null]] ?@"" :(empRatingInfo.flag =[dict objectForKey:kFlag]);
                empRatingInfo.trainerID = @"";
                empRatingInfo.trainerName = @"";
                [trainingFeedbackTemplete addObject:empRatingInfo];
            }
        }
        return trainingFeedbackTemplete;
    }


@end
