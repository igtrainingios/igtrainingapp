//
//  WebServiceClient.m
//  igTraining
//  Created by Niharika on 29/01/15.
//  Copyright (c) 2015 Infogain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebServiceClient : NSObject


-(void)processAsynchronousPOSTRequestForUrl:(NSString *) url withRequestBody:(NSString *)requestData withCompletionHandler:(void (^)(NSData *data)) completionHandler withFailureHandler : (void (^)(NSError *error) ) failureHandler;

-(void)processAsynchronousGETRequestForUrl:(NSString *) url withRequestBody:(NSString *)requestData withCompletionHandler:(void (^)(NSData *data)) completionHandler withFailureHandler : (void (^)(NSError *error) ) failureHandler ;


@end
