//
//  WebServiceRequestHelper.h
//  igTraining
//
//  Created by Niharika on 29/01/15.
//  Copyright (c) 2015 Infogain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceClient.h"
#import <UIKit/UIKit.h>

@interface WebServiceRequestHelper : NSObject
@property(nonatomic ,strong) WebServiceClient *webserviceClient;


-(void)requestForLogin:(NSString*)domainId andUsername:(NSString *)username andPassword:(NSString *)password withSucessHandler:(void(^)(NSString *message)) sucessHandler withFailureHandler :(void(^)(NSError *error)) failureHandler;


-(void)requestForNominate :(NSString *)employeeId andTrainingId :(NSString *)trainingId withSucceHandler :(void(^)(NSString *message)) sucessHandler withFailureHandler :(void(^)(NSError *error)) failureHandler;

-(void)requestForTraining :(NSString *)employeeId  locationID:(NSString*)locationID withSucceHandler :(void(^)(NSArray *message)) sucessHandler withFailureHandler :(void(^)(NSError *error)) failureHandler;

-(void)requestForCancellingTraining :(NSString *)employeeId andTrainingId :(NSString *)trainingId withCancelMessage:(NSString *)cancelMsg withSucceHandler :(void(^)(NSString *message)) sucessHandler withFailureHandler :(void(^)(NSError *error)) failureHandler;

-(void)requestForFeedbackTraining :(NSString *)employeeId withSucceHandler :(void(^)(NSArray *message))sucessHandler withFailureHandler :(void(^)(NSError *error)) failureHandler;

-(void)getFeedbackStarRatingInformation :(NSString *)employeeId withSucceHandler :(void(^)(NSDictionary *message)) sucessHandler withFailureHandler :(void(^)(NSError *error)) failureHandler;

-(void)requestForTrainingSubmission :(NSString *)employeeId andTrainingId :(NSString *)trainingId Rating:(NSString*)ratings withSucceHandler :(void(^)(NSString *message)) sucessHandler withFailureHandler :(void(^)(NSError *error)) failureHandler;

/*
-(void)requestForEmployeeImage :(NSString *)employeeId  withSucceHandler :(void(^)(UIImage *image)) sucessHandler withFailureHandler :(void(^)(NSError *error)) failureHandler;


-(void)requestForEmployeeDetails:(NSString*)domainId andUsername:(NSString *)username  withSucessHandler:(void(^)(NSMutableDictionary *employeeDetails)) sucessHandler withFailureHandler :(void(^)(NSError *error)) failureHandler;
 
 
 */


@end
