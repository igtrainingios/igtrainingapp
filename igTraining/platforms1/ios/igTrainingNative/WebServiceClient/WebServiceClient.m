//
//  WebServiceClient.m
//  igTraining
//  Created by Niharika on 29/01/15.
//  Copyright (c) 2015 Infogain. All rights reserved.
//

#import "WebServiceClient.h"

#define MAX_TIMEOUT 60

@implementation WebServiceClient


-(void)processAsynchronousPOSTRequestForUrl:(NSString *) url withRequestBody:(NSString *)requestData withCompletionHandler:(void (^)(NSData *data)) completionHandler withFailureHandler : (void (^)(NSError *error) ) failureHandler {
    
    NSURLRequest *urlRequest = [self urlRequestForPostData:[requestData dataUsingEncoding:NSUTF8StringEncoding] forUrl:url];
    NSOperationQueue *requestQueue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:requestQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if(connectionError) {
            failureHandler(connectionError);
        } else {
            completionHandler(data);
        }
    }];
}

-(void)processAsynchronousGETRequestForUrl:(NSString *) url withRequestBody:(NSString *)requestData withCompletionHandler:(void (^)(NSData *data)) completionHandler withFailureHandler : (void (^)(NSError *error) ) failureHandler {
    
    NSURLRequest *urlRequest = [self urlRequestForGetData:[requestData dataUsingEncoding:NSUTF8StringEncoding] forUrl:url];
    NSOperationQueue *requestQueue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:requestQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if(connectionError) {
            failureHandler(connectionError);
        } else {
            completionHandler(data);
        }
    }];
}


-(NSURLRequest*) urlRequestForPostData :(NSData *) data forUrl :(NSString*) url{
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:data];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setTimeoutInterval:MAX_TIMEOUT];
    return urlRequest;
}


-(NSURLRequest*) urlRequestForGetData :(NSData *) data forUrl :(NSString*) url{
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [urlRequest setHTTPMethod:@"GET"];
    [urlRequest setHTTPBody:data];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setTimeoutInterval:MAX_TIMEOUT];
    return urlRequest;
}





@end
