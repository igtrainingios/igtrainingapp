//
//  WebServiceRequestHelper.m
//  igTraining
//
//  Created by Niharika on 29/01/15.
//  Copyright (c) 2015 Infogain. All rights reserved.
//

#import "WebServiceRequestHelper.h"
#import "WebServiceClient.h"
#import "Constants.h"
#import "igTTrainingListParser.h"
#import "IGUserDefaults.h"


@implementation WebServiceRequestHelper
-(void)requestForLogin:(NSString*)domainId andUsername:(NSString *)username andPassword:(NSString *)password withSucessHandler:(void(^)(NSString *message)) sucessHandler withFailureHandler :(void(^)(NSError *error)) failureHandler
{
    self.webserviceClient=[[WebServiceClient alloc]init];

    NSDictionary *dic =[NSDictionary dictionaryWithObjectsAndKeys:domainId,@"Domainid",username
                        ,@"UserName",password,@"Password",nil];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic
                                                       options:0
                                                         error:nil];
    

    NSString* jsonstring = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

    [_webserviceClient processAsynchronousPOSTRequestForUrl:kLoginUrl withRequestBody:jsonstring withCompletionHandler:^(NSData *data) {
        
        NSDictionary* responseDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                             options:0
                                                               error:nil];
        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error;
        
        if (responseDictionary) {
            if([[responseDictionary valueForKey:@"LoginStatus"] isEqualToString:@"Success"])
            {
                NSString *base64EncodedString = [responseDictionary objectForKey:kIGServiceKeyEmployeeImage];
                if (base64EncodedString != nil)
                {
                    NSData *imageData = [[NSData alloc] initWithBase64EncodedString:base64EncodedString options:NSDataBase64DecodingIgnoreUnknownCharacters];
                    if (imageData != nil)
                    {
                        UIImage *img = [UIImage imageWithData:imageData];
                        [IGUserDefaults sharedInstance].userImage = img;
                    }
                }
                
                [[IGUserDefaults sharedInstance]loginWithUser:responseDictionary updateInfo:true];
               /* [IGUserDefaults sharedInstance].employee.staffID = [responseDictionary valueForKey:kIGServiceKeyEmployeeStaffID] ;
                [IGUserDefaults sharedInstance].employee.name = [responseDictionary valueForKey:kIGServiceKeyEmployeeName] ;
                  [IGUserDefaults sharedInstance].employee.mailID = [responseDictionary valueForKey:kIGServiceKeyEmployeeMailID] ;*/

                sucessHandler(responseString);

            }
            else
            {
                error=[NSError errorWithDomain:@"" code:[kLoginFail integerValue] userInfo:nil];
                failureHandler(error);
            }
            

        }else{
            failureHandler(error);
        }
    } withFailureHandler:^(NSError *error) {
        NSLog(@"error%@",error);
        failureHandler(error);
    }];

}

-(void)requestForNominate :(NSString *)employeeId andTrainingId :(NSString *)trainingId withSucceHandler :(void(^)(NSString *message)) sucessHandler withFailureHandler :(void(^)(NSError *error)) failureHandler
{
    self.webserviceClient=[[WebServiceClient alloc]init];
    
    NSDictionary *dic =[NSDictionary dictionaryWithObjectsAndKeys:employeeId,@"EmployeeId",trainingId
                        ,@"TrainingID",nil];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic
                                                       options:0
                                                         error:nil];
    
    
    NSString* jsonstring = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
//  employeeId=@"2131024";
//    trainingId=@"3060";
    [_webserviceClient processAsynchronousPOSTRequestForUrl:kNominateUrl withRequestBody:jsonstring withCompletionHandler:^(NSData *data) {
        
        NSDictionary* responseDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                           options:0
                                                                             error:nil];
        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error;
        
        if (responseDictionary) {
            if([responseDictionary valueForKey:@"Message"])
                sucessHandler([responseDictionary valueForKey:@"Message"]);
            else
                failureHandler(error);
        }else{
            failureHandler(error);
        }
    } withFailureHandler:^(NSError *error) {
        NSLog(@"error%@",error);
        failureHandler(error);
    }];
    
}

-(void)requestForCancellingTraining :(NSString *)employeeId andTrainingId :(NSString *)trainingId withCancelMessage:(NSString *)cancelMsg withSucceHandler :(void(^)(NSString *message)) sucessHandler withFailureHandler :(void(^)(NSError *error)) failureHandler
{
    self.webserviceClient=[[WebServiceClient alloc]init];
    
    NSDictionary *dic =[NSDictionary dictionaryWithObjectsAndKeys:employeeId,@"EmployeeId",trainingId
                        ,@"TrainingID",cancelMsg,@"cancelMsg",nil];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic
                                                       options:0
                                                         error:nil];
    
    
    NSString* jsonstring = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
//    employeeId=@"2131024";
    //    trainingId=@"3060";
    [_webserviceClient processAsynchronousPOSTRequestForUrl:kCancelTrainingUrl withRequestBody:jsonstring withCompletionHandler:^(NSData *data) {
        
        NSDictionary* responseDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                           options:0
                                                                             error:nil];
        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error;
        
        if (responseDictionary) {
            if([responseDictionary valueForKey:@"Message"])
                sucessHandler([responseDictionary valueForKey:@"Message"]);
            else
                failureHandler(error);
        }else{
            failureHandler(error);
        }
    } withFailureHandler:^(NSError *error) {
        NSLog(@"error%@",error);
        failureHandler(error);
    }];

    
}
-(void)requestForTraining :(NSString *)employeeId locationID:(NSString*)locationID withSucceHandler :(void(^)(NSArray *message)) sucessHandler withFailureHandler :(void(^)(NSError *error)) failureHandler
{
    self.webserviceClient=[[WebServiceClient alloc]init];
//    employeeId=@"2131024";
    NSDictionary *dic =[NSDictionary dictionaryWithObjectsAndKeys:employeeId,@"EmployeeId",locationID
                        ,@"locationId",nil];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic
                                                       options:0
                                                         error:nil];
    
    NSString* jsonstring = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    igTTrainingListParser *trainingParser=[[igTTrainingListParser alloc]init];
    [_webserviceClient processAsynchronousPOSTRequestForUrl:kTrainingUrl withRequestBody:jsonstring withCompletionHandler:^(NSData *data) {
        
        NSDictionary* responseDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                           options:0
                                                                             error:nil];
        
        NSArray* array = [responseDictionary objectForKey:@"TrainingDetail"];
        //NSArray *array=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        //NSString *responseString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        //data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error;
        
        if (responseDictionary) {
            
                sucessHandler([trainingParser parseTraining:array ]);
        }else{
            failureHandler(error);
        }
    } withFailureHandler:^(NSError *error) {
        NSLog(@"error%@",error);
        failureHandler(error);
    }];
    
}

-(void)requestForFeedbackTraining :(NSString *)employeeId withSucceHandler :(void(^)(NSArray *message)) sucessHandler withFailureHandler :(void(^)(NSError *error)) failureHandler;
{
    self.webserviceClient=[[WebServiceClient alloc]init];
    //    employeeId=@"2131024";
    NSDictionary *dic =[NSDictionary dictionaryWithObjectsAndKeys:[IGUserDefaults sharedInstance].employee.staffID,@"EmployeeId",
                        nil];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic
                                                       options:0
                                                         error:nil];
    
    NSString* jsonstring = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    igTTrainingListParser *trainingParser=[[igTTrainingListParser alloc]init];
    [_webserviceClient processAsynchronousPOSTRequestForUrl:kFeedbackTrainingsUrl withRequestBody:jsonstring withCompletionHandler:^(NSData *data) {
        
        NSDictionary* responseDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                           options:0
                                                                             error:nil];
        
        NSArray* array = [responseDictionary objectForKey:@"FeedBackTrainingDetail"];
        NSError *error;
        
        if (responseDictionary) {
            
            sucessHandler([trainingParser parseFeedbackTraining:array]);
        }else{
            failureHandler(error);
        }
    } withFailureHandler:^(NSError *error) {
        NSLog(@"error%@",error);
        failureHandler(error);
    }];

}


-(void)getFeedbackStarRatingInformation :(NSString *)employeeId withSucceHandler :(void(^)(NSDictionary *message)) sucessHandler withFailureHandler :(void(^)(NSError *error)) failureHandler
{
    self.webserviceClient=[[WebServiceClient alloc]init];
    //    employeeId=@"2131024";
    NSDictionary *dic =[NSDictionary dictionaryWithObjectsAndKeys:employeeId,@"TrainingId"
                        ,nil];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic
                                                       options:0
                                                         error:nil];
    
    NSString* jsonstring = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    igTTrainingListParser *trainingParser=[[igTTrainingListParser alloc]init];
    [_webserviceClient processAsynchronousPOSTRequestForUrl:kFeedbackRatingURL withRequestBody:jsonstring withCompletionHandler:^(NSData *data) {
        
        
        /*NSString *filePath = [[NSBundle mainBundle] pathForResource:@"feedback" ofType:@"json"];
        
        //création d'un string avec le contenu du JSON
        NSData *localJson = [NSData dataWithContentsOfFile:filePath];*/

        
        NSDictionary* responseDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                           options:0
                                                                             error:nil];
        
       // NSArray* array = [responseDictionary objectForKey:@"TrainingDetail"];
        //NSArray *array=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        //NSString *responseString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        //data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error;
        
        if (responseDictionary) {
            
            sucessHandler([trainingParser parseFeedbackRating:responseDictionary]);
        }else{
            failureHandler(error);
        }
    } withFailureHandler:^(NSError *error) {
        NSLog(@"error%@",error);
        failureHandler(error);
    }];
    
}

-(void)requestForTrainingSubmission :(NSString *)employeeId andTrainingId :(NSString *)trainingId Rating:(NSString*)ratings withSucceHandler :(void(^)(NSString *message)) sucessHandler withFailureHandler :(void(^)(NSError *error)) failureHandler
{
    self.webserviceClient=[[WebServiceClient alloc]init];
    
    NSDictionary *dic =[NSDictionary dictionaryWithObjectsAndKeys:employeeId,@"EmployeeId",trainingId
                        ,@"TrainingID",ratings,@"RatingAndComments",nil];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic
                                                       options:0
                                                         error:nil];
    
    
    NSString* jsonstring = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"respinse = %@",jsonstring);
    //  employeeId=@"2131024";
    //    trainingId=@"3060";
    [_webserviceClient processAsynchronousPOSTRequestForUrl:kTrainingFeedbackSubmission withRequestBody:jsonstring withCompletionHandler:^(NSData *data) {
        
        NSDictionary* responseDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                           options:0
                                                                             error:nil];
        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error;
        
        if (responseDictionary) {
            if([responseDictionary valueForKey:@"Message"])
                sucessHandler([responseDictionary valueForKey:@"Message"]);
            else
                failureHandler(error);
        }else{
            failureHandler(error);
        }
    } withFailureHandler:^(NSError *error) {
        NSLog(@"error%@",error);
        failureHandler(error);
    }];
    
}


/*
-(void)requestForEmployeeImage :(NSString *)employeeId  withSucceHandler :(void(^)(UIImage *image)) sucessHandler withFailureHandler :(void(^)(NSError *error)) failureHandler
{
    self.webserviceClient=[[WebServiceClient alloc]init];
//    employeeId=@"2140947";
    NSDictionary *dic =[NSDictionary dictionaryWithObjectsAndKeys:employeeId,@"EmployeeId",nil];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic
                                                       options:0
                                                         error:nil];
    
    
    NSString* jsonstring = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [_webserviceClient processAsynchronousPOSTRequestForUrl:kImageUrl withRequestBody:jsonstring withCompletionHandler:^(NSData *data) {
        
        NSDictionary* responseDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                           options:0
                                                                             error:nil];

        UIImage *employeeImage = nil;

        id value = [responseDictionary objectForKey:@"EmpImage"];
        if (value)
        {
            NSData *imageData = [[NSData alloc] initWithBase64EncodedString:value options:NSDataBase64DecodingIgnoreUnknownCharacters];
            if (imageData == nil)
            {
                employeeImage = [UIImage imageNamed:@"profile-avtaar-pic"];
            }
            else
            {
                employeeImage = [UIImage imageWithData:imageData];
            }
        }
        
        sucessHandler(employeeImage);
//        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
//        data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
//        NSError *error;
//        
//        if (responseDictionary) {
//            if([[responseDictionary valueForKey:@"LoginStatus"] isEqualToString:@"Success"])
//                sucessHandler(responseString);
//            else
//                failureHandler(error);
//            
//        }else{
//            failureHandler(error);
//        }
    } withFailureHandler:^(NSError *error) {
        NSLog(@"error%@",error);
        failureHandler(error);
    }];
    


}


-(void)requestForEmployeeDetails:(NSString*)domainId andUsername:(NSString *)username  withSucessHandler:(void(^)(NSMutableDictionary *employeeDetails)) sucessHandler withFailureHandler :(void(^)(NSError *error)) failureHandler{
    self.webserviceClient=[[WebServiceClient alloc]init];
    
    NSDictionary *dic =[NSDictionary dictionaryWithObjectsAndKeys:domainId,@"Domainid",username
                        ,@"UserName",nil];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic
                                                       options:0
                                                         error:nil];
    
    
    NSString* jsonstring = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [_webserviceClient processAsynchronousPOSTRequestForUrl:kEmployeeDetailUrl withRequestBody:jsonstring withCompletionHandler:^(NSData *data) {
        
        NSDictionary* responseDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                           options:0
                                                                             error:nil];
        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error;
        
        if (responseDictionary) {
           // [[responseDictionary[@"LoginDetail"] objectAtIndex:0] objectForKey:@"EMP_STAFFID"];
//
         //   if([[responseDictionary valueForKey:@"LoginStatus"] isEqualToString:@"Success"])
                sucessHandler( [responseDictionary[@"LoginDetail"] objectAtIndex:0] );
//            else
//                failureHandler(error);
            
        }else{
            failureHandler(error);
        }
    } withFailureHandler:^(NSError *error) {
        NSLog(@"error%@",error);
        failureHandler(error);
    }];
    

}
 */
@end
