//
//  IGFileManager.m
//  IGTimeSheet
//
//  Created by Manish on 06/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGFileManager.h"

#define kErrorDomain                    @"com.igTimeSheet"

@implementation IGFileManager


/*
    Method:      pathForDirectory
    Description: Get the directory path from  document/cache or from Temporary location
 */

+ (NSString *)pathForDirectory:(igDirectory)directory
{
    switch (directory)
    {
        case igDocumentDirectory:
        {
            return [self pathForSystemDirectory:NSDocumentDirectory];
        }
            
        case igCacheDirectory:
        {
            return [self pathForSystemDirectory:NSCachesDirectory];
        }
            
        case igTemporaryDirectory:
        {
            return NSTemporaryDirectory();
        }
            
        default:
        {
            return nil;
        }
    }
}

+ (NSString *)pathForSystemDirectory:(NSSearchPathDirectory)directory
{
    return [NSSearchPathForDirectoriesInDomains(directory, NSUserDomainMask, YES) lastObject];
}

+ (BOOL)writeData:(NSData *)data
         fileName:(NSString *)fileName
        directory:(igDirectory)directory
            error:(NSError * __autoreleasing *)error
{
    if (data != nil && [data isKindOfClass:[NSData class]] == NO)
    {
        *error = [[NSError alloc] initWithDomain:kErrorDomain code:0 userInfo:@{ NSLocalizedDescriptionKey : kStringMessageExpectedParameterOfKindNSData }];
        return NO;
    }
    
    if (data == nil || data.length == 0)
    {
        *error = [[NSError alloc] initWithDomain:kErrorDomain code:0 userInfo:@{ NSLocalizedDescriptionKey : kStringMessageNoDataToWrite }];
        return NO;
    }
    
    if ([igUtility isValidString:fileName] == NO)
    {
        *error = [[NSError alloc] initWithDomain:kErrorDomain code:0 userInfo:@{ NSLocalizedDescriptionKey : kStringMessageFileNameNotValid }];
        return NO;
    }
    
    NSString *directoryPath = [self pathForDirectory:directory];
    if (directoryPath == nil)
    {
        *error = [[NSError alloc] initWithDomain:kErrorDomain code:0 userInfo:@{ NSLocalizedDescriptionKey : kStringMessageDirectoryNotRecognized }];
        return NO;
    }
    
    NSString *filePath = [directoryPath stringByAppendingPathComponent:fileName];
    
    NSError *lError = nil;
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        BOOL success = [[NSFileManager defaultManager] removeItemAtPath:filePath error:&lError];
        
        if (success == NO)
        {
            if (lError)
            {
                *error = [[NSError alloc] initWithDomain:kErrorDomain code:0 userInfo:lError.userInfo];
            }
            else
            {
                *error = [[NSError alloc] initWithDomain:kErrorDomain code:0 userInfo:@{ NSLocalizedDescriptionKey : kStringMessageDeleteOperationFailed}];
            }
            
            return NO;
        }
    }
    
    if ([data writeToFile:filePath options:NSDataWritingAtomic error:&lError] == NO)
    {
        if (lError)
        {
            *error = [[NSError alloc] initWithDomain:kErrorDomain code:0 userInfo:lError.userInfo];
        }
        else
        {
            *error = [[NSError alloc] initWithDomain:kErrorDomain code:0 userInfo:@{ NSLocalizedDescriptionKey : kStringMessageWriteOperationFailed}];
        }
        return NO;
    }
    
    return YES;
}

+ (BOOL)writeDictionary:(NSDictionary *)data
               fileName:(NSString *)fileName
              directory:(igDirectory)directory
                  error:(NSError * __autoreleasing *)error;
{
    if (data != nil && [data isKindOfClass:[NSDictionary class]] == NO)
    {
        *error = [[NSError alloc] initWithDomain:kErrorDomain code:0 userInfo:@{ NSLocalizedDescriptionKey : kStringMessageExpectedParameterOfKindNSDictionary }];
        return NO;
    }
    
    if (data == nil)
    {
        *error = [[NSError alloc] initWithDomain:kErrorDomain code:0 userInfo:@{ NSLocalizedDescriptionKey : kStringMessageNoDataToWrite }];
        return NO;
    }
    
    if ([igUtility isValidString:fileName] == NO)
    {
        *error = [[NSError alloc] initWithDomain:kErrorDomain code:0 userInfo:@{ NSLocalizedDescriptionKey : kStringMessageFileNameNotValid }];
        return NO;
    }
    
    NSString *directoryPath = [self pathForDirectory:directory];
    if (directoryPath == nil)
    {
        *error = [[NSError alloc] initWithDomain:kErrorDomain code:0 userInfo:@{ NSLocalizedDescriptionKey : kStringMessageDirectoryNotRecognized}];
        return NO;
    }
    
    NSString *filePath = [directoryPath stringByAppendingPathComponent:fileName];
    
    NSError *lError = nil;
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        BOOL success = [[NSFileManager defaultManager] removeItemAtPath:filePath error:&lError];
        
        if (success == NO)
        {
            if (lError)
            {
                *error = [[NSError alloc] initWithDomain:kErrorDomain code:0 userInfo:lError.userInfo];
            }
            else
            {
                *error = [[NSError alloc] initWithDomain:kErrorDomain code:0 userInfo:@{ NSLocalizedDescriptionKey : kStringMessageDeleteOperationFailed}];
            }
            
            return NO;
        }
    }
    
    if ([data writeToFile:filePath atomically:YES] == NO)
    {
        *error = [[NSError alloc] initWithDomain:kErrorDomain code:0 userInfo:@{ NSLocalizedDescriptionKey : kStringMessageWriteOperationFailed}];
        return NO;
    }
    
    return YES;
}

+ (BOOL)writeArray:(NSArray *)data
          fileName:(NSString *)fileName
         directory:(igDirectory)directory
             error:(NSError * __autoreleasing *)error
{
    if (data != nil && [data isKindOfClass:[NSArray class]] == NO)
    {
        *error = [[NSError alloc] initWithDomain:kErrorDomain code:0 userInfo:@{ NSLocalizedDescriptionKey : kStringMessageExpectedParameterOfKindNSArray }];
        return NO;
    }
    
    if (data == nil)
    {
        *error = [[NSError alloc] initWithDomain:kErrorDomain code:0 userInfo:@{ NSLocalizedDescriptionKey : kStringMessageNoDataToWrite }];
        return NO;
    }
    
    if ([igUtility isValidString:fileName] == NO)
    {
        *error = [[NSError alloc] initWithDomain:kErrorDomain code:0 userInfo:@{ NSLocalizedDescriptionKey : kStringMessageFileNameNotValid }];
        return NO;
    }
    
    NSString *directoryPath = [self pathForDirectory:directory];
    if (directoryPath == nil)
    {
        *error = [[NSError alloc] initWithDomain:kErrorDomain code:0 userInfo:@{ NSLocalizedDescriptionKey : kStringMessageDirectoryNotRecognized}];
        return NO;
    }
    
    NSString *filePath = [directoryPath stringByAppendingPathComponent:fileName];
    
    NSError *lError = nil;
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        BOOL success = [[NSFileManager defaultManager] removeItemAtPath:filePath error:&lError];
        
        if (success == NO)
        {
            if (lError)
            {
                *error = [[NSError alloc] initWithDomain:kErrorDomain code:0 userInfo:lError.userInfo];
            }
            else
            {
                *error = [[NSError alloc] initWithDomain:kErrorDomain code:0 userInfo:@{ NSLocalizedDescriptionKey : kStringMessageDeleteOperationFailed}];
            }
            
            return NO;
        }
    }
    
    if ([data writeToFile:filePath atomically:YES] == NO)
    {
        *error = [[NSError alloc] initWithDomain:kErrorDomain code:0 userInfo:@{ NSLocalizedDescriptionKey : kStringMessageWriteOperationFailed}];
        return NO;
    }
    
    return YES;
}


+ (UIImage *)imageWithName:(NSString *)fileName
                 directory:(igDirectory)directory
{
    if ([igUtility isValidString:fileName])
    {
        NSString *directoryPath = [self pathForDirectory:directory];
        if (directoryPath != nil)
        {
            NSString *filePath = [directoryPath stringByAppendingPathComponent:fileName];
            UIImage *image = [UIImage imageWithContentsOfFile:filePath];
            return image;
        }
    }
    return nil;
}

+ (BOOL)deleteFileNamed:(NSString *)fileName
              directory:(igDirectory)directory
{
    if ([igUtility isValidString:fileName])
    {
        NSString *directoryPath = [self pathForDirectory:directory];
        if (directoryPath != nil)
        {
            NSString *filePath = [directoryPath stringByAppendingPathComponent:fileName];
            
            NSError *error = nil;
            if ([[NSFileManager defaultManager] fileExistsAtPath:filePath])
            {
                if ([[NSFileManager defaultManager] removeItemAtPath:filePath error:&error])
                {
                    return YES;
                }
                else
                {
                    NSLog(kStringMessageErrorRemovingItemAtPath_Reason_, filePath, [error localizedDescription]);
                    return NO;
                }
            }
            else
            {
                return YES;
            }
        }
    }
    
    return NO;
}

+ (BOOL)fileExistsForName:(NSString *)fileName
                directory:(igDirectory)directory
{
    NSString *directoryPath = [self pathForDirectory:directory];
    if ([igUtility isValidString:fileName] && (directoryPath != nil))
    {
        NSString *filePath = [directoryPath stringByAppendingPathComponent:fileName];
        return [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    }
    
    return NO;
}


@end
