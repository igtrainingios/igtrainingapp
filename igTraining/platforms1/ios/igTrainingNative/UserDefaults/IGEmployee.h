//
//  IGEmployee.h
//  IGTimeSheet
//
//  Created by Manish on 23/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface IGEmployee : NSObject

@property (nonatomic, strong)   NSString *name;
@property (nonatomic, strong)   NSString *staffID; //employeeId
@property (nonatomic, strong) NSString *employeeNewID;
@property (nonatomic, strong) NSString *locationID;
@property (nonatomic, strong) NSString *mailID;
@property (nonatomic, strong)   UIImage *image;

@property (nonatomic, assign) BOOL isAuthenticated;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

- (void)reset;

@end
