//
//  IGFileManager.h
//  IGTimeSheet
//
//  Created by Manish on 06/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "igUtility.h"

typedef NS_ENUM(NSInteger, igDirectory) {
    igDocumentDirectory,
    igCacheDirectory,
    igTemporaryDirectory
};

@interface IGFileManager : NSObject

+ (NSString *)pathForDirectory:(igDirectory)directory;

+ (BOOL)writeData:(NSData *)data
         fileName:(NSString *)fileName
        directory:(igDirectory)directory
            error:(NSError * __autoreleasing *)error;

+ (BOOL)writeDictionary:(NSDictionary *)dictionary
               fileName:(NSString *)fileName
              directory:(igDirectory)directory
                  error:(NSError * __autoreleasing *)error;

+ (BOOL)writeArray:(NSArray *)data
          fileName:(NSString *)fileName
         directory:(igDirectory)directory
             error:(NSError * __autoreleasing *)error;

+ (UIImage *)imageWithName:(NSString *)fileName
                 directory:(igDirectory)directory;

+ (BOOL)deleteFileNamed:(NSString *)fileName
              directory:(igDirectory)directory;

+ (BOOL)fileExistsForName:(NSString *)fileName
                directory:(igDirectory)directory;

@end
