//
//  IGEmployee.m
//  IGTimeSheet
//
//  Created by Manish on 23/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGEmployee.h"
@implementation IGEmployee

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    if ([dictionary respondsToSelector:@selector(objectForKey:)] == NO)
    {
        NSLog(@"Expected a dictionary. Got %@", NSStringFromClass([dictionary class]));
        return nil;
    }
    
    self = [super init];
    
    return self;
}


- (void)reset
{
    _name = @"";
    _staffID = @"";
    _image = nil;
    _isAuthenticated = NO;
    _employeeNewID = @"";
    _mailID = @"";
    _employeeNewID = @"";
    _locationID = @"";
}

@end
