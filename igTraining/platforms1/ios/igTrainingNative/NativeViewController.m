//
//  ViewController.m
//  Moodle Mobile
//
//  Created by Asif on 21/03/17.
//
//

#import "NativeViewController.h"
#import "AppDelegate.h"

@interface NativeViewController ()

@end

@implementation NativeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)openMoodle:(id)sender {
 
    
    id viewController = ((AppDelegate*)(UIApplication.sharedApplication.delegate)).viewController;
    
    if (viewController == nil) {
        viewController = [[CDVViewController alloc] init];
    }
    [self presentViewController:viewController animated:YES completion:nil];
    //[self.view addSubview:viewController.view];
}

@end
