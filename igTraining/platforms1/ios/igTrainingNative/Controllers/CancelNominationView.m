//
//  ConcelNominationView.m
//  igTraining
//
//  Created by Sandeep on 29/12/16.
//  Copyright © 2016 Infogain. All rights reserved.
//

#import "CancelNominationView.h"

@implementation CancelNominationView

-(void)awakeFromNib
{
    [super awakeFromNib];
//    CGRect screenRect = [[UIScreen mainScreen] bounds];
  //  self.subView.frame = screenRect;
    self.btnCancelTraining.layer.borderWidth = 1.0f;
    self.btnDismissView.layer.borderWidth = 1.0f;
    self.btnCancelTraining.layer.borderColor = [UIColor blueColor].CGColor;
    self.btnDismissView.layer.borderColor = [UIColor blueColor].CGColor;
    
    self.btnCancelTraining.layer.cornerRadius = 2.0f;
    self.btnDismissView.layer.cornerRadius = 2.0f;
    
    self.txtCancelReason.layer.borderColor = [UIColor whiteColor].CGColor;;
    self.txtCancelReason.layer.borderWidth = 2.0;
    self.txtCancelReason.layer.cornerRadius = 2.0;
    [self.txtCancelReason becomeFirstResponder];
}


-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.txtCancelReason resignFirstResponder];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        self.txtCancelReason.text = @"";
        [self.txtCancelReason resignFirstResponder];
    }
    return true;
}

-(IBAction)actionDismissButton:(id)sender
{
    [self.txtCancelReason  resignFirstResponder];
    [self.cmdDelegate cancelNotification:self.txtCancelReason.text State:true];
    self.txtCancelReason.text = @"";

}

-(IBAction)actionCancelButton:(id)sender
{
    [self.txtCancelReason  resignFirstResponder];
    [self.cmdDelegate cancelNotification:self.txtCancelReason.text State:false];
    self.txtCancelReason.text = @"";

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
