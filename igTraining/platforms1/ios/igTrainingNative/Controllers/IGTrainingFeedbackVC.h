//
//  IGTrainingFeedbackVC.h
//  igTraining
//
//  Created by Shipra Chauhan on 09/11/16.
//  Copyright © 2016 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"
#import "igTTrainingFeedbackListModel.h"

@class RSDFDatePickerView;

@interface IGTrainingFeedbackVC : UIViewController

@end
