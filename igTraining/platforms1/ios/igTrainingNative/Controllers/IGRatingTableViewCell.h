//
//  IGRatingTableViewCell.h
//  igTraining
//
//  Created by Shipra Chauhan on 09/11/16.
//  Copyright © 2016 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"

@interface IGRatingTableViewCell : UITableViewCell<EDStarRatingProtocol>
@property (weak, nonatomic) IBOutlet UILabel *ratingQuestionLabel;
@property (weak, nonatomic) IBOutlet EDStarRating *ratingValueLabel;

@property (weak, nonatomic) IBOutlet EDStarRating *starRatingImage;
@end
