//
//  igTrainingFeedbackOverlay.h
//  igTraining
//
//  Created by Sandeep on 05/01/17.
//  Copyright © 2017 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "igTEmployeeTraining.h"

@interface igTrainingFeedbackOverlay : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scrollVw;

@property (weak, nonatomic) IBOutlet UILabel *lblCourseTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTrainingDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTrainerName;
@property (weak, nonatomic) IBOutlet UILabel *lblTrainingTime;
@property (weak, nonatomic) IBOutlet UILabel *lblTrainingVenue;
@property (weak, nonatomic) IBOutlet UILabel *lblPrerequisite;
@property (weak, nonatomic) IBOutlet UILabel *lblTrainingMode;
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;
@property (weak, nonatomic)igTEmployeeTraining* objTraining;


@end
