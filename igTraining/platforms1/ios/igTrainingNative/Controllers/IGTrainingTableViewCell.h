//
//  IGTrainingTableViewCell.h
//  igTraining
//
//  Created by Shipra Chauhan on 09/11/16.
//  Copyright © 2016 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGTrainingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *tableCellSubvw;
@property (weak, nonatomic) IBOutlet UIView *dateSubview;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTrainingTopic;
@property (weak, nonatomic) IBOutlet UILabel *lblTrainerName;


-(void)configureCell:(NSString*)date Topic:(NSString*)topic Trainer:(NSString*)trainer;
@end
