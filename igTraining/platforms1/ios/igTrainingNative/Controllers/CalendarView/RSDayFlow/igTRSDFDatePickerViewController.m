//
// RSDFDatePickerViewController.m
//
// Copyright (c) 2013 Evadne Wu, http://radi.ws/
// Copyright (c) 2013-2014 Ruslan Skorb, http://lnkd.in/gsBbvb
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#import "igTRSDFDatePickerViewController.h"
#import "RSDFDatePickerView.h"
#import "RSDFCustomDatePickerView.h"
#import "Constants.h"
#import "igTTrainingViewController.h"
#import "igTTutorialRootViewController.h"
#import "WebServiceRequestHelper.h"
#import "igTEmployeeTraining.h"
#import "igUtility.h"
#import  "IGUserDefaults.h"
#import "AppDelegate.h"

@interface igTRSDFDatePickerViewController() <RSDFDatePickerViewDelegate, RSDFDatePickerViewDataSource>
{
    NSMutableArray *datesArray;
    NSMutableArray *totalTrainingArray;
}
@property (strong, nonatomic) NSArray *datesToMark;
@property (strong, nonatomic) NSMutableArray *totalTrainingToMark;

@property (strong, nonatomic) NSDictionary *statesOfTasks;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) RSDFDatePickerView *datePickerView;
@property (strong, nonatomic) RSDFCustomDatePickerView *customDatePickerView;


@end

@implementation igTRSDFDatePickerViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
	[super viewDidLoad];
    // [igUtility setScreenName:@"TrainingCalender-CalenderView" ClassName:NSStringFromClass([self class])];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    NSArray* transparentxibContents = [[NSBundle mainBundle] loadNibNamed:@"TransparentView" owner:nil options:nil];
    self.transparentView = [transparentxibContents objectAtIndex:0];
    self.transparentView.frame = self.view.frame;
    _spinner = [[GMSpinKitView alloc] initWithColor:[UIColor colorWithRed:23.0/255.0  green:134.0/255.0 blue:216.0f/255.0 alpha:1.0f]];
    
    _spinner.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds) - 44.0f);
    [self.transparentView addSubview:_spinner];
    _spinner.hidden = YES;

    [_spinner stopAnimating];
    [self configureNavigatonBar];
    
    
    if (![IGUserDefaults sharedInstance].isFirstTimeLogin) {
        UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        igTTutorialRootViewController *tncVC = (igTTutorialRootViewController*) [mainStoryBoard instantiateViewControllerWithIdentifier:@"GMTutorialRootViewController"];
        
        [self presentViewController:tncVC animated:YES completion:^{
            
        }];
        
        [IGUserDefaults sharedInstance].isFirstTimeLogin = YES;
    }
}

//- (UIStatusBarStyle)preferredStatusBarStyle
//{
//    return UIStatusBarStyleLightContent;
//}

-(void)viewWillAppear:(BOOL)animated
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.view.backgroundColor = [UIColor whiteColor];
        [self.datePickerView removeFromSuperview];
        self.datePickerView = nil;
    });
    
    [self requestForTrainingList];

}



-(void)addDefaultView
{
    double delayInSeconds = 0.0; // number of seconds to wait
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){


        self.view.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.3];
        NSDateComponents *todayComponents = [self.calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[NSDate date]];
        NSDate *today = [self.calendar dateFromComponents:todayComponents];
        self.customDatePickerView.hidden = YES;
        [self.view addSubview:self.datePickerView];
        [self.datePickerView selectDate:today];
    });

}
/*
    Method:requestForTrainingList
    Description: This method is called to get the total  trainings from web services and create the date calender based on the trainings
 */
-(void)requestForTrainingList
{
    kAlertAndReturnIfNetworkNotReachable
    
    NSDictionary* dictonary = @{
                                @"screen_name": @"Training-List CalenderView",
                                @"event_name": @"all trainings service call"
                                };
   // [igUtility setEventName:NSStringFromSelector(_cmd) Parameters:dictonary];

    _trainingsArray=nil;
    _datesToMark = nil;
    _totalTrainingToMark = nil;
    [self showIndicator:true];

    WebServiceRequestHelper * helper=[[ WebServiceRequestHelper alloc]init];
    [helper requestForTraining:[IGUserDefaults sharedInstance].employee.staffID locationID: [IGUserDefaults sharedInstance].employee.locationID withSucceHandler:^(NSArray *message)
     {
        _trainingsArray=message;
        datesArray=[[NSMutableArray alloc]init];
        if(_trainingsArray.count > 0 && _trainingsArray != nil)
        {
            [self datesToMarkTrainings];
            [self totalTrainingToMarkTrainings];
            double delayInSeconds = 1.0; // number of seconds to wait
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self showIndicator:false];
                self.view.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.3];
                NSDateComponents *todayComponents = [self.calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[NSDate date]];
                NSDate *today = [self.calendar dateFromComponents:todayComponents];
                self.customDatePickerView.hidden = YES;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.datePickerView removeFromSuperview];
                    self.datePickerView = nil;
                [self.view addSubview:self.datePickerView];
                [self.datePickerView selectDate:today];
                });
                
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self showIndicator:false];
                [self addDefaultView];
            });
        }
     }
    withFailureHandler:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self addDefaultView];

            [self showIndicator:false];

            [igUtility showOKAlertWithTitle:kStringError message:[igUtility errorDescription:error]]; });
    }];
}

#pragma mark - Custom Accessors

- (void)setCalendar:(NSCalendar *)calendar
{
    if (![_calendar isEqual:calendar]) {
        _calendar = calendar;
        
        self.title = [_calendar.calendarIdentifier capitalizedString];
    }
}

/*
 Method:datesToMark
 Description: This method is used to filter all the trainings  for which employee is nominated and create the array based on the filter to mark the dates
 */
-(void)datesToMarkTrainings
{
    NSLog(@"datesToMarkTrainings");
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setCalendar:self.calendar];
    [dateFormatter setLocale:[self.calendar locale]];
    [dateFormatter setDateFormat:@"dd-MMM-yyyy"];

    for(igTEmployeeTraining *emp in _trainingsArray)
    {
        if(emp.isNominated)
          [ datesArray addObject:emp.date];
           }
    if (!_datesToMark && datesArray.count  > 0) {
        NSMutableArray *datesToMark = [[NSMutableArray alloc] initWithCapacity:datesArray.count];
       for(NSString *datesFromServer in datesArray)
       {
           NSDate *date = [dateFormatter dateFromString:datesFromServer];
           [datesToMark addObject:date];
       }
        
        _datesToMark = [datesToMark copy];
    }
}

/*
 Method:totalTrainingToMark
 Description: This method is used to filter all the trainings for the month and create the array based on the filter to mark the dates
 */
-(void)totalTrainingToMarkTrainings
{
    totalTrainingArray =[[NSMutableArray alloc]init];
    NSLog(@"totalTrainingToMarkTrainings");
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setCalendar:self.calendar];
    [dateFormatter setLocale:[self.calendar locale]];
    [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
    
    for(igTEmployeeTraining *emp in _trainingsArray)
    {
        if(!emp.isNominated && ![datesArray containsObject:emp.date])
                        [totalTrainingArray addObject:emp.date];
    }
    if (!_totalTrainingToMark && totalTrainingArray.count > 0) {
        NSMutableArray *datesToMark = [[NSMutableArray alloc] initWithCapacity:datesArray.count];
        for(NSString *datesFromServer in totalTrainingArray)
        {
            NSDate *date = [dateFormatter dateFromString:datesFromServer];
            [datesToMark addObject:date];
        }
        
        _totalTrainingToMark = [datesToMark copy];
    }
}


/*
 Method statesOfTasks
 Description:This method is called to  to filter out all the trainings and create array for states of task like- comppleted/Pending/Upcoming...
 */
- (NSDictionary *)statesOfTasks
{
    if (!_statesOfTasks) {
        NSDateComponents *todayComponents = [self.calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[NSDate date]];
        NSDate *today = [self.calendar dateFromComponents:todayComponents];
        
        NSMutableDictionary *statesOfTasks = [[NSMutableDictionary alloc] initWithCapacity:[self.datesToMark count]];
        [self.datesToMark enumerateObjectsUsingBlock:^(NSDate *date, NSUInteger idx, BOOL *stop) {
            BOOL isCompletedAllTasks = NO;
            if ([date compare:today] == NSOrderedAscending) {
                isCompletedAllTasks = YES;
            }
            statesOfTasks[date] = @(isCompletedAllTasks);
        }];
        
        _statesOfTasks = [statesOfTasks copy];
    }
    return _statesOfTasks;
}

#pragma dateFormatter styles
- (NSDateFormatter *)dateFormatter
{
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setCalendar:self.calendar];
        [_dateFormatter setLocale:[self.calendar locale]];
        [_dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    }
    return _dateFormatter;
}

- (RSDFDatePickerView *)datePickerView
{
	if (!_datePickerView) {
		_datePickerView = [[RSDFDatePickerView alloc] initWithFrame:self.view.bounds calendar:self.calendar];
        _datePickerView.delegate = self;
        _datePickerView.dataSource = self;
		_datePickerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	}
	return _datePickerView;
}

- (RSDFCustomDatePickerView *)customDatePickerView
{
    if (!_customDatePickerView) {
        _customDatePickerView = [[RSDFCustomDatePickerView alloc] initWithFrame:self.view.bounds calendar:self.calendar];
        _customDatePickerView.delegate = self;
        _customDatePickerView.dataSource = self;
		_customDatePickerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return _customDatePickerView;
}

#pragma mark - Action handling

- (void)onTodayButtonTouch:(UIBarButtonItem *)sender
{
    if (!self.datePickerView.hidden) {
        [self.datePickerView scrollToToday:YES];
    } else {
        [self.customDatePickerView scrollToToday:YES];
    }
}

- (void)onRestyleButtonTouch:(UIBarButtonItem *)sender
{
    if (!self.datePickerView.hidden) {
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:244/255.0f green:245/255.0f blue:247/255.0f alpha:1.0f];
        self.datePickerView.hidden = YES;
        self.customDatePickerView.hidden = NO;
    } else {
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:248/255.0f green:248/255.0f blue:248/255.0f alpha:1.0f];
        self.customDatePickerView.hidden = YES;
        self.datePickerView.hidden = NO;
    }
}

#pragma mark - RSDFDatePickerViewDelegate

- (void)datePickerView:(RSDFDatePickerView *)view didSelectDate:(NSDate *)date
{
    NSDictionary* dictonary = @{
                                @"screen_name": @"Training-List CalenderView",
                                @"event_name": NSStringFromSelector(_cmd)
                                };
    //[igUtility setEventName:NSStringFromSelector(_cmd) Parameters:dictonary];
    if([self.datesToMark containsObject:date] || [self.totalTrainingToMark containsObject:date])
    {


    igTTrainingViewController *trainingListController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:kTrainingViewController_Identifier];
        trainingListController.trainings=_trainingsArray;
    trainingListController.navigationTitle = [self.dateFormatter stringFromDate:date];
    trainingListController.showBackButton=YES;
    
    [self.navigationController pushViewController:trainingListController animated:YES];
        trainingListController.date=date;
    }
}

#pragma mark - RSDFDatePickerViewDataSource

- (BOOL)datePickerView:(RSDFDatePickerView *)view shouldMarkDate:(NSDate *)date
{
    //return [self.totalTrainingToMark containsObject:date];
   return NO;
}

- (BOOL)datePickerView:(RSDFDatePickerView *)view isCompletedAllTasksOnDate:(NSDate *)date
{
    //return [self.datesToMark containsObject:date];
	//return [self.statesOfTasks[date] boolValue];
    return NO;

}

- (BOOL)datePickerView:(RSDFDatePickerView *)view nominatedTrainingMark:(NSDate *)date{
    
    return [self.datesToMark containsObject:date];
//    return NO;
    
}
-(BOOL)datePickerView:(RSDFDatePickerView *)view totalTrainingMark:(NSDate *)date
{
    
    if([self.datesToMark containsObject:date]){
        return NO;    }
        
    
    return [self.totalTrainingToMark containsObject:date];

 //   return NO;
}

/*
 Method:showMainMenuBtnAction
 Description: This function is used to create and add side menu controller from where user can select menu items
 */
- (IBAction)showMainMenuBtnAction:(id)sender
{
    [self.sideMenuViewController presentMenuViewController];
}


/*
 Method:toggleView
 Description: Toggel View is used to show the training in from  of list or calender Vuew
 */
-(IBAction)toggleView:(id)sender{
    
    NSDictionary* dictonary = @{
                                @"screen_name": @"Training-List CalenderView",
                                @"event_name": NSStringFromSelector(_cmd)
                                };
   // [igUtility setEventName:NSStringFromSelector(_cmd) Parameters:dictonary];
     UINavigationController *navigationController = (UINavigationController *)self.sideMenuViewController.contentViewController;
    
    igTTrainingViewController *trainingListController =[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:kTrainingViewController_Identifier];

    trainingListController.navigationTitle = @"Training Calendar";
    trainingListController.trainings=_trainingsArray;
    navigationController.viewControllers = @[trainingListController];
    [self.sideMenuViewController hideMenuViewController];
}

-(void)configureNavigatonBar{

    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"pattern_bg"]]];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:18.0];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.text = [NSString stringWithFormat:@"Training Calender"];
    label.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = label;
    [label sizeToFit];
      self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.opaque = YES;
    
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Medium" size:17.0f]};
    
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    //self.navigationController.navigationItem set
    UIImage *image = [UIImage imageNamed:@"menu-icon.png"];
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:image
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(showMainMenuBtnAction:)];
    menuButton.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = menuButton;
    
    UIBarButtonItem *toggleViewButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"list-view-icon.png"] style:UIBarButtonItemStylePlain
                                                                        target:self
                                                                        action:@selector(toggleView:)];
        toggleViewButton.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = toggleViewButton;
    

}

//Show activity indicator while fetching service or performing any action
-(void)showIndicator:(BOOL)isTrue
{
    if(isTrue)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.transparentView removeFromSuperview];
            [self.navigationController.view addSubview:self.transparentView];
            
            _spinner.hidden = NO;
            [_spinner startAnimating];
        });
        
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.transparentView removeFromSuperview];
            
            _spinner.hidden = YES;
            [_spinner stopAnimating];
        });
    }
}


@end
