//
//  ViewController.m
//  SWTableViewCell
//
//  Created by Chris Wendel on 9/10/13.
//  Copyright (c) 2013 Chris Wendel. All rights reserved.
//

#import "igTTrainingViewController.h"
#import "SWTableViewCell.h"
#import "igTTrainingTableViewCell.h"
#import "RESideMenu.h"
#import "igUtility.h"
#import "Constants.h"
#import "igTRSDFDatePickerViewController.h"
#import "igTTutorialRootViewController.h"
#import "WebServiceRequestHelper.h"
#import "igTEmployeeTraining.h"
#import "igUtility.h"
#import "igTTrainingFeedbackListModel.h"
#import "IGUserDefaults.h"
#import "igTrainingFeedbackOverlay.h"
#import <EventKit/EventKit.h>
#import "IGAnalytics.h"
#import "AppDelegate.h"
#import <FirebaseAnalytics/FirebaseAnalytics.h>

@interface igTTrainingViewController () {
    NSArray *_sections;
    NSMutableArray *_testArray;
}

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic) BOOL useCustomCells;
@property (nonatomic, weak) UIRefreshControl *refreshControl;

@end

@implementation igTTrainingViewController
{
    NSArray *searchResults;
}
bool isSearch= NO;
- (void)viewDidLoad {
    [super viewDidLoad];
    NSArray* xibContents = [[NSBundle mainBundle] loadNibNamed:@"CancelNominationView" owner:nil options:nil];
    self.cancelView = [xibContents objectAtIndex:0];
    self.cancelView.frame = self.view.frame;
    /*self.cancelView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin |UIViewAutoresizingFlexibleWidth |
    UIViewAutoresizingFlexibleRightMargin |
    UIViewAutoresizingFlexibleTopMargin |
    UIViewAutoresizingFlexibleHeight |
    UIViewAutoresizingFlexibleBottomMargin ;
    */
    NSArray* transparentxibContents = [[NSBundle mainBundle] loadNibNamed:@"TransparentView" owner:nil options:nil];
    self.transparentView = [transparentxibContents objectAtIndex:0];
    self.transparentView.frame = self.view.frame;

    
    [igUtility setScreenName:@"TrainingCalender-ListView" ClassName:NSStringFromClass([self class])];
    
    _spinner = [[GMSpinKitView alloc] initWithColor:[UIColor colorWithRed:23.0/255.0  green:134.0/255.0 blue:216.0f/255.0 alpha:1.0f]];
    
    _spinner.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds) );
    _spinner.hidden = YES;
    [self.transparentView  addSubview:_spinner];
    [self.view bringSubviewToFront:_spinner];


    [self configureNavigationBar];
    // Setup refresh control for example app
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"dashboard_bgIPad"]];
        
    }
    else{
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"dashboard_bg"]];
        
    }
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(toggleCells:) forControlEvents:UIControlEventValueChanged];
    refreshControl.tintColor = [UIColor colorWithRed:23.0/255.0  green:134.0/255.0 blue:216.0f/255.0 alpha:1.0f];
    
    self.refreshControl = refreshControl;
    
    self.searchBar.delegate=self;
    igTTrainingTableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:@"UMCell"];
    NSLog(@"%f",cell.frame.origin.y);
    if(self.strSortByNominateID != nil || self.date != nil)
    {
        self.searchBar.hidden = true;
        self.searchBar.delegate = nil;
        CGRect bounds = self.tableView.bounds;
        bounds.origin.y = self.tableView.bounds.origin.y + self.searchBar.bounds.size.height;
        self.tableView.bounds = bounds;
       //[self.tableView setContentOffset:CGPointMake(0, 44)];
    }
    [self requestForTrainingList];
   
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    float scrollOffset = scrollView.contentOffset.y;
    
    if((scrollOffset ==  0) && (self.strSortByNominateID != nil || self.date != nil))
    {
        [self scrollingFinish];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
}
- (void)scrollingFinish {
        
    [UIView animateWithDuration:0.3
                     animations:
     ^{
         [self.tableView setContentOffset:CGPointMake(0, 44)];
         
     }
     ];

    
    //enter code here
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    

 }
-(void)requestForTrainingList
{
    kAlertAndReturnIfNetworkNotReachable

    NSDictionary* dictonary = @{
        @"screen_name": @"Training-List",
        @"event_name": @"all trainings service call"
    };
   [igUtility setEventName:NSStringFromSelector(_cmd) Parameters:dictonary];
    [self showIndicator:true];
    WebServiceRequestHelper * helper=[[ WebServiceRequestHelper alloc]init];
    [helper requestForTraining:[IGUserDefaults sharedInstance].employee.staffID locationID:[IGUserDefaults sharedInstance].employee.locationID withSucceHandler:^(NSArray *message) {
     
        _trainings=message;
        if(_trainings.count > 0 &&  _trainings != nil)
        {
            if(_trainings.count > 0)
            {
                if(self.date!=nil)
                {
                    isSearch=YES;
                    [self filterDate:self.date SortBy:@"Date"];
                }
                else if(self.strSortByNominateID != nil)
                {
                    [self filterDate:self.date SortBy:@"ID"];
                    isSearch=YES;
                }
                else
                {
                    isSearch=NO;
                    
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self showIndicator:false];

                    if(self.date!=nil || self.strSortByNominateID != nil)
                    {
                        if(searchResults == nil || searchResults.count == 0)
                        {
                             [igUtility showOKAlertWithTitle:kStringError message:@"No training available."];
                        }
                    }
                    [self.tableView reloadData];
                });
            }
        }
        else{
            [self showIndicator:false];
             [igUtility showOKAlertWithTitle:kStringError message:@"No training available."];
        }
        
        } withFailureHandler:^(NSError *error) {
        
        [self showIndicator:false];
        [igUtility showOKAlertWithTitle:kStringError message:[igUtility errorDescription:error]];
        }
    ];
}
-(void)filterDate :(NSDate *)date SortBy:(NSString*)type
{
    if([type isEqualToString:@"Date"])
    {
        NSDateFormatter * df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"dd MMM yyyy"];
        NSString * depResult = [df stringFromDate:date];
       
            NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"date contains[c] %@", depResult];
            searchResults = [_trainings filteredArrayUsingPredicate:resultPredicate];
    }
    else{
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"isNominated == YES"];
        searchResults = [_trainings filteredArrayUsingPredicate:resultPredicate];
    }
    //[self.tableView reloadData];
}
#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
 //   igTTrainingModel *trainingModel;
    igTEmployeeTraining *trainingModel;

    
    if(isSearch)
    { if(searchResults!=nil && searchResults.count!=0)
    {
        trainingModel  =[searchResults objectAtIndex:indexPath.row];
        if(trainingModel.isNominated)
            
            return 185;//176
        else
            return 157;//148
        ;
    }
    }
    else
    {
        trainingModel  =[_trainings objectAtIndex:indexPath.row];
        if(trainingModel.isNominated)
            return 185;//230
        else
            return 157;
    }
    return 185;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //    return [_testArray[section] count];
    if(isSearch)
    { if(searchResults!=nil && searchResults.count!=0)
    {
        return [searchResults count];
    }
        return 0;
    }
    else
        return [_trainings count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return nil;
}


#pragma mark - UIRefreshControl Selector

- (void)toggleCells:(UIRefreshControl*)refreshControl
{
    [refreshControl beginRefreshing];
   
    self.refreshControl.tintColor = [UIColor blueColor];
    [self.tableView reloadData];
    [refreshControl endRefreshing];
}

#pragma mark - UIScrollViewDelegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    igTEmployeeTraining *training;
    if(isSearch)
    {
        training=[searchResults objectAtIndex:indexPath.row];

    }
    else
    {
        training=[_trainings objectAtIndex:indexPath.row];

    }
    igTrainingFeedbackOverlay *overlayVC = (igTrainingFeedbackOverlay *)[self.storyboard instantiateViewControllerWithIdentifier:@"igTrainingOverlay"];
    overlayVC.objTraining  = training;
    [self presentViewController:overlayVC animated:YES completion:nil];
    

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    igTTrainingTableViewCell  *cell = [self.tableView dequeueReusableCellWithIdentifier:@"UMCell" forIndexPath:indexPath];

    // optionally specify a width that each set of utility buttons will share
    cell.delegate = self;
   // UIView *view = [[UIView alloc] initWithFrame:CGRectMake(15, 15, self.view.bounds.size.width - 30, self.view.bounds.size.height - 30)];
    
    //view.backgroundColor = [UIColor whiteColor];
    cell.trainingCell.layer.cornerRadius = 5.f;
    cell.backgroundColor = [UIColor clearColor];
    
    
    
    [cell.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [cell.layer setShadowRadius:3];
    [cell.layer setShadowOpacity:1.0];
    [cell.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
    
    UISwipeGestureRecognizer* recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    [recognizer setDirection:UISwipeGestureRecognizerDirectionLeft+UISwipeGestureRecognizerDirectionRight];
    for (UIGestureRecognizer *gesture in cell.gestureRecognizers) {
        [cell removeGestureRecognizer:gesture];
    }

      self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
   //        cell.label.text = [NSString stringWithFormat:@"Section: %ld, Seat: %ld", (long)indexPath.section, (long)indexPath.row];
   // igTTrainingModel *training;
   
     igTEmployeeTraining *training;
    cell.apporovedBtn.titleLabel.textAlignment = NSTextAlignmentCenter;



    if(isSearch)
    { if(searchResults!=nil && searchResults.count!=0)
    {
        training=[searchResults objectAtIndex:indexPath.row];
        
       cell.titleLabel.text=training.topic;
        cell.dateLabel.text=[NSString stringWithFormat:@"%@-%@",training.date,training.endDate];
        cell.timeLabel.text=training.time;
        cell.prerequisite.text=training.prerequisite;
        cell.trainingMode.text=training.trainingMode;
        cell.trainer.text=training.trainerName;
        cell.apporovedBtn.titleLabel.text = training.nominationStatus;

       // [cell.apporovedBtn.titleLabel.textAlignment: NSTextAlignmentCenter];


        if([training.trainingOccurance isEqualToString:@"R"])
           cell.trainingVenue.text=training.webminarLocation;
        else
            cell.trainingVenue.text=training.venue;
        if(!training.isNominated)
        {
            [cell setLeftUtilityButtons:[self leftButtons] WithButtonWidth:65.0f];
            [cell setRightUtilityButtons:[self rightButtons] WithButtonWidth:0.0f];
            CGRect rect = cell.trainingCell.frame;
            rect.size.height =142;//136;
            cell.trainingCell.frame = rect;

            
        }
        else
        {
            [cell setRightUtilityButtons:[self setRightUtilityButtonsWithAddCalender] WithButtonWidth:80.0f];
            [cell setLeftUtilityButtons:[self leftButtons] WithButtonWidth:0.0f];
            CGRect rect = cell.trainingCell.frame;
            rect.size.height =170;//164;
            cell.trainingCell.frame = rect;
            
        }
        if(training.isTrainingDateGone)
        {
            [cell addGestureRecognizer:recognizer];
            [cell setRightUtilityButtons:[self rightButtons] WithButtonWidth:0.0f];
            [cell setLeftUtilityButtons:[self leftButtons] WithButtonWidth:0.0f];
            
        }
        
    }
        
    }
    else
    {
        training=[_trainings objectAtIndex:indexPath.row];
        cell.titleLabel.text=training.topic;
        cell.dateLabel.text=[NSString stringWithFormat:@"%@-%@",training.date,training.endDate];
        cell.timeLabel.text=training.time;
        cell.prerequisite.text=training.prerequisite;
        cell.trainingMode.text=training.trainingMode;
        cell.trainer.text=training.trainerName;
        if([training.trainingOccurance isEqualToString:@"R"])
            cell.trainingVenue.text=training.webminarLocation;
        else
            cell.trainingVenue.text=training.venue;
        if(training.isNominated)
        {

            [cell setRightUtilityButtons:[self setRightUtilityButtonsWithAddCalender] WithButtonWidth:65.0f];
            [cell setLeftUtilityButtons:[self leftButtons] WithButtonWidth:0.0f];
            CGRect rect = cell.trainingCell.frame;
            rect.size.height =170;//164;
            cell.trainingCell.frame = rect;
            
            
        }
        else
        {
            [cell setLeftUtilityButtons:[self leftButtons] WithButtonWidth:65.0f];
            [cell setRightUtilityButtons:[self rightButtons] WithButtonWidth:0.0f];
            CGRect rect = cell.trainingCell.frame;
            rect.size.height =142;//136;
            cell.trainingCell.frame = rect;
           
        }
        if(training.isTrainingDateGone)
        {
            [cell addGestureRecognizer:recognizer];
            [cell setRightUtilityButtons:[self rightButtons] WithButtonWidth:0.0f];
            [cell setLeftUtilityButtons:[self leftButtons] WithButtonWidth:0.0f];
        }
        
        
    }
    if (training.isNominated) {
         [self buttonsCurve:cell];
    }
   

    return cell;
    
}

-(void)handleSwipe:(id)sender
{
    [igUtility showOKAlertWithTitle:kStringNominateError message:@"You can Nominate/Cancel your nomination before 24 hours of Training Start time."];
}

-(void)buttonsCurve:(igTTrainingTableViewCell *)cell
{
    cell.apporovedBtn.layer.cornerRadius = (cell.apporovedBtn.frame.size.height/2.0f);
    cell.apporovedBtn.layer.borderWidth = 1;
    cell.apporovedBtn.layer.borderColor = [UIColor clearColor].CGColor;
    
    cell.nominatedBtn.layer.cornerRadius = (cell.nominatedBtn.frame.size.height/2.0f);
    cell.nominatedBtn.layer.borderWidth = 1;
    cell.nominatedBtn.layer.borderColor = [UIColor clearColor].CGColor;
}

- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
                                                 icon:[UIImage imageNamed:@"cancel-icon.png"]];
    
    return rightUtilityButtons;
}

- (NSArray *)leftButtons
{
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    
    [leftUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.07 green:0.75f blue:0.16f alpha:1.0]
                                                icon:[UIImage imageNamed:@"nominate-icon.png"]];

    
    return leftUtilityButtons;
}

-(NSArray*)setRightUtilityButtonsWithAddCalender
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
                                                 icon:[UIImage imageNamed:@"cancel-icon.png"]];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.07 green:0.75f blue:0.16f alpha:1.0]
                                                icon:[UIImage imageNamed:@"online-calendar.png"]];
    
    return rightUtilityButtons;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Set background color of cell here if you don't want default white
}

#pragma mark - SWTableViewDelegate

- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state
{
    switch (state) {
        case 0:
            NSLog(@"utility buttons closed");
            break;
        case 1:
            NSLog(@"left utility buttons open");
            break;
        case 2:
            NSLog(@"right utility buttons open");
            break;
        default:
            break;
    }
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index
{
    switch (index) {
        case 0:
        {
            kAlertAndReturnIfNetworkNotReachable
            [self showIndicator:true];
            WebServiceRequestHelper * helper=[[ WebServiceRequestHelper alloc]init];
          
            NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
            igTEmployeeTraining *emp;
            if(isSearch)
            {
                 emp= [searchResults objectAtIndex:cellIndexPath.row];
            }
                else
                {
            emp= [_trainings objectAtIndex:cellIndexPath.row];
                }
            
            [helper requestForNominate:[IGUserDefaults sharedInstance].employee.staffID andTrainingId:emp.trainingID withSucceHandler:^(NSString *message) {
               [FIRAnalytics logEventWithName:@"Training_nomination" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"Nominaed_by-%@",[IGUserDefaults sharedInstance].employee.employeeNewID]}];
                [IGAnalytics trackEventWith:@"iOS" action:@"Training_nomination" actionName:[NSString stringWithFormat:@"Nominated by:%@_%@",[IGUserDefaults sharedInstance].employee.employeeNewID,[IGUserDefaults sharedInstance].userName]];

                dispatch_async(dispatch_get_main_queue(), ^{
                    [self showIndicator:false];
                    [igUtility showOKAlertWithTitle:@"Info" message:message];
                    [self requestForTrainingList];
                });
                
               
            } withFailureHandler:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self showIndicator:false];
                    [igUtility showOKAlertWithTitle:kStringError message:[igUtility errorDescription:error]];
                });
            }];
            [cell hideUtilityButtonsAnimated:YES];

            NSLog(@"left button 0 was pressed");
        }
            break;
        case 1:
            NSLog(@"left button 1 was pressed");
            break;
        case 2:
            NSLog(@"left button 2 was pressed");
            break;
        case 3:
            NSLog(@"left btton 3 was pressed");
        default:
            break;
    }
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    kAlertAndReturnIfNetworkNotReachable
    
    switch (index)
    {
        case 0:
        {
            self.cancelView.transform = CGAffineTransformMakeScale(0.01f, 0.01f);
            self.cancelView.cmdDelegate = self;
            self.selectCell = cell;
            // Add to the view hierarchy
            [self.navigationController.view addSubview:self.cancelView];
            
          /*  UIView *subView=_cancelView;
            UIView *parent=self.view;
            
            subView.translatesAutoresizingMaskIntoConstraints = NO;
            
            //Trailing
            NSLayoutConstraint *trailing =[NSLayoutConstraint
                                           constraintWithItem:subView
                                           attribute:NSLayoutAttributeTrailing
                                           relatedBy:NSLayoutRelationEqual
                                           toItem:parent
                                           attribute:NSLayoutAttributeTrailing
                                           multiplier:1.0f
                                           constant:0.f];
            
            //Leading
            
            NSLayoutConstraint *leading = [NSLayoutConstraint
                                           constraintWithItem:subView
                                           attribute:NSLayoutAttributeLeading
                                           relatedBy:NSLayoutRelationEqual
                                           toItem:parent
                                           attribute:NSLayoutAttributeLeading
                                           multiplier:1.0f
                                           constant:0.f];
            
            //Bottom
            NSLayoutConstraint *bottom =[NSLayoutConstraint
                                         constraintWithItem:subView
                                         attribute:NSLayoutAttributeBottom
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:parent
                                         attribute:NSLayoutAttributeBottom
                                         multiplier:1.0f
                                         constant:0.f];
            
            //Height to be fixed for SubView same as AdHeight
          NSLayoutConstraint *height = [NSLayoutConstraint
                                          constraintWithItem:subView
                                          attribute:NSLayoutAttributeHeight
                                          relatedBy:NSLayoutRelationEqual
                                          toItem:nil
                                          attribute:NSLayoutAttributeNotAnAttribute
                                          multiplier:0
                                          constant:ADHeight];
            
            
            [parent addConstraint:trailing];
            [parent addConstraint:bottom];
            [parent addConstraint:leading];
            
            [subView addConstraint:height];*/
            
            //[self.navigationController.view layoutSubviews];
            [UIView animateWithDuration:0.5
                             animations:
             ^{
                 self.cancelView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
                 
             }
             ];
            break;
        }
        case 1:
        {

            [self addEventToCalender:cell];
        }
        default:
            break;
    }
}

-(void)addEventToCalender:(SWTableViewCell*)cell
{
    NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
    igTEmployeeTraining *emp;
 
    
    if(isSearch)
    {
        emp= [searchResults objectAtIndex:cellIndexPath.row];
    }
    else
    {
        emp= [_trainings objectAtIndex:cellIndexPath.row];
    }
    EKEventStore *store = [EKEventStore new];

    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (!granted) {
            
            return; }
        EKEvent *event = [EKEvent eventWithEventStore:store];
        event.title =emp.topic;
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];//@"dd MMM yyyy"];
        NSDate *startDate = [dateFormat dateFromString:emp.startDateTime];
        event.startDate = startDate;
        
        NSDate *endDate = [dateFormat dateFromString:emp.endDateTime];
        event.endDate = endDate;
        event.calendar = [store defaultCalendarForNewEvents];
        EKAlarm *alarm = [EKAlarm alarmWithAbsoluteDate:[NSDate dateWithTimeInterval:-900 sinceDate:event.startDate]];
        // Add the alarm to the event.
        [event addAlarm:alarm];
        NSError *err = nil;
        BOOL isExist = [self isEventExist:startDate EndDate:endDate Title:emp.topic];
        if(!isExist)
        {
        [store saveEvent:event span:EKSpanFutureEvents commit:YES error:&err];
        if(err == nil)
        {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [igUtility showOKAlertWithTitle:@"Event" message:@"Training Event  has been added to your calender"];
                    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:cellIndexPath] withRowAnimation:UITableViewRowAnimationNone];
                    
                });
        }
        else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [igUtility showOKAlertWithTitle:@"Event" message:@"There is some issue while adding events to your calender. Please try again later"];
                     [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:cellIndexPath] withRowAnimation:UITableViewRowAnimationNone];
                    
                });
                
            }
        }

        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [igUtility showOKAlertWithTitle:@"Event" message:@"Training Event is already added to your calender"];
                 [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:cellIndexPath] withRowAnimation:UITableViewRowAnimationNone];
                
            });

        }
    }];
}

-(BOOL)isEventExist:(NSDate*)startDate EndDate:(NSDate*)endDate Title:(NSString*)eventTitle
{
    EKEventStore *store = [EKEventStore new];

    NSPredicate *predicate = [store predicateForEventsWithStartDate:startDate endDate:endDate calendars:nil];
    NSArray *eventsOnDate = [store eventsMatchingPredicate:predicate];
    
    __block BOOL eventExists = NO;
    
    [eventsOnDate enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        EKEvent *eventToCheck = (EKEvent*)obj;
        
        if([eventTitle isEqualToString:eventToCheck.title])
        {
            eventExists = YES;
            *stop = YES;
        }
    }];
    return eventExists;
}

-(void)removeEvent:(NSString*)startDate EndDate:(NSString*)endDate Title:(NSString*)eventTitle
{
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];//@"dd MMM yyyy"];
    NSDate *beginDate = [dateFormat dateFromString:startDate];

    
    NSPredicate *predicate = [eventStore predicateForEventsWithStartDate:beginDate
                                                            endDate:[beginDate dateByAddingTimeInterval:60*60]
                                                          calendars:nil];
    NSArray *eventList = [eventStore eventsMatchingPredicate:predicate];
    
   
    if(eventList != nil && eventList.count >0)
    {
        for (EKEvent *event in eventList) {
            [eventStore removeEvent:event span:EKSpanThisEvent error:nil];
        }
    }
   
}

-(void)cancelNotification:(NSString*)cancelReason State:(BOOL)isCancel
{
    [self.cancelView removeFromSuperview];
    self.cancelView.cmdDelegate = nil;
   if(!isCancel)
   {
       kAlertAndReturnIfNetworkNotReachable
       [self showIndicator:true];
       
        WebServiceRequestHelper * helper=[[ WebServiceRequestHelper alloc]init];
        NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:self.selectCell];
        igTEmployeeTraining *emp;
        if(isSearch)
        {
            emp= [searchResults objectAtIndex:cellIndexPath.row];
        }
        else
        {
            emp= [_trainings objectAtIndex:cellIndexPath.row];
        }
        [helper requestForCancellingTraining:[IGUserDefaults sharedInstance].employee.staffID andTrainingId:emp.trainingID withCancelMessage:cancelReason withSucceHandler:^(NSString *message) {
            
            [FIRAnalytics logEventWithName:@"Training_nomination_cancel" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"Canceled_by-%@",[IGUserDefaults sharedInstance].employee.employeeNewID]}];
             [IGAnalytics trackEventWith:@"iOS" action:@"Training_nomination_cancellation" actionName:[NSString stringWithFormat:@"Cancelled by:%@_%@",[IGUserDefaults sharedInstance].employee.employeeNewID,[IGUserDefaults sharedInstance].userName]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self showIndicator:false];
                
                [self removeEvent:emp.startDateTime EndDate:emp.endDate Title:emp.topic];
                [igUtility showOKAlertWithTitle:@"Info" message:message];
                [self requestForTrainingList];
                
                
            });
        } withFailureHandler:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self showIndicator:false];
                
                [igUtility showOKAlertWithTitle:kStringError message:[igUtility errorDescription:error]];
            });
            
        }];
        [self.selectCell hideUtilityButtonsAnimated:YES];
   }
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    // allow just one cell's utility button to be open at once
    return YES;
}

- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state
{
    switch (state) {
        case 1:
            // set to NO to disable all left utility buttons appearing
            return YES;
            break;
        case 2:
            // set to NO to disable all right utility buttons appearing
            return YES;
            break;
        default:
            break;
    }
    
    return YES;
}


#pragma mark - bar button item click

-(void)configureNavigationBar
{
//    self.navigationController.navigationBar.barTintColor =    [UIColor colorWithRed:23.0/255.0  green:134.0/255.0 blue:216.0f/255.0 alpha:1.0f];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationItem.title = self.navigationTitle;
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"pattern_bg"]]];
    UIImage *image = [UIImage imageNamed:@"menu-icon.png"];
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:image
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(showMainMenuBtnAction:)];
    menuButton.tintColor = [UIColor whiteColor];
    
    [menuButton setTarget:self];
    [menuButton setAction:@selector(showMainMenuBtnAction:)];
    if(self.showBackButton)
    {
        UIImage *image = [UIImage imageNamed:@"back-icon.png"];
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:image
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(backButtonAction:)];
        backButton.tintColor = [UIColor whiteColor];
        
        
        self.navigationItem.leftBarButtonItem = backButton;
       

        
    }
    else
    {
        UIImage *image = [UIImage imageNamed:@"menu-icon.png"];
        UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:image
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(showMainMenuBtnAction:)];
        menuButton.tintColor = [UIColor whiteColor];
        
        [menuButton setTarget:self];
        [menuButton setAction:@selector(showMainMenuBtnAction:)];
    self.navigationItem.leftBarButtonItem = menuButton;
    }
    if(![self.navigationTitle isEqualToString:@"My Trainings"])
    {
        UIBarButtonItem *toggleViewButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"online-calendar.png"] style:UIBarButtonItemStylePlain
                                                                        target:self
                                                                        action:@selector(toggleView:)];
        toggleViewButton.tintColor = [UIColor whiteColor];
        self.navigationItem.rightBarButtonItem = toggleViewButton;
    }
    
    
}

- (void)showMainMenuBtnAction:(id)sender
{
    [self.sideMenuViewController presentMenuViewController];
}

-(IBAction)toggleView:(id)sender{

    igTRSDFDatePickerViewController *calendarVC = [[igTRSDFDatePickerViewController alloc] init];
    calendarVC.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    calendarVC.calendar.locale = [NSLocale currentLocale];
    self.navigationController.viewControllers = @[calendarVC];
}



#pragma mark -search bar delgates
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if([searchBar.text isEqualToString:@""])
    {
        isSearch=NO;
    }
    else
    {
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"topic contains[c] %@", self.searchBar.text];
        searchResults = [_trainings filteredArrayUsingPredicate:resultPredicate];
        isSearch=YES;
    }
    [self.tableView reloadData];
    
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
    [self.tableView reloadData];
    
}// called when text ends editing
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
    [self.view endEditing:YES];
    if([searchBar.text isEqualToString:@""])
    {
        isSearch=NO;
    }
    else
    {
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"title contains[c] %@", self.searchBar.text];
        searchResults = [_trainings filteredArrayUsingPredicate:resultPredicate];
        isSearch=YES;
    }
    [self.tableView reloadData];
    
}

//Show activity indicator while fetching service or performing any action
-(void)showIndicator:(BOOL)isTrue
{
    if(isTrue)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.transparentView removeFromSuperview];
            [self.navigationController.view addSubview:self.transparentView];
            
            _spinner.hidden = NO;
            [_spinner startAnimating];
        });

    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.transparentView removeFromSuperview];
            
            _spinner.hidden = YES;
            [_spinner stopAnimating];
        });
    }
}

-(void)backButtonAction :(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
