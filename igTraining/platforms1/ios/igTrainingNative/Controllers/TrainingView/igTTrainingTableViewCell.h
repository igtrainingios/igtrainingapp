//
//  UMTableViewCell.h
//  SWTableViewCell
//
//  Created by Matt Bowman on 12/2/13.
//  Copyright (c) 2013 Chris Wendel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

/*
 *  Example of a custom cell built in Storyboard
 */
@interface igTTrainingTableViewCell : SWTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *trainingContentView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIButton *apporovedBtn;
@property (weak, nonatomic) IBOutlet UIButton *nominatedBtn;
@property (weak, nonatomic) IBOutlet UIView *trainingCell;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *trainer;
@property (weak, nonatomic) IBOutlet UILabel *trainingMode;
@property (weak, nonatomic) IBOutlet UILabel *prerequisite;
- (IBAction)nominateBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *trainingVenue;

@end
