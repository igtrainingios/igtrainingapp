//
//  ViewController.h
//  SWTableViewCell
//
//  Created by Chris Wendel on 9/10/13.
//  Copyright (c) 2013 Chris Wendel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
#import "GMSpinKitView.h"
#import "CancelNominationView.h"
#import "TransparentView.h"

@interface igTTrainingViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate,SWTableViewCellDelegate,NotifyCancelNomination,UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong,nonatomic) NSString *navigationTitle;
@property (strong,nonatomic) NSDate *date;

@property (strong,nonatomic) UIView *containerView;
@property(assign)bool showBackButton;
@property(nonatomic,strong) NSArray *trainings;
@property (nonatomic, strong) GMSpinKitView *spinner;
@property (nonatomic, strong)CancelNominationView* cancelView;
@property (nonatomic, strong)TransparentView* transparentView;
@property (weak, nonatomic)SWTableViewCell * selectCell;
@property (weak, nonatomic)NSString * strSortByNominateID;

-(NSArray*)setRightUtilityButtonsWithAddCalender;




-(void)cancelNotification:(NSString*)cancelReason State:(BOOL)isCancel;


@end
