//
//  UMTableViewCell.m
//  SWTableViewCell
//
//  Created by Matt Bowman on 12/2/13.
//  Copyright (c) 2013 Chris Wendel. All rights reserved.
//

#import "igTTrainingTableViewCell.h"
#import "WebServiceRequestHelper.h"

@implementation igTTrainingTableViewCell

- (IBAction)nominateBtnAction:(id)sender {
 
}
-(void)buttonsCurve
{
    self.apporovedBtn.layer.cornerRadius = (self.apporovedBtn.frame.size.height/2.0f);
    self.apporovedBtn.layer.borderWidth = 1;
    self.apporovedBtn.layer.borderColor = [UIColor clearColor].CGColor;
    
    self.nominatedBtn.layer.cornerRadius = (self.nominatedBtn.frame.size.height/2.0f);
    self.nominatedBtn.layer.borderWidth = 1;
    self.nominatedBtn.layer.borderColor = [UIColor clearColor].CGColor;
}

@end
