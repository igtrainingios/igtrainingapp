//
//  IGCommentsVCViewController.m
//  igTraining
//
//  Created by Shipra Chauhan on 09/11/16.
//  Copyright © 2016 Infogain. All rights reserved.
//

#import "IGCommentsVCViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "igTFeedbackRatingInfo.h"
#import "Constants.h"
#import "IGUserDefaults.h"
//#import "igUtility.h"
#import "AppDelegate.h"

@interface IGCommentsVCViewController ()<UINavigationBarDelegate>


- (IBAction)saveBtnClkd:(UIButton *)sender;
- (IBAction)cancleBtnClkd:(UIBarButtonItem *)sender;
@property (weak, nonatomic) IBOutlet UINavigationBar *navigation;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollVw;
@property (weak, nonatomic) IBOutlet UITextField *txtHardware;
@property (weak, nonatomic) IBOutlet UITextField *txtSoftware;
@property (weak, nonatomic) IBOutlet UITextView *txtOthers;
@property (weak, nonatomic) IBOutlet UITextField *txtClassroom;

@property (weak, nonatomic) IBOutlet UILabel *courseTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblTrainerName;
@property (weak, nonatomic) IBOutlet UILabel *lblTrainingDate;

@property (weak, nonatomic) IBOutlet UILabel *lblHeaderTypeTwo;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderTypeThree;

@property (weak, nonatomic) IBOutlet UILabel *lblHardwareLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblSoftwareLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblClassroomLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblOtherCommentsLabel;
@property(weak, nonatomic) UIView *activeTextView;


@end

@implementation IGCommentsVCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
      //[igUtility setScreenName:@"Comments View Controller" ClassName:NSStringFromClass([self class])];
     [self.navigation setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"pattern_bg"]]];
    [_scrollVw setContentSize:CGSizeMake(320, 600)];
    
    self.lblTrainerName.text = self.strTrainerName;
    self.lblTrainingDate.text = self.strTrainingDate;
    self.courseTitleLabel.text = self.strCourseName;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardDidHideNotification object:nil];
    
    UITapGestureRecognizer *tapScroll = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ViewTouched)];
    tapScroll.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapScroll];
    
    [self drawCommentsView];

    

    // [self configureNavigatonBar];
    // Do any additional setup after loading the view.
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardDidHideNotification];
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardDidShowNotification];
}

-(void)ViewTouched
{
    [self.view endEditing:YES];
}

-(void)drawCommentsView
{
    @try {

        igTFeedbackRatingInfo* info = [[self.dictComments objectForKey:kFeedbackTemplete4]firstObject];
        
        self.lblHeaderTypeTwo.text = info.feedbackType;
        NSArray* arrObjects = [self.dictComments objectForKey:kFeedbackTemplete4];
        
        for(int count  = 0; count < [arrObjects count]; count++)
        {
            info = [arrObjects objectAtIndex:count];
            if(count == 0)
            {
                self.lblHardwareLabel.text = info.feedbackSubType;
            }
            if(count == 1)
            {
                self.lblClassroomLabel.text = info.feedbackSubType;
            }
            if( count == 2)
            {
                self.lblSoftwareLabel.text = info.feedbackSubType;
            }
        }
        
        info = [[self.dictComments objectForKey:kFeedbackTemplete5]firstObject];
        
        self.lblHeaderTypeThree.text = info.feedbackType;
        NSArray* arrOtherComments = [self.dictComments objectForKey:kFeedbackTemplete5];
        
        for(int count  = 0; count < [arrOtherComments count]; count++)
        {
            info = [arrOtherComments objectAtIndex:count];
            if(count == 0)
            {
                self.lblOtherCommentsLabel.text = info.feedbackSubType;
            }
        }

    } @catch (NSException *exception) {
        
        NSLog(@"Exception occured");
        
    } @finally {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)configureNavigatonBar{
    //[self.navigation setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"pattern_bg"]]];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"pattern_bg"]]];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:18.0];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.text = [NSString stringWithFormat:@"Comment"];
    label.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = label;
    [label sizeToFit];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.opaque = YES;
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Medium" size:17.0f]};
    
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    UIImage *image = [UIImage imageNamed:@"close.png"];
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:image
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(showMainMenuBtnAction:)];
    menuButton.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = menuButton;
 
}

-(UIBarPosition)positionForBar:(id <UIBarPositioning>)bar
{
    return UIBarPositionTopAttached;
}

//- (IBAction)showMainMenuBtnAction:(id)sender
//{
//    //[self.navigationController popViewControllerAnimated:YES];
//    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
//}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField        // return NO to disallow editing.
{
    return true;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 35)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveBtnClkd:)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    textField.inputAccessoryView = numberToolbar;
    //  if([textField isKindOfClass:[_txtHardware class]])
    {
        self.activeTextView = textField;
    }
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.activeTextView = nil;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
     if ( textField == self.txtHardware){
        [_txtClassroom becomeFirstResponder];
    }else if ( textField == self.txtClassroom){
        [self.txtSoftware becomeFirstResponder];
    }else if ( textField == self.txtSoftware){
        [self.txtOthers becomeFirstResponder];
    }
    else{
        [textField resignFirstResponder];
    }
    return YES;
    // [textField resignFirstResponder];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveBtnClkd:)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    textView.inputAccessoryView = numberToolbar;
    self.activeTextView = textView;
    return true;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.activeTextView = textView;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    self.activeTextView = nil;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return true;
}


// Called when the UIKeyboardDidShowNotification is received
- (void)keyboardWasShown:(NSNotification *)aNotification
{
    // keyboard frame is in window coordinates
    NSDictionary *userInfo = [aNotification userInfo];
    CGRect keyboardInfoFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    // get the height of the keyboard by taking into account the orientation of the device too
    CGRect windowFrame = [self.view.window convertRect:self.view.frame fromView:self.view];
    CGRect keyboardFrame = CGRectIntersection (windowFrame, keyboardInfoFrame);
    CGRect coveredFrame = [self.view.window convertRect:keyboardFrame toView:self.view];
    
    // add the keyboard height to the content insets so that the scrollview can be scrolled
    UIEdgeInsets contentInsets = UIEdgeInsetsMake (0.0, 0.0, coveredFrame.size.height, 0.0);
    _scrollVw.contentInset = contentInsets;
    _scrollVw.scrollIndicatorInsets = contentInsets;
    
    // make sure the scrollview content size width and height are greater than 0
    [_scrollVw setContentSize:CGSizeMake (_scrollVw.contentSize.width, _scrollVw.contentSize.height)];
    
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    //    if (!CGRectContainsPoint(aRect, self.activeTextView.frame.origin) ) {
    //        [_scrollVw scrollRectToVisible:self.activeTextView.frame animated:YES];
    //    }
    
    // scroll to the text view
    if (!CGRectContainsPoint(aRect, self.activeTextView.frame.origin) && self.activeTextView != self.txtOthers) {
        [_scrollVw scrollRectToVisible:self.activeTextView.frame animated:YES];
    }
    else{
        CGRect rect = self.activeTextView.frame;
        rect.origin.y   = rect.origin.y-80;
        [_scrollVw scrollRectToVisible:rect animated:YES];
    }
    
    
}

// Called when the UIKeyboardWillHideNotification is received
- (void)keyboardWillBeHidden:(NSNotification *)aNotification
{
    // scroll back..
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _scrollVw.contentInset = contentInsets;
    _scrollVw.scrollIndicatorInsets = contentInsets;
    CGRect rect = self.lblHeaderTypeTwo.frame;
    [_scrollVw scrollRectToVisible:rect animated:YES];
}

-(void)doneWithNumberPad
{
    [self.activeTextView resignFirstResponder];
}


- (IBAction)saveBtnClkd:(UIButton *)sender {
    
    AppDelegate* appDelegate = kAppDelegate;
    appDelegate.strCommentPayload = @"";
    NSDictionary* dictonary = @{
                                @"screen_name": @"Comments View controller",
                                @"event_name": NSStringFromSelector(_cmd)
                                };
  //  [igUtility setEventName:NSStringFromSelector(_cmd) Parameters:dictonary];
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
    NSArray* arrfeedbackTemplate1 = [self.dictComments objectForKey:kFeedbackTemplete4];
    int count = 0;
    NSString* strCommentPayload = @"";
    for(igTFeedbackRatingInfo* info in arrfeedbackTemplate1)
    {
        if(count == 0)
        strCommentPayload = [strCommentPayload stringByAppendingFormat:@",%@!%@-%@",[IGUserDefaults sharedInstance].employee.staffID,info.mappingID,_txtHardware.text];
        else if(count == 1)
            strCommentPayload = [strCommentPayload stringByAppendingFormat:@",%@!%@-%@",[IGUserDefaults sharedInstance].employee.staffID,info.mappingID,_txtClassroom.text];
        else if(count == 2)
            strCommentPayload = [strCommentPayload stringByAppendingFormat:@",%@!%@-%@",[IGUserDefaults sharedInstance].employee.staffID,info.mappingID,_txtSoftware.text];
        ++count;
    }
     NSArray* arrfeedbackTemplate2 = [self.dictComments objectForKey:kFeedbackTemplete5];
    for(igTFeedbackRatingInfo* info in arrfeedbackTemplate2)
    {
        strCommentPayload = [strCommentPayload stringByAppendingFormat:@",%@!%@-%@",[IGUserDefaults sharedInstance].employee.staffID,info.mappingID,_txtOthers.text];
    }
    appDelegate.strCommentPayload = strCommentPayload;
}


- (IBAction)cancleBtnClkd:(UIBarButtonItem *)sender {
    
    NSDictionary* dictonary = @{
                                @"screen_name": @"Comments View controller",
                                @"event_name": NSStringFromSelector(_cmd)
                                };
    //[igUtility setEventName:NSStringFromSelector(_cmd) Parameters:dictonary];
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(IBAction)actionDoneDutton:(id)sender
{
    [self.activeTextView resignFirstResponder];
}

@end
