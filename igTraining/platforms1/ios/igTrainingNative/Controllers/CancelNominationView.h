//
//  ConcelNominationView.h
//  igTraining
//
//  Created by Sandeep on 29/12/16.
//  Copyright © 2016 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NotifyCancelNomination;


@interface CancelNominationView : UIView<UITextViewDelegate>


@property (weak, nonatomic) IBOutlet UITextView*  txtCancelReason;
@property (weak, nonatomic) IBOutlet UIButton*    btnCancelTraining;
@property (weak, nonatomic) IBOutlet UIButton*    btnDismissView;
@property (weak, nonatomic) id<NotifyCancelNomination>cmdDelegate;

-(IBAction)actionDismissButton:(id)sender;
-(IBAction)actionCancelButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *subView;

@end
@protocol NotifyCancelNomination <NSObject>

-(void)cancelNotification:(NSString*)cancelReason State:(BOOL)isCancel;



@end
