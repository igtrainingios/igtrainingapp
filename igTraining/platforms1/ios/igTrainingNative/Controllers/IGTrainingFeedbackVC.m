//
//  IGTrainingFeedbackVC.m
//  igTraining
//
//  Created by Shipra Chauhan on 09/11/16.
//  Copyright © 2016 Infogain. All rights reserved.
//

#import "IGTrainingFeedbackVC.h"
#import "IGTrainingTableViewCell.h"
#import "IGRatingVC.h"
#import "WebServiceRequestHelper.h"
#import "TransparentView.h"
#import "GMSpinKitView.h"
#import "igUtility.h"
#import "IGUserDefaults.h"
#import "Constants.h"
#import "AppDelegate.h"

@interface IGTrainingFeedbackVC ()
@property (weak, nonatomic) IBOutlet UITableView *trainingTableView;
@property (weak, nonatomic) IBOutlet UILabel *lblNoTrainingAvailable;
@property (weak, nonatomic) IBOutlet UIImageView *imgNoTrainingAvailable;
@property (strong, nonatomic)NSMutableArray* _trainingsArray;
@property (strong, nonatomic)TransparentView* transparentView;
@property (nonatomic, strong) GMSpinKitView *spinner;

@end

@implementation IGTrainingFeedbackVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // [igUtility setScreenName:@"Feedbacks View Controller" ClassName:NSStringFromClass([self class])];
    [self configureNavigatonBar];
    // Do any additional setup after loading the view.
    NSArray* transparentxibContents = [[NSBundle mainBundle] loadNibNamed:@"TransparentView" owner:nil options:nil];
    self.transparentView = [transparentxibContents objectAtIndex:0];
    self.transparentView.frame = self.view.frame;

    
    //[igUtility setScreenName:@"TrainingCalender-ListView" ClassName:NSStringFromClass([self class])];
    
    _spinner = [[GMSpinKitView alloc] initWithColor:[UIColor colorWithRed:23.0/255.0  green:134.0/255.0 blue:216.0f/255.0 alpha:1.0f]];
    
    _spinner.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds) );
    _spinner.hidden = YES;
    [self.transparentView  addSubview:_spinner];
    _lblNoTrainingAvailable.hidden = true;
    

    
    [self getTrainingFeedbackList];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Show activity indicator while fetching service or performing any action
-(void)showIndicator:(BOOL)isTrue
{
    if(isTrue)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.transparentView removeFromSuperview];
            [self.navigationController.view addSubview:self.transparentView];
            
            _spinner.hidden = NO;
            [_spinner startAnimating];
        });
        
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.transparentView removeFromSuperview];
            
            _spinner.hidden = YES;
            [_spinner stopAnimating];
        });
    }
}


//Get Past trainings list from web services
-(void)getTrainingFeedbackList
{
    kAlertAndReturnIfNetworkNotReachable
    WebServiceRequestHelper * helper=[[ WebServiceRequestHelper alloc]init];
    [self showIndicator:true];

    [helper requestForFeedbackTraining:[IGUserDefaults sharedInstance].employee.staffID withSucceHandler:^(NSArray *message)
     {
         self._trainingsArray= [NSMutableArray arrayWithArray:message];
         if(self._trainingsArray.count > 0 && self._trainingsArray != nil)
         {

             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 _lblNoTrainingAvailable.hidden = true;
                 _imgNoTrainingAvailable.hidden = true;


                 [self showIndicator:false];

                 [_trainingTableView reloadData];

             });

             
         }
         else
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 _lblNoTrainingAvailable.hidden = false;
                 _imgNoTrainingAvailable.hidden = false;

                 [self showIndicator:false];
                 [igUtility showOKAlertWithTitle:kStringError message:@"No Training available for rating" ];
                 
             });
         }
     }
    withFailureHandler:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    _lblNoTrainingAvailable.hidden = false;
                    _imgNoTrainingAvailable.hidden = false;


                    [self showIndicator:false];
                    
                    [igUtility showOKAlertWithTitle:kStringError message:[igUtility errorDescription:error]]; });
            }];


}

/*
 Function:getDateFormat
 Description: convert date to ddMMM formate.
 */
-(NSString*)getDateFormat:(NSString*)strDatetoConvert{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString:strDatetoConvert];
    // converting into our required date format
    [dateFormatter setDateFormat:@"dd MMM"];
    NSString *reqDateString = [dateFormatter stringFromDate:date];
    NSLog(@"date is %@", reqDateString);
    return reqDateString;
}


-(void)configureNavigatonBar{
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"pattern_bg"]]];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:18.0];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.text = [NSString stringWithFormat:@"Training Feedback"];
    label.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = label;
    [label sizeToFit];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.opaque = YES;
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Medium" size:17.0f]};
    
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    UIImage *image = [UIImage imageNamed:@"menu-icon.png"];
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:image
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(showMainMenuBtnAction:)];
    menuButton.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = menuButton;
    
}
- (IBAction)showMainMenuBtnAction:(id)sender
{
    [self.sideMenuViewController presentMenuViewController];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self._trainingsArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"IGTrainingTableViewCell";
    IGTrainingTableViewCell *cell = [self.trainingTableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    igTTrainingFeedbackListModel* feedbackModal = [self._trainingsArray objectAtIndex:indexPath.row];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.tableCellSubvw.layer.masksToBounds = FALSE;
    
    cell.tableCellSubvw.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    cell.tableCellSubvw.layer.shadowOffset = CGSizeMake(0.2f, 0.2f);
    cell.tableCellSubvw.layer.shadowOpacity = 0.5f;
    cell.tableCellSubvw.layer.cornerRadius = 2.0f;
    
    cell.dateSubview.layer.masksToBounds = FALSE;
    cell.dateSubview.layer.cornerRadius = 2.0f;
    NSString* startDate = [self getDateFormat:feedbackModal.startDate].uppercaseString;
    [cell configureCell:startDate Topic:feedbackModal.topic Trainer:feedbackModal.trainer];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary* dictonary = @{
                                @"screen_name": @"Feedback View Controller",
                                @"event_name": NSStringFromSelector(_cmd)
                                };
   // [igUtility setEventName:NSStringFromSelector(_cmd) Parameters:dictonary];
    
    NSLog(@"push view with data of index = %@",indexPath);
    IGRatingVC *contentVC = (IGRatingVC *)[self.storyboard instantiateViewControllerWithIdentifier:@"IGRatingVC"];
    igTTrainingFeedbackListModel* feedbackModal = [self._trainingsArray objectAtIndex:indexPath.row];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate* startDate  = [dateFormatter dateFromString:feedbackModal.startDate];
    NSDate* endDate = [dateFormatter dateFromString:feedbackModal.endDate];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];

    NSString* strStartDate = [dateFormatter stringFromDate:startDate];
    
     NSString* strEndDate = [dateFormatter stringFromDate:endDate];
     
    NSString* strDatetoShow = [NSString stringWithFormat:@"%@-%@",strStartDate,strEndDate];
    
    contentVC.strCourseName = feedbackModal.topic;
    contentVC.strTrainerName = feedbackModal.trainer;
    contentVC.strTrainingDate  = strDatetoShow;
    contentVC.strFacultyID = feedbackModal.trainerID;
    contentVC.strTrainingID = feedbackModal.trainingID;
    

    
    [self.navigationController pushViewController:contentVC animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
