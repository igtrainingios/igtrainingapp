//
//  IGRatingVC.h
//  igTraining
//
//  Created by Shipra Chauhan on 09/11/16.
//  Copyright © 2016 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBCircularProgressBarLayer.h"
#import "MBCircularProgressBarView.h"

//@class RSDFDatePickerView;

@interface IGRatingVC : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *trainingTitle;
@property (weak, nonatomic) IBOutlet UILabel *tranerName;
@property (weak, nonatomic) IBOutlet MBCircularProgressBarView *progressBar;
@property (weak, nonatomic) IBOutlet UIView *ratingTableFooter;

//passed data
@property (nonatomic, strong) NSString *strCourseName;
@property (nonatomic, strong) NSString *strTrainerName;
@property (nonatomic, strong) NSString *strTrainingDate;

@property (nonatomic, strong) NSString *strFacultyID;

@property (nonatomic, strong) NSString *strTrainingID;


@end
