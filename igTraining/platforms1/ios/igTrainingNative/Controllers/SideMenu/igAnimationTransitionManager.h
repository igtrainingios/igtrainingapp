//
//  igAnimationTransitionManager.h
//  IGCafe
//
//  Created by Manish on 22/06/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface igAnimationTransitionManager : NSObject <UIViewControllerTransitioningDelegate>

@property (nonatomic, assign) NSTimeInterval transitionDuration;

@end
