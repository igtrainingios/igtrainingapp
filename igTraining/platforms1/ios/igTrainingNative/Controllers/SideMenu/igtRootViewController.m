//
//  ISRootViewController.m
//  iSupport
//
//  Created by Asif on 30/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "igtRootViewController.h"
#import "igTSideMenuViewController.h"
#import "Constants.h"
#import "igTRSDFDatePickerViewController.h"
#import "igTTrainingViewController.h"
#import "AppDelegate.h"
#import "IGUserDefaults.h"

@interface igtRootViewController ()

@end

@implementation igtRootViewController

- (void)awakeFromNib
{
    self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:kContentController_Identifier];

    UINavigationController *contentViewNavigationController = (UINavigationController *)self.contentViewController;
//    igTRSDFDatePickerViewController *calendarVC = [[igTRSDFDatePickerViewController alloc] init];
//    calendarVC.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
//    calendarVC.calendar.locale = [NSLocale currentLocale];
//    igTTrainingViewController *trainingListController =[self.storyboard instantiateViewControllerWithIdentifier:kTrainingViewController_Identifier];
//    trainingListController.navigationTitle = @"Training Calendar";
////    navigationController.viewControllers = @[trainingListController];
//    [contentViewNavigationController setViewControllers:@[trainingListController]];

    AppDelegate* appDelegate = kAppDelegate;
    if(![IGUserDefaults sharedInstance].hasAcceptedTermsAndConditions)
    {
        [appDelegate openTermsAndCondition];
    }
    else
    {
        igTRSDFDatePickerViewController *calendarVC = [[igTRSDFDatePickerViewController alloc] init];
        calendarVC.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        calendarVC.calendar.locale = [NSLocale currentLocale];
        contentViewNavigationController.viewControllers = @[calendarVC];
        [self.sideMenuViewController hideMenuViewController];
        
        self.menuViewController = [self.storyboard instantiateViewControllerWithIdentifier:kSideMenuViewController_Identifier];
        
        
    //    EMWeatherManager *weatherManager = [EMWeatherManager sharedWeatherManager];
    //    /* User Personalized Screen */
    //    [super.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:[weatherManager userPersonalizedScreenImageName]]]];
        
        [super.view setBackgroundColor:[UIColor blackColor]];
        self.delegate = ( igTSideMenuViewController*)self.menuViewController;
    }
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if ( event.subtype == UIEventSubtypeMotionShake )
    {
        // Put in code here to handle shake
//        [self launchCreateNoteScreen];
    }
}
//- (void) launchCreateNoteScreen {
//    
//    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    EMCreateNoteViewController *cnVC = (EMCreateNoteViewController*) [mainStoryBoard instantiateViewControllerWithIdentifier:@"EMCreateNoteViewController"];
//    
//    [self presentViewController:cnVC animated:YES completion:^{
//        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
//    }];
//}


@end
