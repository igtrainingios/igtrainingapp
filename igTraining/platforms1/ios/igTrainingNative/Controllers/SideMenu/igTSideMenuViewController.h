//
//  ISSideMenuViewController.h
//  iSupport
//
//  Created by Asif on 30/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"
#import "LeftMenuView.h"
#import "igAnimationTransitionManager.h"
@interface igTSideMenuViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, RESideMenuDelegate>

@property (strong, nonatomic)IBOutlet UITableView *tableView;
@property (strong, nonatomic)IBOutlet UILabel* lblVersionNumber;
@property(strong ,nonatomic)   LeftMenuView* sectionHeaderCell;

- (IBAction)signOutBtnAction:(id)sender;
- (IBAction)settingsBtnAction:(id)sender;

@end
