//
//  igAnimationTransitionManager.m
//  IGCafe
//
//  Created by Manish on 22/06/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "igAnimationTransitionManager.h"

@interface igForwardAnimator : NSObject <UIViewControllerAnimatedTransitioning>
@property (nonatomic, assign) NSTimeInterval transitionDuration;
@end

@implementation igForwardAnimator

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return MIN(_transitionDuration, 0.3);
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    UIView *fromView = fromViewController.view;
    UIView *toView = toViewController.view;

    UIView *container = [transitionContext containerView];
    [container addSubview:fromView];
    [container addSubview:toView];
    
    NSTimeInterval duration = [self transitionDuration:transitionContext];
    toView.alpha = 0.0f;
    [UIView animateWithDuration:duration animations:^{
        toView.alpha = 1.0f;
    } completion:^(BOOL finished) {
        [transitionContext completeTransition:!transitionContext.transitionWasCancelled];
    }];
    
}

- (void)animationEnded:(BOOL)transitionCompleted
{
 //   DLog(@"Forward anim end");
}

@end


@interface igReverseAnimator : NSObject <UIViewControllerAnimatedTransitioning>
@property (nonatomic, assign) NSTimeInterval transitionDuration;
@end

@implementation igReverseAnimator

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return MIN(_transitionDuration, 0.3);
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    UIView *fromView = fromViewController.view;
    UIView *toView = toViewController.view;

    UIView *container = [transitionContext containerView];
    [container addSubview:toView];
    [container addSubview:fromView];
    
    NSTimeInterval duration = [self transitionDuration:transitionContext];
    [UIView animateWithDuration:duration animations:^{
        fromView.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [transitionContext completeTransition:!transitionContext.transitionWasCancelled];
    }];
}

- (void)animationEnded:(BOOL)transitionCompleted
{
  //  DLog(@"Reverse anim end");
}

@end


@implementation igAnimationTransitionManager
{
    igForwardAnimator *forwardAnimator;
    igReverseAnimator *reverseAnimator;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        forwardAnimator = [[igForwardAnimator alloc] init];
        reverseAnimator = [[igReverseAnimator alloc] init];
    }
    return self;
}

- (void)setTransitionDuration:(NSTimeInterval)transitionDuration
{
    _transitionDuration = transitionDuration;
    forwardAnimator.transitionDuration = transitionDuration;
    reverseAnimator.transitionDuration = transitionDuration;
}


#pragma mark - UIViewControllerTransitioningDelegate

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    return forwardAnimator;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    return reverseAnimator;
}

@end
