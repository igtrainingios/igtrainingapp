//
//  ISSideMenuViewController.m
//  iSupport
//
//  Created by Asif on 30/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "igTSideMenuViewController.h"
#import "UIViewController+RESideMenu.h"
#import "igTRSDFDatePickerViewController.h"
#import "igTTrainingViewController.h"
#import "Constants.h"
#import "igTTutorialRootViewController.h"
#import "WebServiceRequestHelper.h"
#import "UIImage+EMAdditions.h"
#import "igAnimationTransitionManager.h"
#import "IGTrainingFeedbackVC.h"
#import "IGUserDefaults.h"
#import <Instabug/Instabug.h>
#import "UIView+IGCircularView.h"
#import "Constants.h"
#import "igUtility.h"
#import "AppDelegate.h"
#import "NativeViewController.h"

@interface igTSideMenuViewController ()

@property(strong, nonatomic)__block NSDictionary *addressResponse;
@property(strong, nonatomic)__block NSDictionary *weatherResponse;
@property(weak, nonatomic)IBOutlet UIToolbar* toolbar;

@end

@implementation igTSideMenuViewController
{
    NSArray *titles;
    NSArray *images;
    igAnimationTransitionManager *animationManager;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        self.tableView.frame = CGRectMake(30, 100, _tableView.frame.size.width, _tableView.frame.size.height);
    }
    else{
        if(IS_IPHONE_6 || IS_IPHONE_6P || IS_IPHONE_7 || IS_IPHONE_7P){
             self.tableView.frame = CGRectMake(0,50, _tableView.frame.size.width, _tableView.frame.size.height);
        }
        
        else
        
        self.tableView.frame = CGRectMake(0,30, _tableView.frame.size.width, _tableView.frame.size.height);
    }
   //  [igUtility setScreenName:@"Side Menu Controller" ClassName:NSStringFromClass([self class])];
   
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = YES;
    
    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.bounces = YES;
    self.tableView.scrollsToTop = NO;
    self.tableView.alwaysBounceVertical = YES;
    
    titles = @[@"Training Calendar", @"My Trainings", @"Tutorial", @"Training Feedback",@"E-learning",@"Share Feedback"];
    images = @[@"my-training", @"online-calendar", @"documents_icon",@"feedback",@"eLearning",@"share"];
    
    NSString *versionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    self.lblVersionNumber.text = [NSString stringWithFormat:@"v%@",versionString];

}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self.tableView reloadData];
}

#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary* dictonary = @{
                                @"screen_name": @"Side Menu Controller",
                                @"event_name": NSStringFromSelector(_cmd)
                                };
  //  [igUtility setEventName:NSStringFromSelector(_cmd) Parameters:dictonary];
    
    UINavigationController *navigationController = (UINavigationController *)self.sideMenuViewController.contentViewController;
    //    navigationController.viewControllers = @[[self.storyboard instantiateViewControllerWithIdentifier:@"calendarViewController"]];
    
    //[self.sideMenuViewController hideMenuViewController];
    //    return;
    //    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //    UINavigationController *navigationController = (UINavigationController *)self.sideMenuViewController.contentViewController;
    //
    switch (indexPath.row) {
            
        case 0:{
            igTRSDFDatePickerViewController *calendarVC = [[igTRSDFDatePickerViewController alloc] init];
            calendarVC.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            calendarVC.calendar.locale = [NSLocale currentLocale];
            navigationController.viewControllers = @[calendarVC];
            [self.sideMenuViewController hideMenuViewController];
            break;
        }
        case 1:{
            
            igTTrainingViewController *trainingListController =[self.storyboard instantiateViewControllerWithIdentifier:kTrainingViewController_Identifier];
            trainingListController.navigationTitle = @"My Trainings";
            trainingListController.strSortByNominateID = @"NominateID";
            navigationController.viewControllers = @[trainingListController];
            [self.sideMenuViewController hideMenuViewController];
            break;
        }
           
        case 2:
        {
           
            
            
       UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
      igTTutorialRootViewController *tncVC = (igTTutorialRootViewController*) [mainStoryBoard instantiateViewControllerWithIdentifier:@"GMTutorialRootViewController"];
          [self.sideMenuViewController hideMenuViewController];
          [self presentViewController:tncVC animated:YES completion:^{
              
           }];
            break;
            

        }
        case 3:
        {
            IGTrainingFeedbackVC *objFeedback =[self.storyboard instantiateViewControllerWithIdentifier:kFeedbackViewController_Identifier];
            navigationController.viewControllers = @[objFeedback];
            [self.sideMenuViewController hideMenuViewController];

            break;

            
            
        }
        case 4:{
            id viewController = ((AppDelegate*)(UIApplication.sharedApplication.delegate)).viewController;
            
            if (viewController == nil) {
                viewController = [[CDVViewController alloc] init];
            }
           // [self.sideMenuViewController hideMenuViewController];
            //[self presentViewController:viewController animated:YES completion:nil];
            
            navigationController.viewControllers = @[viewController];
            [self.sideMenuViewController hideMenuViewController];
            
           /* UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            NativeViewController *vc = (NativeViewController*) [mainStoryBoard instantiateViewControllerWithIdentifier:@"NativeViewControllerId"];
            navigationController.viewControllers = @[vc];
            [self.sideMenuViewController hideMenuViewController];*/
            break;
        }
        case 5:
        {
            [self.tableView reloadData];
            [Instabug invoke];
   
          
            break;
            
            
            
        }
            ////        case 2:
            ////
            ////            navigationController.viewControllers = @[[self.storyboard instantiateViewControllerWithIdentifier:@"caseListController"]];
            ////                       [self.sideMenuViewController hideMenuViewController];
            ////            break;
            ////        case 3:
            ////                navigationController.viewControllers = @[[self.storyboard instantiateViewControllerWithIdentifier:@"repairListController"]];
            ////                        [self.sideMenuViewController hideMenuViewController];
            ////        break;
            ////
            ////        case 6:{
            ////            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])  {
            ////
            ////            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            ////            picker.delegate = nil;
            ////            picker.allowsEditing = YES;
            ////            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            ////
            ////            [self presentViewController:picker animated:YES completion:NULL];
            ////
            ////            }
            ////            else{
            ////                UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Camera facility is not available!!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            ////                [calert show];
            ////
            ////            }
            ////            break;
            ////        }
            //
            //        case 2:
            //        {
            ////            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
            ////            webViewController.webURL = kLocateURL;
            ////            webViewController.webTitle = @"Locate";
            //            MapViewController *mapViewController = (MapViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"mapViewController"];
            //            NSArray *storesArray = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"MyData" ofType:@"plist"]];
            //            mapViewController.mapPinsArray=storesArray;
            //            mapViewController.isFromSideMenu = YES;
            //
            //
            //
            //            navigationController.viewControllers = [NSArray arrayWithObject:mapViewController];
            //            [self.sideMenuViewController hideMenuViewController];
            //
            //            break;
            //        }
            //        case 4:
            //        {
            //            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
            //            webViewController.webURL = kCheckSupport;
            //            webViewController.webTitle = @"Check Service and Support";
            //            webViewController.isFromFeedback = YES;
            //
            //            navigationController.viewControllers = [NSArray arrayWithObject:webViewController];
            //            [self.sideMenuViewController hideMenuViewController];
            //            break;
            //        }
            //
            //        case 5:
            //        {
            //            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"diags://"]];
            //            
            //            break;
            //        }
            //        case 6:
            //        {
            //                        break;
            //        }
            //
            //            
            //        case 7:
            //        {
            //            ISWebViewController *webViewController = (ISWebViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
            //            webViewController.webURL = @"http://www.apple.com/feedback/";
            //            webViewController.webTitle = @"Send Feedback";
            //            webViewController.isFromFeedback = YES;
            //            
            //            navigationController.viewControllers = [NSArray arrayWithObject:webViewController];
            //            [self.sideMenuViewController hideMenuViewController];
            //            
            //            break;
            //        }
            //
        default:
            break;
    }
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        return 70.0f;
    }
    else{
        return 44.0f;
    }
    
    //return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return [titles count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        
        if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
             cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:22];
            cell.imageView.frame = CGRectMake(cell.imageView.frame.origin.x, cell.imageView.frame.origin.y, 70, 70);
        }
        else{
             cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
             cell.imageView.frame = CGRectMake(cell.imageView.frame.origin.x, cell.imageView.frame.origin.y, 44, 44);
        }
       
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.highlightedTextColor = [UIColor lightGrayColor];
        cell.selectedBackgroundView = [[UIView alloc] init];
    }
    cell.textLabel.text = titles[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:images[indexPath.row]];
  //  cell.imageView.backgroundColor = [UIColor redColor];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        return 300.0f;
    }
    else{
        return 190.0f;
    }
    

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *cellIdentifier = @"headerView";

    self.sectionHeaderCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    self.sectionHeaderCell.employeeIdLabel.text= [IGUserDefaults sharedInstance].employee.employeeNewID ;/*[[NSUserDefaults standardUserDefaults ]valueForKey:@"EmployeeID"];*/
    self.sectionHeaderCell.employeeName.text= [IGUserDefaults sharedInstance].employee.name;/*[[NSUserDefaults standardUserDefaults]valueForKey:@"EmployeeName"];*/
    
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        self.sectionHeaderCell.employeeName.font = [UIFont fontWithName:@"HelveticaNeue" size:22];
    }
    else{
        self.sectionHeaderCell.employeeName.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
    }

    [self performSelector:@selector(setUserImage) withObject:self afterDelay:0.05 ];
    
       return _sectionHeaderCell;
}

-(void)setUserImage{
    [self.sectionHeaderCell.userImageView roundViewWithCornerRaidus:(CGRectGetWidth(self.sectionHeaderCell.userImageView.frame) * 0.5)
                                                        borderWidth:2.0
                                                        borderColor:[UIColor whiteColor]];
    
    self.sectionHeaderCell.userImageView.backgroundColor = [igUtility averageColorFromImage:[IGUserDefaults sharedInstance].employee.image];
    
    
    self.sectionHeaderCell.userImageView.layer.cornerRadius = self.sectionHeaderCell.userImageView.frame.size.height/2;
    self.sectionHeaderCell.userImageView.layer.masksToBounds = false;
    
    self.sectionHeaderCell.userImageView.clipsToBounds = YES;
    self.sectionHeaderCell.userImageView.layer.borderWidth = 3.0f;
    
    self.sectionHeaderCell.userImageView.image = [IGUserDefaults sharedInstance].userImage;

}

-(void)viewDidAppear:(BOOL)animated{
    //bg_port-1.png
    
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_port-1"]];
        _toolbar.barTintColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_port-1"]];
    }
    else{
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"main_bg"]];
        _toolbar.barTintColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"main_bg"]];
    }
    
   
 
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    
//    static NSString *cellIdentifier = @"headerView";
//    
//    __block CustomHeaderView *sectionHeaderCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//    sectionHeaderCell.profileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
//
//    EMLoginManager *loginManager = [EMLoginManager sharedLoginManager];
//    if (loginManager.userProfile) {
//        sectionHeaderCell.profileNameLabel.text = loginManager.userProfile.userName;
//        
//        if (loginManager.userProfile.image)
//            sectionHeaderCell.profileImageView.image = loginManager.userProfile.image;
//
////        [sectionHeaderCell.profileImageView loadFromURLString:loginManager.userProfile.imageURL];
//
////        // Block variable to be assigned in block.
////        __block NSData *imageData;
////        dispatch_queue_t backgroundQueue  = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
////        
////        // Dispatch a background thread for download
////        dispatch_async(backgroundQueue, ^(void) {
////            imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:loginManager.userProfile.imageURL]];
////            
////            UIImage *imageLoad;
////            imageLoad = [[UIImage alloc] initWithData:imageData];
////            
////            // Update UI on main thread
////            dispatch_async(dispatch_get_main_queue(), ^(void) {
////                sectionHeaderCell.profileImageView.image = imageLoad;
////            });
////        });
//
//    }
//
//    if (self.addressResponse) {
//        sectionHeaderCell.stateLabel.text = self.addressResponse[@"City"];
//        sectionHeaderCell.countryLabel.text = self.addressResponse[@"Country"];
//        
//    }
//    
//    if (self.weatherResponse) {
//        if ([[self.weatherResponse objectForKey:@"main"] objectForKey:@"temp"]) {
//            
//            CGFloat temperatureCelcius = ([[[self.weatherResponse objectForKey:@"main"] objectForKey:@"temp"] floatValue] - 272.15f);
//            
//            if ([[NSUserDefaults standardUserDefaults] boolForKey:kUnitsUserDefault]) {
//                sectionHeaderCell.temperatureLabel.text = [NSString stringWithFormat:@"%.0f°C", temperatureCelcius];
//            }
//            else {
//                sectionHeaderCell.temperatureLabel.text = [NSString stringWithFormat:@"%.0f°F", (temperatureCelcius * 1.8f)+32.0f];
//            }
//            
//            sectionHeaderCell.wheatherDetailLabel.text = [[[self.weatherResponse objectForKey:@"weather"] objectAtIndex:0] objectForKey:@"main"];
//            
//            NSString *weatherIcon = [[[self.weatherResponse objectForKey:@"weather"] objectAtIndex:0] objectForKey:@"icon"];
//            sectionHeaderCell.weatherImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png", weatherIcon]];
//
//            [UIView animateWithDuration:2.0f delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
//                [sectionHeaderCell.weatherImageView setAlpha:1.0];
//            } completion:nil];
//
//            /*
//            // Block variable to be assigned in block.
//            __block NSData *imageData;
//            dispatch_queue_t backgroundQueue  = dispatch_queue_create("com.eclipsemedia.imageDownloadQueue", NULL);//dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//            
//            // Dispatch a background thread for download
//            dispatch_async(backgroundQueue, ^(void) {
//                EMWeatherManager *weatherManager = [EMWeatherManager sharedWeatherManager];
//                
//                imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.png", [weatherManager weatherIconBaseURL], weatherIcon]]];
//                
//                UIImage *imageLoad;
//                imageLoad = [[UIImage alloc] initWithData:imageData];
//                
//                // Update UI on main thread
//                dispatch_async(dispatch_get_main_queue(), ^(void) {
//                                         [UIView transitionWithView:self.view
//                                                           duration:2.0f
//                                                            options:UIViewAnimationOptionTransitionCrossDissolve
//                                                         animations:^{
//                                                             sectionHeaderCell.weatherImageView.image = imageLoad;
//                                                         } completion:NULL];
//
//                });
//            });
//            */
//            /* Commented out due to performance issues
//            __weak EMSideMenuViewController *weakSelf = self;
//            EMWeatherManager *weatherManager = [EMWeatherManager sharedWeatherManager];
//            NSURLRequest *imageDataRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.png", [weatherManager weatherIconBaseURL], weatherIcon]]];
//            [NSURLConnection sendAsynchronousRequest:imageDataRequest queue:[[EMCommunicationManager defaultManager] httpOperationQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
//                
//                NSLog(@"called...");
//                        UIImage *imageLoad = [[UIImage alloc] initWithData:data];
//                        dispatch_async(dispatch_get_main_queue(), ^(void) {
//                            sectionHeaderCell.weatherImageView.image = imageLoad;
//                            [sectionHeaderCell.weatherImageView setAlpha:0.0];
//                            [UIView animateWithDuration:2.0f delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
//                                [sectionHeaderCell.weatherImageView setAlpha:1.0];
//                            } completion:nil];
////                            [UIView transitionWithView:weakSelf.view
////                                              duration:2.0f
////                                               options:UIViewAnimationOptionTransitionCrossDissolve
////                                            animations:^{
////                                                sectionHeaderCell.weatherImageView.image = imageLoad;
////                                            } completion:NULL];
//                            
//                            
//                        });
//                
//            }];
//            */
//        }
//    }
//    
//    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(profilePicSelected:)];
//    tapGesture.numberOfTapsRequired = 1;
//    [sectionHeaderCell addGestureRecognizer:tapGesture];
//    //[sectionHeaderCell setBackgroundColor:[UIColor redColor]];
//    return sectionHeaderCell;
//}

//- (void)profilePicSelected:(UITapGestureRecognizer*)tapGesture
//{
//    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    EMUserProfileViewController *userProfileVC = (EMUserProfileViewController*) [mainStoryBoard instantiateViewControllerWithIdentifier:@"EMUserProfileViewController"];
//    [self presentViewController:userProfileVC animated:YES completion:^{
//        
//    }];
//}

//- (UIStatusBarStyle)preferredStatusBarStyle
//{
//    return UIStatusBarStyleLightContent;
//}

#pragma mark -
#pragma mark RESideMenu Delegate

- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"willShowMenuViewController");
    
    [self.tableView reloadData];
    
//    /* Location details */
//    EMLocationManager *locationManager = [EMLocationManager sharedLocationManager];
//    [locationManager fetchActualUserAddressWithCompletionBlock:^(NSDictionary *response, NSError *error) {
//        self.addressResponse = response;
//        [self.tableView reloadData];
//    }];
//    
//    /* Weather details */
//    CLLocation *currentLocation = [locationManager actualUserLocation];
//    EMWeatherManager *weatherManager = [EMWeatherManager sharedWeatherManager];
//    [self.view setBackgroundColor:[UIColor clearColor]];
//
//    [weatherManager fetchForecastOfLocationWithLatitude:[NSString stringWithFormat:@"%f", currentLocation.coordinate.latitude]
//                                            andLogitude:[NSString stringWithFormat:@"%f", currentLocation.coordinate.longitude]
//                                     andCompletionBlock:^(NSDictionary *response, NSError *error) {
//                                         self.weatherResponse = response;
//                                         [self.tableView reloadData];
//
//    }];
//    
}

- (void)sideMenu:(RESideMenu *)sideMenu didShowMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"didShowMenuViewController");
}

- (void)sideMenu:(RESideMenu *)sideMenu willHideMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"willHideMenuViewController");
}

- (void)sideMenu:(RESideMenu *)sideMenu didHideMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"didHideMenuViewController");
}

- (IBAction)signOutBtnAction:(id)sender
{
    AppDelegate *appDelegate = kAppDelegate;
   // [appDelegate setupLoginControllerAsRootViewController
     //];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"Login"];
     [[IGUserDefaults sharedInstance] logout];
    
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    
//    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Logout"     // Event category (required)
//                                                          action:@"Eclipse"  // Event action (required)
//                                                           label:nil         // Event label
//                                                           value:nil] build]];    // Event value
//    
//    [EMActivityBar showWithStatus:@"Logging out.." forAction:@"Logout"];
//    RemoteOperation * operation = [[EMCommunicationManager defaultManager] operationForType:EMLogout];
//    
//    EMLoginManager *loginManager = [EMLoginManager sharedLoginManager];
//    [loginManager signOutSocialMedia];
//    
//    [[EMCommunicationManager defaultManager] addRemoteOperation:operation
//                                         usingCompletionHandler:^(RemoteOperation *operation, NSError *error) {
//                                             
//                                             if(operation != nil && error == nil)
//                                             {
//                                                 [EMActivityBar showSuccessWithStatus:@"Welcome to Eclipse!" forAction:@"Logout"];
//                                                 
//                                             }
//                                             
//                                             if (error)
//                                             {
//                                                 [EMActivityBar showErrorWithStatus:@"Logout Failed!" forAction:@"Logout"];
//                                             }
//                                             
//                                             loginManager.userProfile = [[EMUser alloc] init];
//                                             
//                                             EMDataManager *dataManager = [EMDataManager sharedInstance];
//                                             [dataManager resetDataManager];
//                                             
//                                             [self.navigationController popToRootViewControllerAnimated:YES];
//
//                                         }];
//     
}

- (IBAction)settingsBtnAction:(id)sender
{
    AppDelegate *appDelegate = kAppDelegate;
    //[appDelegate setupLoginControllerAsRootViewController
     //];
    self.navigationController.navigationBarHidden = NO;
    
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"Login"];
    [[IGUserDefaults sharedInstance] logout];

//    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    EMSettingsViewController *settingsVC = (EMSettingsViewController*) [mainStoryBoard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
//    [self presentViewController:settingsVC animated:YES completion:^{
//        
//    }];
}

- (IBAction)unwind:(UIStoryboardSegue *)segue
{
    NSLog(@"gaya");
}
- (UIImage *)imageFromView:(UIView *)view
{
    UIImage *image = nil;
    UIGraphicsBeginImageContext(view.bounds.size);
    BOOL success = [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:NO];
    if (success) {
        image = UIGraphicsGetImageFromCurrentImageContext();
    }
    UIGraphicsEndImageContext();
    return image;
}


@end
