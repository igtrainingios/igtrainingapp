//
//  LeftMenuView.h
//  igTraining
//
//  Created by Niharika on 10/03/15.
//  Copyright (c) 2015 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftMenuView : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *employeeIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *employeeName;

@end
