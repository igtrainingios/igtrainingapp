//
//  LeftMenuView.m
//  igTraining
//
//  Created by Niharika on 10/03/15.
//  Copyright (c) 2015 Infogain. All rights reserved.
//

#import "LeftMenuView.h"

@implementation LeftMenuView

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
