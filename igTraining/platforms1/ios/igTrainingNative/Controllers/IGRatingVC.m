//
//  IGRatingVC.m
//  igTraining
//
//  Created by Shipra Chauhan on 09/11/16.
//  Copyright © 2016 Infogain. All rights reserved.
//

#import "IGRatingVC.h"
#import "IGRatingTableViewCell.h"
#import "IGCommentsVCViewController.h"
#import "Constants.h"
#import "TransparentView.h"
#import "GMSpinKitView.h"
#import "WebServiceRequestHelper.h"
#import "igTFeedbackRatingInfo.h"
#import "igUtility.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "IGUserDefaults.h"

@interface IGRatingVC (){
    NSArray *ratingPoints;
    NSMutableArray *ratingAverageArray;
    float rating;
}

@property (weak, nonatomic) IBOutlet UITableView *ratingTableView;
- (IBAction)addCommentBtnClkd:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *courseTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblTrainerName;
@property (weak, nonatomic) IBOutlet UILabel *lblTrainingDate;
@property (strong, nonatomic)TransparentView* transparentView;
@property (nonatomic, strong) GMSpinKitView *spinner;
@property (nonatomic, strong) NSMutableDictionary *dictTrainingFeedbacks;
@property (nonatomic, strong) NSMutableDictionary *dictCommentfeedback;


@end

@implementation IGRatingVC

- (void)viewDidLoad {
    [super viewDidLoad];
  //  [igUtility setScreenName:@"Rating View Controller" ClassName:NSStringFromClass([self class])];
    [self configureNavigatonBar];
    ratingAverageArray =[[NSMutableArray alloc]init];
    self.dictCommentfeedback = [[NSMutableDictionary alloc]init];
    rating =0;
   // [ratingAverageArray addObject:[NSNull null]];
    
    NSArray* transparentxibContents = [[NSBundle mainBundle] loadNibNamed:@"TransparentView" owner:nil options:nil];
    self.transparentView = [transparentxibContents objectAtIndex:0];
    self.transparentView.frame = self.view.frame;

    
    //[igUtility setScreenName:@"TrainingCalender-ListView" ClassName:NSStringFromClass([self class])];
    
    _spinner = [[GMSpinKitView alloc] initWithColor:[UIColor colorWithRed:23.0/255.0  green:134.0/255.0 blue:216.0f/255.0 alpha:1.0f]];
    
    _spinner.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds) );
    _spinner.hidden = YES;
    [self.transparentView  addSubview:_spinner];
    
    self.lblTrainerName.text = self.strTrainerName;
    self.lblTrainingDate.text = self.strTrainingDate;
    self.courseTitleLabel.text = self.strCourseName;
 
    
    self.ratingTableView.rowHeight = UITableViewAutomaticDimension;
    self.ratingTableView.estimatedRowHeight = 57.0; // set to whatever your "average" cell height is

    
    ratingPoints =[[NSArray alloc]initWithObjects:@"Course coverage with agreed time limit",@"Quality of the course material",@"Relevant example/activities",@"Knowledge of the subject(agreed coverage + within timeline)",@"Instructor was able to create an effective learning environment",@"Presentation & Communication skills",@"Interaction level",@"Response to Queries/Questions", nil];
    
    [self getFeedbackRatingList];
    // Do any additional setup after loading the view.
}

//Initiliaze rating array based on total number of rows in feedback listing
-(void)initializeRatings
{
    NSUInteger sizeOfArray = [self getTranerRowCount] + [[self.dictTrainingFeedbacks objectForKey:kFeedbackTemplete1] count] + [[self.dictTrainingFeedbacks objectForKey:kFeedbackTemplete3] count];
    for (NSUInteger i = 0; i < sizeOfArray; i++) {
        [ratingAverageArray addObject:[NSNull null]];
    }

}

-(void)configureNavigatonBar{
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"pattern_bg"]]];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:18.0];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.text = [NSString stringWithFormat:@"Rating"];
    label.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = label;
    [label sizeToFit];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.opaque = YES;
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Medium" size:17.0f]};
    
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    UIImage *image = [UIImage imageNamed:@"back-icon.png"];
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:image
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(showMainMenuBtnAction:)];
    menuButton.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = menuButton;
    
}

//Show activity indicator while fetching service or performing any action
-(void)showIndicator:(BOOL)isTrue
{
    if(isTrue)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.transparentView removeFromSuperview];
            [self.navigationController.view addSubview:self.transparentView];
            
            _spinner.hidden = NO;
            [_spinner startAnimating];
        });
        
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.transparentView removeFromSuperview];
            
            _spinner.hidden = YES;
            [_spinner stopAnimating];
        });
    }
}


//Get Past trainings list from web services
-(void)getFeedbackRatingList
{
    kAlertAndReturnIfNetworkNotReachable
    WebServiceRequestHelper * helper=[[ WebServiceRequestHelper alloc]init];
    [self showIndicator:true];
    
    [helper getFeedbackStarRatingInformation:self.strTrainingID withSucceHandler:^(NSDictionary *message)
     {
         if(message.count > 0 && message != nil)
         {
  
             self.dictTrainingFeedbacks = (NSMutableDictionary*)message;
             [self.dictCommentfeedback setObject:[self.dictTrainingFeedbacks objectForKey:kFeedbackTemplete4 ] forKey:kFeedbackTemplete4];
             [self.dictCommentfeedback setObject:[self.dictTrainingFeedbacks objectForKey:kFeedbackTemplete5 ] forKey:kFeedbackTemplete5];
             [self.dictTrainingFeedbacks removeObjectForKey:kFeedbackTemplete4];
             [self.dictTrainingFeedbacks removeObjectForKey:kFeedbackTemplete5];
             
                dispatch_async(dispatch_get_main_queue(), ^{
                     
                     [self initializeRatings];
                     [self showIndicator:false];
                     
                     [_ratingTableView reloadData];
                     
                 });
        }
         else
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 [self showIndicator:false];
                 [igUtility showOKAlertWithTitle:kStringError message:@"No rating available." ];
             });
         }
     }
    withFailureHandler:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self showIndicator:false];
            
            [igUtility showOKAlertWithTitle:kStringError message:[igUtility errorDescription:error]]; });
    }];
    
    
}


- (IBAction)showMainMenuBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark table view delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if([self.dictTrainingFeedbacks count]  > 0 && self.dictTrainingFeedbacks != nil)
        return [self.dictTrainingFeedbacks count];
    else
        return  0;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0)
    {
        return [[self.dictTrainingFeedbacks objectForKey:kFeedbackTemplete1] count];
    }
    if(section == [self.dictTrainingFeedbacks count]-1)
    {
        return [[self.dictTrainingFeedbacks objectForKey:kFeedbackTemplete3] count];
    }
    else
    {
        return [[self.dictTrainingFeedbacks objectForKey:[NSString stringWithFormat:@"trainerFeedback%ld",(long)section]] count];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
   [headerView setBackgroundColor:[UIColor grayColor]];
    UILabel *objLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0,tableView.bounds.size.width,30)];
    objLabel.textColor = [UIColor whiteColor];
    objLabel.font = [UIFont systemFontOfSize:13];
    if(section == 0)
    {
        igTFeedbackRatingInfo* info  = [[self.dictTrainingFeedbacks objectForKey:kFeedbackTemplete1] firstObject];
        objLabel.text = [NSString stringWithFormat:@"   %@",info.feedbackType];
    }
    else if(section == [self.dictTrainingFeedbacks count]-1)
    {
        igTFeedbackRatingInfo* info  = [[self.dictTrainingFeedbacks objectForKey:kFeedbackTemplete3] firstObject];
        objLabel.text =  [NSString stringWithFormat:@"  %@",info.feedbackType];
    }
    else
    {
        igTFeedbackRatingInfo* info  = [[self.dictTrainingFeedbacks objectForKey:[NSString stringWithFormat:@"trainerFeedback%ld",(long)section]]firstObject];
        objLabel.text =  [NSString stringWithFormat:@" Trainer - %@",info.trainerName];
    }

    [headerView addSubview:objLabel];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return UITableViewAutomaticDimension;
//}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"IGRatingTableViewCell";
    IGRatingTableViewCell *cell = [self.ratingTableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    NSArray* arrRatings = [self getFeedbackTemplate:(int)indexPath.section];
    if(arrRatings != nil)
    {
        igTFeedbackRatingInfo* ratingInfo =  [arrRatings objectAtIndex:indexPath.row];
        cell.ratingQuestionLabel.text =ratingInfo.feedbackSubType;

    }
    
    if (indexPath.row==4 | indexPath.row==5| indexPath.row==6| indexPath.row==7) {
        cell.contentView.backgroundColor =[UIColor clearColor];
    }
    else{
        cell.contentView.backgroundColor =[UIColor whiteColor];
    }
    
    cell.starRatingImage.starImage = [UIImage imageNamed:@"unrating.png"];
    cell.starRatingImage.starHighlightedImage = [UIImage imageNamed:@"rating.png"];
    cell.starRatingImage.maxRating = 5.0;
    cell.starRatingImage.horizontalMargin = 12;
    cell.starRatingImage.editable=YES;
    cell.starRatingImage.displayMode=EDStarRatingDisplayAccurate;
    
    [self setStarRarting:(int)indexPath.section Row:(int)indexPath.row Cell:cell];
    
    cell.starRatingImage.returnBlock = ^(float rating1 )
    {
        NSNumber *ratingOfparticularPoint = [NSNumber numberWithFloat: rating1];
        if(indexPath.section == 0)
        {
            [self calculateAverageRtaing:ratingOfparticularPoint :indexPath.row];
        }
        else if(indexPath.section == [self.dictTrainingFeedbacks count]-1){
            
            NSInteger getPreviousSectionCount = [[self.dictTrainingFeedbacks objectForKey:kFeedbackTemplete1] count] + [self getTranerRowCount];
            [self calculateAverageRtaing:ratingOfparticularPoint :(indexPath.row + getPreviousSectionCount)];
        }
        else{
            NSInteger getPreviousSectionCount = [self getSectionsRowCount:(int)(indexPath.section-1)];
            if(indexPath.section == 1)
                getPreviousSectionCount = [[self.dictTrainingFeedbacks objectForKey:kFeedbackTemplete1] count];
            else
                getPreviousSectionCount = getPreviousSectionCount+ [[self.dictTrainingFeedbacks objectForKey:kFeedbackTemplete1] count];
            
            [self calculateAverageRtaing:ratingOfparticularPoint :(indexPath.row + getPreviousSectionCount)];
        }
        int TotalRating = 0 ;
        
        for (int i=0; i <[ratingAverageArray count]; i++) {
            int x= 0;
            if (![[ratingAverageArray objectAtIndex:i]isKindOfClass:[NSNull class]]) {
                x = [(NSNumber *)[ratingAverageArray objectAtIndex:i] intValue];
                TotalRating = TotalRating + x;
            }
            
        }
        NSLog(@"TotalRating =%ld",(long)TotalRating);
        float averageRating =0 ;
        int x = (int)[ratingAverageArray count]*5;
        averageRating = (TotalRating*100)/x;
        NSLog(@"average=%f",averageRating);
        self.progressBar.value = averageRating;
    };
    
       return cell;
}


/*
 Function:getFeedbackTemplate:(int)section
 Description: get feedback template base on the section and return array for tempate header
 */
-(NSArray*)getFeedbackTemplate:(int)section
{
    if(section == 0)
    {
        NSArray* arrRatings = [self.dictTrainingFeedbacks objectForKey:kFeedbackTemplete1];
        return arrRatings;
    }
    else if(section == [self.dictTrainingFeedbacks count]-1)
    {
        NSArray* arrRatings = [self.dictTrainingFeedbacks objectForKey:kFeedbackTemplete3];
        return arrRatings;
    }
    else
    {
        NSArray* arrRatings =  [self.dictTrainingFeedbacks objectForKey:[NSString stringWithFormat:@"trainerFeedback%ld",(long)section]];
        return arrRatings;
    }
    return nil;
}


/*
 Function:setStarRarting:(int)section Row:(int)row Cell:(IGRatingTableViewCell*)cell
 Description: show star rating as given/selected by employee
 */
-(void)setStarRarting:(int)section Row:(int)row Cell:(IGRatingTableViewCell*)cell
{
 @try
{
    if(section == 0)
    {
        
        if (![[ratingAverageArray objectAtIndex:row]isKindOfClass:[NSNull class]]){
            cell.starRatingImage.rating = [[ratingAverageArray objectAtIndex:row] floatValue];
        }
        else{
            cell.starRatingImage.rating = 0;
        }
    }
    else if(section == [self.dictTrainingFeedbacks count]-1)
    {
        NSInteger getPreviousSectionCount = [[self.dictTrainingFeedbacks objectForKey:kFeedbackTemplete1] count] + [self getTranerRowCount];
        if (![[ratingAverageArray objectAtIndex:(row + getPreviousSectionCount)]isKindOfClass:[NSNull class]]){
            cell.starRatingImage.rating = [[ratingAverageArray objectAtIndex:(row + getPreviousSectionCount)] floatValue];
        }
        else{
            cell.starRatingImage.rating = 0;
        }
        
    }
    else
    {
       NSInteger getPreviousSectionCount = [self getSectionsRowCount:(int)(section-1)];
        if(section == 1)
            getPreviousSectionCount = [[self.dictTrainingFeedbacks objectForKey:kFeedbackTemplete1] count];
        else
            getPreviousSectionCount = getPreviousSectionCount+ [[self.dictTrainingFeedbacks objectForKey:kFeedbackTemplete1] count];
        
        if (![[ratingAverageArray objectAtIndex:(row + getPreviousSectionCount)]isKindOfClass:[NSNull class]]){
            cell.starRatingImage.rating = [[ratingAverageArray objectAtIndex:(row + getPreviousSectionCount)] floatValue];
        }
        else{
            cell.starRatingImage.rating = 0;
        }

    }
}@catch (NSException *exception) {
    NSLog(@"catch esception  = %@",exception.description);
} @finally {
    
}
}
//Calculate all trainer  sections total row count to set the star ratings
-(NSInteger)getTranerRowCount{
    
    NSInteger rowCount = 0;
    for(int count = 0; count < self.dictTrainingFeedbacks.count; count++)
    {
        rowCount = rowCount + [[self.dictTrainingFeedbacks objectForKey:[NSString stringWithFormat:@"trainerFeedback%d",(int)count+1]] count];

    }
    return rowCount;
}

//Calculate each trainer section row count
-(NSInteger)getSectionsRowCount:(int)section
{
    NSInteger rowCount = 0;
    for(int count = 0; count < section; count++)
    {
        rowCount = rowCount + [[self.dictTrainingFeedbacks objectForKey:[NSString stringWithFormat:@"trainerFeedback%d",(int)count+1]] count];
        
    }
    return rowCount;

}

-(void)calculateAverageRtaing:(NSNumber*)ratingOfparticularPoint :(NSInteger)indexpath{
    
    //NSMutableArray *ratingArray=[[NSMutableArray alloc]init];
    //[ratingAverageArray insertObject:ratingOfparticularPoint atIndex:indexpath];
    [ratingAverageArray replaceObjectAtIndex:indexpath withObject:ratingOfparticularPoint];
    //NSLog(@"ratingArray =%@",ratingAverageArray);
}

-(void)getRatingOfCell:(float)ratingValue :(NSInteger)rowNumber{
    //NSLog(@"rating Value =%f & indexpath.row =%ld",ratingValue,(long)rowNumber);
}

-(void)starsSelectionChanged:(EDStarRating *)control rating:(float)rating1 indexPath:indexPath
{
    
   NSString * ratingString = [NSString stringWithFormat:@"%.1f", rating1];
    NSLog(@"rating value =%@ & indexpath =%@",ratingString,indexPath);
  /*  if( [control isEqual:_starRatingImage] )
        _starRatingImageLabel.text = ratingString;
    else
        _starRatingImageLabel.text = ratingString;*/
}


- (IBAction)addCommentBtnClkd:(UIButton *)sender {
    
    
    NSDictionary* dictonary = @{
                                @"screen_name": @"RatingView controller",
                                @"event_name": NSStringFromSelector(_cmd)
                                };
    //[igUtility setEventName:NSStringFromSelector(_cmd) Parameters:dictonary];
    
    IGCommentsVCViewController *contentVC = (IGCommentsVCViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"IGCommentsVCViewController"];
    
    contentVC.strCourseName = self.strCourseName;
    contentVC.strTrainerName = self.strTrainerName;
    contentVC.strTrainingDate  = self.strTrainingDate;
    contentVC.dictComments = self.dictCommentfeedback;
    
   [self presentViewController:contentVC animated:YES completion:nil];
}

/*
 Function:actionSubmitButton:(id)sender
 Description: submit button is called to submit all ratings given by employee to server.
 */
-(IBAction)actionSubmitButton:(id)sender
{
    NSString* strPayloadData = @"";
    @try
    {
        BOOL isValid = [self checkAllRatingValidations];
        if(isValid)
        {
            NSArray* arrfeedbackTemplate1 = [self.dictTrainingFeedbacks objectForKey:kFeedbackTemplete1];
            int count = 0;
            for(igTFeedbackRatingInfo* info in arrfeedbackTemplate1)
            {
                strPayloadData = [strPayloadData stringByAppendingFormat:@"%@!%@-%@,",[IGUserDefaults sharedInstance].employee.staffID,info.mappingID,[ratingAverageArray objectAtIndex:count]];
                ++count;
            }
            strPayloadData = [strPayloadData substringToIndex:[strPayloadData length]-1];
            
            for(NSString* key in [self.dictTrainingFeedbacks allKeys])
            {
                if([key containsString:@"trainerFeedback"])
                {
                    for(igTFeedbackRatingInfo* info in [self.dictTrainingFeedbacks objectForKey:key])
                    {
                        strPayloadData = [strPayloadData stringByAppendingFormat:@",%@!%@-%@",info.trainerID,info.mappingID,[ratingAverageArray objectAtIndex:count]];
                        ++count;
                    }
                }
            }
            NSArray* arrfeedbackTemplate3 = [self.dictTrainingFeedbacks objectForKey:kFeedbackTemplete3];
            for(igTFeedbackRatingInfo* info in arrfeedbackTemplate3)
            {
                strPayloadData = [strPayloadData stringByAppendingFormat:@",%@!%@-%@",[IGUserDefaults sharedInstance].employee.staffID,info.mappingID,[ratingAverageArray objectAtIndex:count]];
                ++count;
            }
            AppDelegate* appDelegate = kAppDelegate;

            if ([appDelegate.strCommentPayload length] > 0 && ![appDelegate.strCommentPayload isEqualToString:@""])
            {
                strPayloadData = [strPayloadData stringByAppendingFormat:@"%@",appDelegate.strCommentPayload];
            }


            NSLog(@"data = %@",strPayloadData);
            [self submitFeedbackComment:strPayloadData];
        }

    } @catch (NSException *exception) {
        
    } @finally {
        
    }
  
}

//Submit feedback ratings for selected training.
-(void)submitFeedbackComment:(NSString*)strPayloadData
{
    kAlertAndReturnIfNetworkNotReachable
    WebServiceRequestHelper * helper=[[ WebServiceRequestHelper alloc]init];
    [self showIndicator:true];
    
    [helper requestForTrainingSubmission:[IGUserDefaults sharedInstance].employee.staffID andTrainingId:self.strTrainingID Rating:strPayloadData withSucceHandler:^(NSString* message)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             [self showIndicator:false];
             AppDelegate* appDelegate = kAppDelegate;
             appDelegate.strCommentPayload = @"";
              [[[UIAlertView alloc] initWithTitle:@"Info" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];

         });
         
    } withFailureHandler:^(NSError *error) {
         dispatch_async(dispatch_get_main_queue(), ^{
             [self showIndicator:false];
             [igUtility showOKAlertWithTitle:kStringError message:[igUtility errorDescription:error]];
         });
     }];

}

// Check rating validations to show alert for missing ratings
-(BOOL)checkAllRatingValidations{
    
    BOOL isTrue = true;
    
    NSArray* arrfeedbackTemplate1 = [self.dictTrainingFeedbacks objectForKey:kFeedbackTemplete1];
    int count = 0;
    for(igTFeedbackRatingInfo* info in arrfeedbackTemplate1)
    {

       if([[ratingAverageArray objectAtIndex:count] isKindOfClass:[NSNull class]])
       {
           [igUtility showOKAlertWithTitle:kStringError message:[NSString stringWithFormat:@"Please select rating for %@",info.feedbackType]];
           isTrue = false;
           return isTrue;
       }
        count++;
    }
    
    for(NSString* key in [self.dictTrainingFeedbacks allKeys])
    {
        if([key containsString:@"trainerFeedback"])
        {
            for(igTFeedbackRatingInfo* info in [self.dictTrainingFeedbacks objectForKey:key])
            {
                if([[ratingAverageArray objectAtIndex:count] isKindOfClass:[NSNull class]])
                {
                    [igUtility showOKAlertWithTitle:kStringError message:[NSString stringWithFormat:@"Please select rating for %@",self.strTrainerName]];
                    isTrue = false;
                    return isTrue;
                }
                
                ++count;
            }
        }
    }
    igTFeedbackRatingInfo* info  = [[self.dictTrainingFeedbacks objectForKey:kFeedbackTemplete3] firstObject];
    if([[ratingAverageArray objectAtIndex:count] isKindOfClass:[NSNull class]])
    {
        [igUtility showOKAlertWithTitle:kStringError message:[NSString stringWithFormat:@"Please select rating for %@",info.feedbackType]];
        isTrue = false;
        return isTrue;
    }
    return isTrue;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {

    [kAppDelegate setupDashboardAsRootViewController];
}

@end
