//
//  IGCommentsVCViewController.h
//  igTraining
//
//  Created by Shipra Chauhan on 09/11/16.
//  Copyright © 2016 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGCommentsVCViewController : UIViewController

//passed data
@property (nonatomic, strong) NSString *strCourseName;
@property (nonatomic, strong) NSString *strTrainerName;
@property (nonatomic, strong) NSString *strTrainingDate;
@property (nonatomic, strong) NSDictionary *dictComments;

-(IBAction)actionDoneDutton:(id)sender;

@end
