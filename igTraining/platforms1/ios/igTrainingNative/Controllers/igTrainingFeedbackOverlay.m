//
//  igTrainingFeedbackOverlay.m
//  igTraining
//
//  Created by Sandeep on 05/01/17.
//  Copyright © 2017 Infogain. All rights reserved.
//

#import "igTrainingFeedbackOverlay.h"


@interface igTrainingFeedbackOverlay ()<UINavigationBarDelegate>

- (IBAction)cancleBtnClkd:(UIBarButtonItem *)sender;
@property (weak, nonatomic) IBOutlet UINavigationBar *navigation;

@end

@implementation igTrainingFeedbackOverlay

-(void)viewDidLoad
{
    [super viewDidLoad];
   [self.navigation setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"pattern_bg"]]];
    self.lblCourseTitle.text = [self.objTraining.topic isEqualToString:@""] ? @"NA" : [self.objTraining.topic stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.lblTrainerName.text = [self.objTraining.trainerName isEqualToString:@""] ? @"NA" : [self.objTraining.trainerName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.lblTrainingDate.text = [self.objTraining.date isEqualToString:@""] ? @"NA" : [self.objTraining.date stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.lblTrainingTime.text = [self.objTraining.time isEqualToString:@""] ? @"NA" : [self.objTraining.time stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.lblTrainingMode.text = [self.objTraining.trainingMode isEqualToString:@""] ? @"NA" : [self.objTraining.trainingMode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.lblTrainingVenue.text = [self.objTraining.venue isEqualToString:@""] ? @"NA" : [self.objTraining.venue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.lblPrerequisite.text = [self.objTraining.prerequisite isEqualToString:@""] ? @"NA" : [self.objTraining.prerequisite stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if(self.objTraining.trainingMode == nil)
    {
        self.lblTrainingMode.text = @"NA";
    }
    if(self.objTraining.prerequisite == nil)
    {
        self.lblPrerequisite.text = @"NA";
    }

    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"dashboard_bgIPad.png"]];

    }
    else{
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"dashboard_bg"]];

    }

    
    
     [self.navBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"pattern_bg"]]];
    
    
   
    
}

-(UIBarPosition)positionForBar:(id <UIBarPositioning>)bar
{
    return UIBarPositionTopAttached;
}
- (IBAction)cancleBtnClkd:(UIBarButtonItem *)sender {
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
/*- (BOOL)prefersStatusBarHidden {
    return YES;
}*/

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(IBAction)dismissView:(id)sender
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        // iOS 7
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    } else {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    }
    [self dismissViewControllerAnimated:true completion:nil];
}
@end
