//
//  ViewController.m
//  igTraining
//
//  Created by Varun on 15/01/15.
//  Copyright (c) 2015 Infogain. All rights reserved.
//

#import "igTLoginViewController.h"
#import "igtRootViewController.h"
#import "Constants.h"
#import "WebServiceRequestHelper.h"
#import "igUtility.h"
#import "IGUserDefaults.h"
#import <FirebaseAnalytics/FirebaseAnalytics.h>
#import "AppDelegate.h"

@interface igTLoginViewController ()
{
    UITextField *activeField;

}
@end

@implementation igTLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [igUtility setScreenName:@"LoginViewController" ClassName:NSStringFromClass([self class])];
    self.userNameTxtField.delegate = self;
    self.passwordTxtField.delegate = self;
    // Do any additional setup after loading the view, typically from a nib.
    
   // self.userNameTxtField.text = @"varun.chaudhary";
   //self.passwordTxtField.text = @"var@234!";
}
-(void)viewWillDisappear:(BOOL)animated
{
    self.userNameTxtField.text = @"";
    self.passwordTxtField.text = @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
 Method:initSpinner
 Description: This function is called to create the activity indicator.
 */
-(void)initSpinner
{
    _spinner = [[GMSpinKitView alloc] initWithColor:[UIColor colorWithRed:23.0/255.0  green:134.0/255.0 blue:216.0f/255.0 alpha:1.0f]];
    
    _spinner.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds) );
    [self.view bringSubviewToFront:_spinner];
    [self.view  addSubview:_spinner];
}

/*
 Method:signInBtnAction
 Description: This function is when user clicked on the Sign in button to get login inside the app. user name and password will be sent to web service for validations.
 */
- (IBAction)signInBtnAction:(id)sender
{
    kAlertAndReturnIfNetworkNotReachable
    NSDictionary* dictonary = @{
                                @"screen_name": @"Login ViewController",
                                @"event_name": NSStringFromSelector(_cmd)
                                };
    [igUtility setEventName:NSStringFromSelector(_cmd) Parameters:dictonary];
    
    [self initSpinner];
    [_spinner startAnimating];
    NSString* userName = self.userNameTxtField.text;
    NSString* password = self.passwordTxtField.text;
    if([self.userNameTxtField.text rangeOfString:@"@infogain.com"].location != NSNotFound)
    {
        userName = [userName substringToIndex:[self.userNameTxtField.text rangeOfString:@"@infogain.com"].location];
    }

    //For User Name and Password Validation If Required;
    NSString *strRegExp=@"[A-Za-z0-9._]+";
    NSPredicate *userNameTest=[NSPredicate predicateWithFormat:@"SELF MATCHES %@",strRegExp];
    
    NSString *errorMessage = nil;
    NSInteger *errorCode = nil;
    
    if (userName.length == 0)
    {
        errorMessage = @"Please Enter User Name";
        errorCode = (NSInteger *)1;
    }
    else if ([userNameTest evaluateWithObject:userName]==NO)
    {
        errorMessage = @"Please Enter Valid User Name";
        errorCode = (NSInteger *)1;
    }
    else if(self.passwordTxtField.text.length == 0)
    {
        errorMessage = @"Please Enter Password";
        errorCode = (NSInteger *)2;
    }
    
    if (errorMessage)
    {
        [self validationError:errorMessage errorType:errorCode];
        [_spinner stopAnimating];
        [_spinner removeFromSuperview];

    }
    else{
//        UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        igtRootViewController *rootVC = (igtRootViewController*) [mainStoryBoard instantiateViewControllerWithIdentifier:kRootViewController_Identifier];
//        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"Login"];
//
//        [self.navigationController pushViewController:rootVC animated:YES];
//        [self.navigationController setNavigationBarHidden:YES];
        
    WebServiceRequestHelper * helper=[[ WebServiceRequestHelper alloc]init];
    [self.view resignFirstResponder];
//    [helper requestForLogin:@"igglobal" andUsername:@"divyanshu.srivastava" andPassword:@"imlondon@123!" withSucessHandler:^(NSString *message) {

    [helper requestForLogin:@"igglobal"
                andUsername:userName
                andPassword:password
          withSucessHandler:^(NSString *message) {
              
              [IGUserDefaults sharedInstance].userName = [userName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
              [IGUserDefaults savePassword:[password stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
              [IGUserDefaults sharedInstance].domainID = @"igglobal";
             // [IGUserDefaults sharedInstance].userName = self.userNameTxtField.text;
              
             // [[NSUserDefaults standardUserDefaults]setValue:self.userNameTxtField.text forKey:@"userName"];
//              UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//              igtRootViewController *rootVC = (igtRootViewController*) [mainStoryBoard instantiateViewControllerWithIdentifier:kRootViewController_Identifier];
//              [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"Login"];
//              [[NSUserDefaults standardUserDefaults] synchronize];

        dispatch_async(dispatch_get_main_queue(), ^{
            [_spinner stopAnimating];
            [_spinner removeFromSuperview];
            if(![IGUserDefaults sharedInstance].isFirstTimeLogin)
            {
                UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                igtRootViewController *rootVC = (igtRootViewController*) [mainStoryBoard instantiateViewControllerWithIdentifier:kRootViewController_Identifier];
                [self.navigationController pushViewController:rootVC animated:YES];
                [self.navigationController setNavigationBarHidden:YES];
            }
            else{
                AppDelegate* appdelegate = kAppDelegate;
                [appdelegate setupDashboardAsRootViewController];
            }
            /* UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
              igtRootViewController *rootVC = (igtRootViewController*) [mainStoryBoard instantiateViewControllerWithIdentifier:kRootViewController_Identifier];
            [self.navigationController pushViewController:rootVC animated:YES];
            [self.navigationController setNavigationBarHidden:YES];*/
        });
    }
         withFailureHandler:^(NSError *error)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [_spinner stopAnimating];
                [_spinner removeFromSuperview];
                [igUtility showOKAlertWithTitle:kStringError message:[igUtility errorDescription:error]];

            });
    }];
   }
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([self.userNameTxtField hasText] && [self.passwordTxtField hasText])
    {
        [igUtility enableButton:self.signInBtn backgroundColor:kColorSigninGreen];
    }
    else
    {
        [igUtility disableButton:self.signInBtn];
    }
    //Log In BUtton Layout
    [self.signInBtn.layer setCornerRadius:8.0f];
    [self.signInBtn.layer setBorderWidth:2.0];
    [self.signInBtn.layer setBorderColor:kColorButtonShadow.CGColor];

    [self.navigationController setNavigationBarHidden:YES];   //it hides
}

//-(void)viewWillDisappear:(BOOL)animated{
//    [super viewWillDisappear:animated];
//    [self.navigationController setNavigationBarHidden:NO];    // it shows
//}


- (void)validationError:(NSString *)error errorType:(NSInteger *)errorCode
{
    [igUtility showOKAlertWithTitle:@"Credentials Error" message:error];
    
    if ((long)errorCode == 1) {
        self.userNameTxtField.text = nil;
        self.passwordTxtField.text = nil;
        
    }
    else if((long)errorCode == 2)
    {
        self.passwordTxtField.text = nil;
    }
   //make outlet for login btn
    [igUtility disableButton:self.signInBtn];
    return;
}


#pragma marks - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
    if (([self.userNameTxtField hasText] && [activeField isEqual:self.passwordTxtField]) ||
        ([self.passwordTxtField hasText] && [activeField isEqual:self.userNameTxtField]))
    {
        [igUtility enableButton:self.signInBtn backgroundColor:kColorSigninGreen];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = textField;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.userNameTxtField)
    {
        [self.passwordTxtField becomeFirstResponder];
    }
    else if (textField == self.passwordTxtField)
    {
        [self.passwordTxtField resignFirstResponder];
    }
    
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField isEqual:self.userNameTxtField])
    {
        return !([textField.text length]>50 && [string length] > range.length);
    }
    else
    {
        return !([textField.text length]>20 && [string length] > range.length);
    }
}


@end
