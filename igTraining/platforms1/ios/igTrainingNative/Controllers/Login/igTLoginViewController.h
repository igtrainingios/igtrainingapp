//
//  ViewController.h
//  igTraining
//
//  Created by Varun on 15/01/15.
//  Copyright (c) 2015 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GMSpinKitView.h"

@interface igTLoginViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *userNameTxtField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTxtField;
@property (weak, nonatomic) IBOutlet UIButton *signInBtn;
@property (nonatomic, strong) GMSpinKitView *spinner;


@end

