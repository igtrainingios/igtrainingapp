//
//  IGTrainingTableViewCell.m
//  igTraining
//
//  Created by Shipra Chauhan on 09/11/16.
//  Copyright © 2016 Infogain. All rights reserved.
//

#import "IGTrainingTableViewCell.h"

@implementation IGTrainingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCell:(NSString*)date Topic:(NSString*)topic Trainer:(NSString*)trainer
{
    self.lblTrainerName.text = trainer;
    self.lblTrainingTopic.text = topic;
    self.lblDate.text = date;
}

@end
