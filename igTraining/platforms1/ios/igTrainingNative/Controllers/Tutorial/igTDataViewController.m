//
//  GMDataViewController.m
//  GMPageController
//
//  Created by Upakul on 18/09/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "igTDataViewController.h"
#import "igTModelController.h"

@interface igTDataViewController () <UINavigationBarDelegate>

@end

@implementation igTDataViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"pattern_bg"]]];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_port-1"]]];
        self.navigationBar.barTintColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_port-1"]];
    }
    else{
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"main_bg"]]];
        self.navigationBar.barTintColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"main_bg"]];
    }
    self.navigationBar.topItem.title = [self.dataObject objectForKey:@"title"];
    if ([[self.dataObject objectForKey:@"index"] intValue] == ([self.modelController.pageData count] - 1)) {
        [self.navigationBar.topItem.rightBarButtonItem setTitle:@"Done"];
    }
    
    self.dataImage.image = [UIImage imageNamed:[self.dataObject objectForKey:@"image"]];
    self.descriptionLabel.text =[self.dataObject objectForKey:@"description"];
    [self.pageControl setNumberOfPages:[self.modelController.pageData count]];
    [self.pageControl setUserInteractionEnabled:NO];
    [self.pageControl setCurrentPage:[[self.dataObject objectForKey:@"index"] intValue]];
    
    [self.dataImage.layer setShadowColor:[UIColor whiteColor].CGColor];
    [self.dataImage.layer setShadowRadius:2.5];
    [self.dataImage.layer setShadowOpacity:1.0];
    [self.dataImage.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
    
    
    

}

- (UIBarPosition)positionForBar:(id <UIBarPositioning>)bar
{
    return UIBarPositionTopAttached;
}

- (IBAction)skip:(id)sender {
    [self.rootViewController dismissTutorial:self];
}

- (IBAction)next:(id)sender {
    
    if ([[self.dataObject objectForKey:@"index"] intValue] == ([self.modelController.pageData count] - 1)) {
        [self.rootViewController dismissTutorial:self];
        return;
    }
    
    NSUInteger retreivedIndex = [self.modelController indexOfViewController:self];
    igTDataViewController *targetPageViewController = [self.modelController viewControllerAtIndex:(retreivedIndex + 1) storyboard:self.storyboard];
    [self.rootViewController.pageViewController setViewControllers:[NSArray arrayWithObjects:targetPageViewController, nil] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:^(BOOL finished) {
        
    }];
}

//- (UIStatusBarStyle)preferredStatusBarStyle
//{
//    return UIStatusBarStyleLightContent;
//}

@end
