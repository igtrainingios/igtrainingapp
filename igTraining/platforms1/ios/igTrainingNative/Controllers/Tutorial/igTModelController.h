//
//  GMModelController.h
//  GMPageController
//
//  Created by Upakul on 18/09/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "igTTutorialRootViewController.h"

@class igTDataViewController;

@interface igTModelController : NSObject <UIPageViewControllerDataSource>
@property (nonatomic, strong) igTTutorialRootViewController *rootViewController;
@property (nonatomic, strong) NSArray *pageData;

- (igTDataViewController *)viewControllerAtIndex:(NSUInteger)index storyboard:(UIStoryboard *)storyboard;
- (NSUInteger)indexOfViewController:(igTDataViewController *)viewController;

@end
