//
//  GMDataViewController.h
//  GMPageController
//
//  Created by Upakul on 18/09/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "igTTutorialRootViewController.h"

@class igTModelController;

@interface igTDataViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *dataLabel;
@property (strong, nonatomic) IBOutlet UIImageView *dataImage;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, strong) igTTutorialRootViewController *rootViewController;
@property (nonatomic, strong) igTModelController *modelController;
@property (strong, nonatomic) id dataObject;

- (IBAction)skip:(id)sender;
- (IBAction)next:(id)sender;

@end
