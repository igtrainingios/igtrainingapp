//
//  IGAuthenticationVC.m
//  IGCafe
//
//  Created by Pulkit on 29/06/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGAuthenticationVC.h"
#import "GMSpinKitView.h"
#import "UIView+IGCircularView.h"
#import "igTSmoothAlertView.h"
#import "IGUserDefaults.h"
#import "WebServiceRequestHelper.h"
#import "igtRootViewController.h"
#import "Constants.h"
#import "IGAnalytics.h"
#import "igUtility.h"
#import <FirebaseAnalytics/FirebaseAnalytics.h>
#import "AppDelegate.h"

#define TAG_AUTHENTICATION_FAILURE_ALERT          5555

@interface IGAuthenticationVC () <EMSmoothAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageViewLogo;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewUserImage;
@property (weak, nonatomic) IBOutlet UIView *viewWaveAnimation;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelMessage;
@property (weak, nonatomic) IBOutlet UIButton *buttonRetry;

- (IBAction)retryAuthentication:(id)sender;

@end

@implementation IGAuthenticationVC
{
    GMSpinKitView *spinner;
}



-(void)initSpinner
{
    spinner = [[GMSpinKitView alloc] initWithColor:[UIColor colorWithRed:23.0/255.0  green:134.0/255.0 blue:216.0f/255.0 alpha:1.0f]];
    
    spinner.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds) );
    [self.view bringSubviewToFront:spinner];
    [self.view  addSubview:spinner];
    [spinner startAnimating];
}


- (void)viewDidLoad
{


    [super viewDidLoad];
    [igUtility setScreenName:@"Authenitication Controller" ClassName:NSStringFromClass([self class])];
    [self initSpinner];
    self.buttonRetry.hidden = YES;
    
    
    self.labelTitle.layer.shadowColor = [UIColor blackColor].CGColor;
    self.labelTitle.layer.shadowOpacity = 0.5f;
    self.labelTitle.layer.shadowOffset = CGSizeMake(0.5, 0.5);
    
    self.imageViewUserImage.backgroundColor = [igUtility averageColorFromImage:[IGUserDefaults sharedInstance].employee.image];
    self.imageViewUserImage.image = [IGUserDefaults sharedInstance].employee.image;
    self.labelTitle.text = [IGUserDefaults sharedInstance].employee.name;

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [IGAnalytics trackScreen:screenAuthentication];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    
    if (spinner.isAnimating)
    {
        [spinner stopAnimating];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.imageViewUserImage roundViewWithCornerRaidus:(CGRectGetWidth(self.imageViewUserImage.frame) * 0.5)
                                           borderWidth:2.0
                                           borderColor:[UIColor whiteColor]];
    self.imageViewLogo.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    self.imageViewLogo.layer.shadowOpacity = 0.5f;
    self.imageViewLogo.layer.shadowOffset = CGSizeMake(0.5, 0.5);

    [super viewDidAppear:animated];

    [self performSelector:@selector(authenticate) withObject:nil afterDelay:0.5];
}

- (void) authenticate

{
    NSString *userName = [IGUserDefaults sharedInstance].userName;
    NSString *password = [IGUserDefaults retrievePassword];
    NSString *domain = [IGUserDefaults sharedInstance].domainID;
    
    WebServiceRequestHelper * helper=[[ WebServiceRequestHelper alloc]init];
    [self.view resignFirstResponder];
    //    [helper requestForLogin:@"igglobal" andUsername:@"divyanshu.srivastava" andPassword:@"imlondon@123!" withSucessHandler:^(NSString *message) {
    
    [helper requestForLogin:@"igglobal"
                andUsername:userName
                andPassword:password
          withSucessHandler:^(NSString *message) {
              
              // [IGUserDefaults sharedInstance].userName = self.userNameTxtField.text;
              
              // [[NSUserDefaults standardUserDefaults]setValue:self.userNameTxtField.text forKey:@"userName"];
         
              [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"Login"];
              [[NSUserDefaults standardUserDefaults] synchronize];
              
               [FIRAnalytics setUserPropertyString:userName forName:@"UserName"];
              [FIRAnalytics logEventWithName:@"Login" parameters:@{kFIRParameterItemName:userName}];
              [IGAnalytics trackEventWith:@"iOS" action:@"Login" actionName:[NSString stringWithFormat:@"%@_%@",[IGUserDefaults sharedInstance].employee.employeeNewID,[IGUserDefaults sharedInstance].userName]];
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  [spinner stopAnimating];
                  [spinner removeFromSuperview];
                  AppDelegate *appDelegate = kAppDelegate;
                  [appDelegate setupDashboardAsRootViewController
                   ];
              });
          }
         withFailureHandler:^(NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             [spinner stopAnimating];
             [spinner removeFromSuperview];
             if (error.code == 1000)
             {
                 [igUtility showOKAlertWithTitle:kStringLoginError
                                       message:[error localizedDescription]
                                      delegate:self
                                           tag:TAG_AUTHENTICATION_FAILURE_ALERT];
             }
             else
             {
                 self.labelMessage.text = [error localizedDescription];
                 self.buttonRetry.hidden = NO;
             }

             
         });
     }];

}

- (void)didReceiveMemoryWarning
{

    [super didReceiveMemoryWarning];
}

- (IBAction)retryAuthentication:(id)sender
{
    self.labelMessage.text = @"";
    [self.view addSubview:spinner];
    [spinner startAnimating];
    self.buttonRetry.hidden = YES;
    [self authenticate];
    
}

#pragma mark - EMSmoothAlertViewDelegate

- (void)alertView:(igTSmoothAlertView *)alertView didDismissWithButton:(UIButton *)button
{

    if (alertView.tag == TAG_AUTHENTICATION_FAILURE_ALERT)
    {
        if ([button isEqual:alertView.defaultButton])
        {
            [IGUserDefaults sharedInstance].isUserLoggedIn = NO;
            AppDelegate *appDelegate = kAppDelegate;
            [appDelegate setupLoginControllerAsRootViewController];
        }
    }
}


@end
