//
//  igTermConditionVC.m
//  IGCafe
//
//  Created by Manish on 23/06/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "igTermConditionVC.h"
#import "Constants.h"
#import "igTSmoothAlertView.h"
#import "igTTutorialRootViewController.h"
#import "IGAnalytics.h"
#import "IGUserDefaults.h"
#import <FirebaseAnalytics/FirebaseAnalytics.h>
#import "AppDelegate.h"

#define kTermsAndConditionsFileName             @"terms_and_conditions"

#define TAG_ALERT_QUIT_APPLICATION              3333

@interface igTermConditionVC ()<EMSmoothAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageViewBackground;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIButton *buttonClose;

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelSubTitle;

@property (weak, nonatomic) IBOutlet UIView *viewButtons;
@property (weak, nonatomic) IBOutlet UIButton *buttonAgree;
@property (weak, nonatomic) IBOutlet UIButton *buttonDisagree;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraintWebView;

- (IBAction)close:(id)sender;
- (IBAction)agree:(id)sender;
- (IBAction)disagree:(id)sender;

@end


@implementation igTermConditionVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.buttonAgree.exclusiveTouch = YES;
    self.buttonDisagree.exclusiveTouch = YES;
    self.buttonClose.exclusiveTouch = YES;
    
    
   /* if (_isLaunchScene)
    {
        self.buttonClose.hidden = YES;
    }
    else
    {
        [self.viewButtons removeFromSuperview];
        //self.bottomConstraintWebView.constant = 12;
    }*/
    
    NSString *filePath = [[[NSBundle mainBundle] pathForResource:kTermsAndConditionsFileName ofType:@"html"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSAssert1(filePath != nil, kStringMessageFileNotFoundInBundle_, kTermsAndConditionsFileName);
    if (filePath)
    {

        NSURL *url = [NSURL URLWithString:filePath];
        [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
    }
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [IGAnalytics trackScreen:screenTermsAndConditions];
}


//- (UIStatusBarStyle)preferredStatusBarStyle
//{
//    return UIStatusBarStyleLightContent;
//}

- (IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)agree:(id)sender
{
   // if (_isLaunchScene)
    {
        
        [IGUserDefaults sharedInstance].hasAcceptedTermsAndConditions = YES;
        [IGUserDefaults sharedInstance].employee.isAuthenticated = YES;
         [FIRAnalytics setUserPropertyString:[IGUserDefaults sharedInstance].userName forName:@"UserName"];
        [IGAnalytics trackEventWith:@"iOS" action:@"AutoLogin" actionName:[NSString stringWithFormat:@"%@_%@",[IGUserDefaults sharedInstance].employee.employeeNewID,[IGUserDefaults sharedInstance].userName]];

        dispatch_async(dispatch_get_main_queue(), ^{
            AppDelegate *appDelegate = kAppDelegate;
            [appDelegate setupDashboardAsRootViewController
             ];
        });

    }
}

- (void)showLogin
{
//    Setup will automatically setup VCs to show login
        [[NSNotificationCenter defaultCenter]postNotificationName:@"ShowTutorial" object:nil];

    }

- (IBAction)disagree:(id)sender
{
    [IGUserDefaults sharedInstance].hasAcceptedTermsAndConditions = NO;
    [IGUserDefaults sharedInstance].employee.isAuthenticated = NO;
    
    
    
    NSString *title = kStringLogOut;
    NSString *message = kStringMessageYouNeedToAgreeToTermsAndConditions;
    //[igUtility showOKAlertWithTitle:title message:message delegate:self tag:TAG_ALERT_QUIT_APPLICATION];

    
    igTSmoothAlertView *alert = [[igTSmoothAlertView alloc] initDropAlertWithTitle:title
                                                                           andText:message
                                                                   andCancelButton:YES
                                                                          andColor:kDefaultBarTintColor
                                                                          andImage:[UIImage imageNamed:@"infogain-logo-circle"]];
    alert.tag = TAG_ALERT_QUIT_APPLICATION;
    [alert.defaultButton setTitle:kStringLogOut forState:UIControlStateNormal];
    alert.delegate = self;
    [alert show];
}




#pragma mark - EMSmoothAlertViewDelegate


- (void)alertView:(igTSmoothAlertView *)alertView didDismissWithButton:(UIButton *)button
{
    if (alertView.tag == TAG_ALERT_QUIT_APPLICATION)
    {
       if ([button isEqual:alertView.defaultButton])
        {
//            [self dismissViewControllerAnimated:true completion:nil];
//            [[NSNotificationCenter defaultCenter]postNotificationName:@"OnCloseTermsAmdCondition" object:nil];
            AppDelegate *appDelegate = kAppDelegate;
            [appDelegate setupLoginControllerAsRootViewController
             ];
            [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"Login"];
            [[IGUserDefaults sharedInstance] logout];
        }
    }
}

@end
