//
//  IGAnalytics.m
//  igTimeSheet
//
//  Created by Manish on 05/03/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGAnalytics.h"
#import "IGGoogleAnalytics.h"
#import "igUtility.h"

#define kIGGoogleAnalyticsTrackingID        @"UA-60353371-5"

// Screen Names
const NSString *screenLogin = @"Login";
const NSString *screenDashBoard = @"Dashboard";
const NSString *screenTaskView = @"TaskView";
const NSString *screenAddTask = @"AddTask";
const NSString *screenViewLeaves = @"ViewLeaves";
const NSString *screenMyArea = @"MyArea";
const NSString *screenMonthlyAverageTimesheet = @"Monthly Average Timesheet";
const NSString *screenHelp = @"Help";
const NSString *screenMenu = @"Menu";

// Action Names
const NSString *actionLogin = @"Login";
const NSString *actionLogout = @"Logout";
const NSString *actionSubmitTimesheet = @"Submit Timesheet";
const NSString *actionSaveTimesheet = @"Save Timehseet";




@implementation IGAnalytics

+ (void)initialize
{
    [super initialize];
    
    NSLog(@"%s", __FUNCTION__);
    
    
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;
    
    // Optional: set Logger to VERBOSE for debug information.
    //#if DEBUG
    //    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    //#else
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelNone];
    //#endif
    
    // Initialize tracker. Replace with your tracking ID.
    [[GAI sharedInstance] trackerWithTrackingId:kIGGoogleAnalyticsTrackingID];
    
}

+ (void)trackScreen:(NSString *)screenName
{
    if ([igUtility isValidString:screenName])
    {
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:screenName];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    }
}

+ (void)trackButtonAction:(NSString *)actionName
{
    [self trackEventForCategory:kIGAnalyticsCategoryUIAction
                         action:kIGAnalyticsActionButtonPress
                     actionName:actionName];
}

+ (void)trackEventForCategory:(IGAnalyticsCategory)category
                       action:(IGAnalyticsAction)action
                   actionName:(NSString *)actionName
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    NSString *lCategory = [self stringFromCategory:category];
    NSString *lAction = [self stringFromAction:action];
    
    GAIDictionaryBuilder *builder = [GAIDictionaryBuilder createEventWithCategory:lCategory
                                                                           action:lAction
                                                                            label:actionName
                                                                            value:nil];
    [tracker send:[builder build]];
    
}


+(void)trackEventWith:(NSString *)category action:(NSString*)action actionName:(NSString *)actionName{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    NSString *lCategory = category;
    NSString *lAction = action;
    
    GAIDictionaryBuilder *builder = [GAIDictionaryBuilder createEventWithCategory:lCategory
                                                                           action:lAction
                                                                            label:actionName
                                                                            value:nil];
    [tracker send:[builder build]];
   
}


#pragma mark - Enum to String Conversion

+ (NSString *)stringFromCategory:(IGAnalyticsCategory)category
{
    switch (category)
    {
        case kIGAnalyticsCategoryUIAction:
        {
            return @"ios_ui_action";
        }
            
        case kIGAnalyticsCategoryServerAPIHit:
        {
            return @"server_api_hit";
        }
            
        default:
        {
            return @"unknown_category";
        }
    }
    
    return @"unknown_category";
}

+ (NSString *)stringFromAction:(IGAnalyticsAction)action
{
    switch (action) {
        case kIGAnalyticsActionButtonPress:
        {
            return @"ios_button_press";
        }
            
        case kIGAnalyticsActionServerAPILogin:
        {
            return @"server_api_login";
        }
            
        default:
        {
            return @"unknown_action";
        }
    }
    
    return @"unknown_action";
}


+ (void)logInfo:(NSString *)message
{
    [[[GAI sharedInstance] logger] info:message];
}

@end
