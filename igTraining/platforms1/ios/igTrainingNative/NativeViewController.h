//
//  ViewController.h
//  Moodle Mobile
//
//  Created by Asif on 21/03/17.
//
//

#import <UIKit/UIKit.h>

@interface NativeViewController : UIViewController

- (IBAction)openMoodle:(id)sender;

@end
