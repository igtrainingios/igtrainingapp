//
//  igUtility.h
//  igTraining
//
//  Created by Niharika on 10/03/15.
//  Copyright (c) 2015 Infogain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface igUtility : NSObject
+ (void)showOKAlertWithTitle:(NSString *)title message:(NSString *)message;

+ (void)showOKAlertWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate tag:(NSInteger)tag;


+   (void)enableButton:(UIButton *)button backgroundColor:(UIColor *)backColor;

+   (void)disableButton:(UIButton *)button;
+(NSString*)errorDescription:(NSError*)error;

+ (BOOL)isValidString:(NSString *)string;
+ (BOOL)isValidNumber:(NSNumber *)number;
+ (BOOL)isArray:(NSObject *)object;
+ (BOOL)isDictionary:(NSObject *)object;
+ (NSString *)stringFromDate:(NSDate *)date dateFormat:(NSString *)dateFormat;
+ (NSDate *)dateFromString:(NSString *)dateString dateFormat:(NSString *)dateFormat;
+ (void)roundCorner:(UIRectCorner)corner radius:(CGFloat)radius forView:(UIView *)view;
+ (NSString *)jsonStringFromObject:(id)object;
+ (void)setupButton:(UIButton *)button enable:(BOOL)enable backgroundColor:(UIColor *)backgroundColor;
+ (NSString *)bundleVersion;
+ (UIImage *)imageFromView:(UIView *)view;
+ (BOOL)openURLWithString:(NSString *)urlString;
+ (UIColor *)averageColorFromImage:(UIImage *)paramImage;

+ (void)setScreenName:(NSString*)screenName ClassName:(NSString*)className;
+ (void)setEventName:(NSString*)eventName  Parameters:(NSDictionary*)parameters;


@end
