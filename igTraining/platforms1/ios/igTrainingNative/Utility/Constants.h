//
//  Constants.h
//  igTraining
//
//  Created by Varun on 15/01/15.
//  Copyright (c) 2015 Infogain. All rights reserved.
//

#ifndef igTraining_Constants_h
#define igTraining_Constants_h

#define kAppDelegate                (AppDelegate *)[[UIApplication sharedApplication] delegate]


#define kAlertAndReturnIfNetworkNotReachable    if ([kAppDelegate networkReachabilityAlert] == false)\
{\
return;\
}

static NSString * const kLoginController_Identifier = @"loginViewController";
static NSString * const kCalendarViewController_Identifier = @"calendarViewController";
static NSString * const kRootViewController_Identifier = @"rootController";
static NSString * const kContentController_Identifier = @"contentController";
static NSString * const kSideMenuViewController_Identifier = @"menuController";
static NSString * const kTrainingViewController_Identifier = @"trainingViewController";
static NSString * const kFeedbackViewController_Identifier = @"IGTrainingFeedbackVC";

static NSString * const kIGServiceKeyEmployeeName = @"EmployeeName";
static NSString * const kIGServiceKeyEmployeeStaffID = @"EmployeeID";
static NSString * const kIGServiceKeyEmployeeMailID = @"mailID";
static NSString * const kIGServiceKeyEmployeeLocation = @"LocationID";

static NSString * const kIGServiceKeyEmployeeNewID = @"Emp_newID";
static NSString * const kIGServiceKeyDomainID = @"DomainId";
static NSString * const kIGServiceKeyUserName = @"UserName";
static NSString * const kIGServiceKeyPassword = @"Password";
static NSString * const kIGServiceKeyEmployeeImage = @"EmpImage";

static NSString * const kKeyUserDefaultsAcceptTerms = @"acceptTerms";
static NSString * const kKeyUserDefaultsFirstTimeLogin = @"firstTimeLogin";
static NSString * const kKeyUserDefaultsIsUserLoggedIn = @"isUserLoggedIn";

static NSString * const kUserDefaultImage = @"profile-avtaar-pic";
static NSString * const kStringLoginError = @"Login Error";
static NSString * const kStringNominateError = @"Error";


//Errors Constants...
static NSString * const kStringMessageExpectedParameterOfKindNSData = @"Expected parameter of kind NSData";
static NSString * const kStringMessageExpectedParameterOfKindNSArray = @"Expected parameter of kind NSArray";
static NSString * const kStringMessageNoDataToWrite = @"No data to write";
static NSString * const kStringMessageFileNameNotValid = @"File name not valid";
static NSString * const kStringMessageDirectoryNotRecognized = @"Directory not recognized";
static NSString * const kStringMessageDeleteOperationFailed = @"Delete operation failed";
static NSString * const kStringMessageWriteOperationFailed =  @"Write operation failed";
static NSString * const kStringMessageExpectedParameterOfKindNSDictionary = @"Expected parameter of kind NSDictionary";
static NSString * const kStringMessageErrorRemovingItemAtPath_Reason_ = @"Error removing Item at path: %@. Reason: %@";
static NSString * const kStringMessageDefaultProfilePicNotFound = @"Default profile pic not found";



#define kStringLogOut                                       @"Log Out"
#define kStringMessageYouNeedToAgreeToTermsAndConditions    @"You need to agree to the terms and conditions to use the application"
#define kStringMessageFileNotFoundInBundle_                 @"File not found in bundle: %@.html"


//screen size
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_LANDSCAPE  ([UIApplication sharedApplication].statusBarOrientation == UIDeviceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIDeviceOrientationLandscapeRight)

#define IS_IPAD_LANDSCAPE (IS_IPAD && IS_LANDSCAPE)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define IPHONE_5_LENGTH  568.0
#define IPHONE_6_LENGTH  667.0
#define IPHONE_6P_LENGTH 736.0
#define IPHONE_7_LENGTH  667.0
#define IPHONE_7P_LENGTH 736.0
#define IPAD_PRO_LENGTH 2732.0

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < IPHONE_5_LENGTH)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == IPHONE_5_LENGTH)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == IPHONE_6_LENGTH)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == IPHONE_6P_LENGTH)
#define IS_IPHONE_7 (IS_IPHONE && SCREEN_MAX_LENGTH == IPHONE_7_LENGTH)
#define IS_IPHONE_7P (IS_IPHONE && SCREEN_MAX_LENGTH == IPHONE_7P_LENGTH)
#define IS_IPADPRO_12I (IS_IPAD && SCREEN_MAX_LENGTH == IPAD_PRO_LENGTH)

//web services key- training calender

#define kTrainingID     @"ID"
#define kTrainingTopic  @"Topic"
#define kNominate       @"nominate"
#define kTrainingMode   @"TrainingMode"
#define kPrerequisite   @"Prerequisite"
#define kTrainingOccurance     @"TrainingOccurance"
#define kStartDate @"date"
#define kEndDate @"Enddate"
#define kTrainingTime @"time"
#define kTrainingVenue @"venue"
#define kTrainerName @"EMP_NAME"
#define kDateFlag @"DateFlag"
#define kStartDateTime @"StartdateTime"
#define kEndDateTime @"EnddateTime"
#define kSD @"SD"
#define kLocation @"Location"
#define kNominationStatus       @"NominationStatus"


#define kFeedbackTrainingID   @"TrainingId"
#define kFeedbackTrainingTopic  @"Topic"
#define kFeedbackTrainingStartDate  @"StartDate"
#define kFeedbackTrainingEndDate    @"EndDate"
#define kFeedbackTrainingTrainerName @"Trainer"
#define kFeedbackTrainingTrainerID  @"TrainerEmpID"


//Feedback Templetes
#define kFeedbacks  @"Feedbacks"
#define kTrainerFeedback  @"TrainerFeedbacks"
#define kFeedbackTemplete1  @"FeedBackTemplate1"
#define kFeedbackTemplete2  @"FeedBackTemplate2"
#define kFeedbackTemplete3  @"FeedBackTemplate3"
#define kFeedbackTemplete4  @"FeedBackTemplate4"
#define kFeedbackTemplete5  @"FeedBackTemplate5"


#define kMappingId @"MappingId"
#define kCategoryId @"CategoryId"
#define kSubCategoryId @"SubCategoryId"
#define kFeedbackType   @"FeedbackType"
#define kFeedbackSubType   @"FeedbackSubType"
#define kFlag   @"Flag"
#define kTrainerID   @"TrainerID"
#define kFacultyName   @"TrainerName"


#define kAppDelegate                (AppDelegate *)[[UIApplication sharedApplication] delegate]



#define kEmployeeID @"EmployeeID"
#define kStringError                        @"Error"

#define kStringMessageThankYouForYourValuebleFeedback       @"Thank you for valuable feedback"

#define kDefaultBarTintColor        [UIColor colorWithRed:(29.0f/255.0f) green:(151.0f/255.0f) blue:(242.0f/255.0f) alpha:1.0f]

#define kImageScreenBG                      @"main_bg"


//web service URLS
//  @"http://172.18.65.154:8081/api/login/login"
#define kLoginUrl @"https:/trainingapi.infogain.com/api/Login/Login"
//#define kLoginUrl @"http://172.18.65.154:8081/api/login/login"
#define kNominateUrl @"https:/trainingapi.infogain.com/api/Training/Nominate"
#define kCancelTrainingUrl @"https:/trainingapi.infogain.com/api/Training/CancelNomination"
#define kFeedbackTrainingsUrl @"https:/trainingapi.infogain.com/api/Training/GetFeedbackTrainingsDetails"
#define kFeedbackRatingURL @"https:/trainingapi.infogain.com/api/Training/GetFeedbackTemplate"
#define kTrainingFeedbackSubmission @"https:/trainingapi.infogain.com/api/Training/FeedbackSubmission"
/*#define kFeedbackTrainingsUrl @"http:/172.18.65.154:8081/api/Training/GetFeedbackTrainingsDetails"
#define kFeedbackRatingURL @"http:/172.18.65.154:8081/api/Training/GetFeedbackTemplate"
#define kTrainingFeedbackSubmission @"http:/172.18.65.154:8081/api/Training/FeedbackSubmission"*/

#define kTrainingUrl @"https:/trainingapi.infogain.com/api/Training/GetTrainingsForCalendar"

#define kColorSigninGreen           [UIColor colorWithRed:(48.0/255.0f) green:(169.0/255.0f) blue:(116.0/255.0f) alpha:1.0f]
#define kColorButtonShadow          [UIColor colorWithRed:(138.0/255.0f) green:(202.0/255.0f) blue:(242.0/255.0f) alpha:1.0f]



#pragma mark - error messages
#define NO_INTERNET_CONNECTION_ERROR @"Please check your internet connectivity and try again."
#define TIME_OUT_ERROR @"Service time out .Please try again."
#define ERROR_IN_FETCHING_DATA @"Error in Fetching Data"
#define  kLoginFail @"1000"
#define  WRONG_LOGIN_CREDENTIALS @"Username or Password is not correct"

#endif
