//
//  igTraining
//
//  Created by Upakul on 15/01/15.
//  Copyright (c) 2015 Infogain. All rights reserved.
//

#import "igTBouncingView.h"

@implementation igTBouncingView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initSuccessCircleWithFrame:(CGRect)frame andImageSize:(int) imageSize andColor:(UIColor*) color
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = color;
        [self.layer setCornerRadius:30.0f];
    }
    return self;
}

- (CGRect) newFrameWithWidth:(float) width andHeight:(float) height
{
    return CGRectMake(self.frame.origin.x + ((self.frame.size.width - width)/2),
                      self.frame.origin.y + ((self.frame.size.height - height)/2),
                      width,
                      height);
}


@end

