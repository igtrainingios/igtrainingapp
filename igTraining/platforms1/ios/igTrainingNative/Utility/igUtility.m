//
//  igUtility.m
//  igTraining
//
//  Created by Niharika on 10/03/15.
//  Copyright (c) 2015 Infogain. All rights reserved.
//

#import "igUtility.h"
#import "igTSmoothAlertView.h"
#import "Constants.h"
#import <FirebaseAnalytics/FirebaseAnalytics.h>

#define kDefaultBarTintColor        [UIColor colorWithRed:(29.0f/255.0f) green:(151.0f/225.0f) blue:(242.0f/225.0f) alpha:1.0f]
#define kColorButtonGray           [UIColor colorWithRed:(221.0/255.0f) green:(218.0/255.0f) blue:(218.0/255.0f) alpha:1.0f]
#define kColorButtonFontColor      [UIColor colorWithRed:(139.0/255.0f) green:(139.0/255.0f) blue:(139.0/255.0f) alpha:1.0f]


@implementation igUtility

+ (void)showOKAlertWithTitle:(NSString *)title message:(NSString *)message
{
    //    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
    //                                                        message:message
    //                                                       delegate:nil
    //                                              cancelButtonTitle:kStringOK
    //                                              otherButtonTitles:nil];
    //    [alertView show];
    
    igTSmoothAlertView *alert = [[igTSmoothAlertView alloc] initDropAlertWithTitle:title
                                                                           andText:message
                                                                   andCancelButton:NO
                                                                          andColor:kDefaultBarTintColor
                                                                          andImage:[UIImage imageNamed:@"infogain-logo-circle"]];
    [alert show];
    
}

+ (void)showOKAlertWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate tag:(NSInteger)tag
{
    igTSmoothAlertView *alert = [[igTSmoothAlertView alloc] initDropAlertWithTitle:title
                                                                           andText:message
                                                                   andCancelButton:NO
                                                                          andColor:kDefaultBarTintColor
                                                                          andImage:[UIImage imageNamed:@"infogain-logo-circle"]];
    alert.tag = tag;
    alert.delegate = delegate;
    [alert show];
    
}


+   (void)enableButton:(UIButton *)button backgroundColor:(UIColor *)backColor
{
    button.enabled = YES;
    button.backgroundColor = backColor;
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

+   (void)disableButton:(UIButton *)button
{
    button.enabled = NO;
    button.backgroundColor = kColorButtonGray;
    [button setTitleColor:kColorButtonFontColor forState:UIControlStateNormal];
}


+(NSString*)errorDescription:(NSError*)error{
    if (([error code] == kCFURLErrorCannotConnectToHost) || ([error code] == kCFURLErrorTimedOut) || ([error code] == kCFURLErrorBadURL) || ([error code] == kCFURLErrorUnsupportedURL) || ([error code] == kCFURLErrorCannotFindHost) || [error code] == kCFURLErrorNotConnectedToInternet || [error code] == kCFURLErrorNetworkConnectionLost) {
        if ([error code]== kCFURLErrorNotConnectedToInternet) {
            return NO_INTERNET_CONNECTION_ERROR ;
        }
        else if ([error code] == (kCFURLErrorCannotConnectToHost | kCFURLErrorTimedOut)){
            return TIME_OUT_ERROR;
        }
            }
    else if([error code] ==[kLoginFail integerValue])
        return WRONG_LOGIN_CREDENTIALS;

    return ERROR_IN_FETCHING_DATA;
}


+ (BOOL)isValidString:(NSString *)string
{
    if (string == nil)
    {
        return NO;
    }
    
    if ([string respondsToSelector:@selector(length)] && string.length == 0)
    {
        return NO;
    }
    
    if ([string respondsToSelector:@selector(stringByTrimmingCharactersInSet:)])
    {
        if ([string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0)
        {
            return NO;
        }
    }
    
    if ([string respondsToSelector:@selector(isEqualToString:)])
    {
        if ([string isEqualToString:@"(null)"] ||
            [string isEqualToString:@"<null>"])
        {
            return NO;
        }
    }
    else
    {
        return NO;
    }
    
    return YES;
}

+ (BOOL)isValidNumber:(NSNumber *)number
{
    if (number == nil)
    {
        return NO;
    }
    
    if ([number isKindOfClass:[NSNumber class]] == NO)
    {
        return NO;
    }
    
    return YES;
}

+ (BOOL)isArray:(NSObject *)object
{
    if (object != nil)
    {
        if ([object isKindOfClass:[NSArray class]] || [object respondsToSelector:@selector(objectAtIndex:)])
        {
            return YES;
        }
    }
    
    return NO;
}

+ (BOOL)isDictionary:(NSObject *)object
{
    if (object != nil)
    {
        if ([object isKindOfClass:[NSDictionary class]] || [object respondsToSelector:@selector(objectForKey:)])
        {
            return YES;
        }
    }
    
    return NO;
}

+ (NSString *)stringFromDate:(NSDate *)date dateFormat:(NSString *)dateFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = dateFormat;
    dateFormatter.timeZone = [NSTimeZone defaultTimeZone];
    return [dateFormatter stringFromDate:date];
}

+ (NSDate *)dateFromString:(NSString *)dateString dateFormat:(NSString *)dateFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = dateFormat;
    dateFormatter.timeZone = [NSTimeZone systemTimeZone];
    return [dateFormatter dateFromString:dateString];
}


+ (void)roundCorner:(UIRectCorner)corner radius:(CGFloat)radius forView:(UIView *)view
{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds
                                                   byRoundingCorners:corner
                                                         cornerRadii:CGSizeMake(radius, radius)];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = view.bounds;
    maskLayer.path = maskPath.CGPath;
    view.layer.mask = maskLayer;
}

+ (NSString *)jsonStringFromObject:(id)object
{
    if (object)
    {
        NSData *data = [NSJSONSerialization dataWithJSONObject:object options:NSJSONWritingPrettyPrinted error:nil];
        if (data)
        {
            NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            if (jsonString)
            {
                return jsonString;
            }
        }
    }
    
    return @"";
}

+ (void)setupButton:(UIButton *)button enable:(BOOL)enable backgroundColor:(UIColor *)backgroundColor;
{
    button.enabled = enable;
    
    if (enable)
    {
        button.backgroundColor = backgroundColor;
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    else
    {
        button.backgroundColor = kColorButtonGray;
        [button setTitleColor:kColorButtonFontColor forState:UIControlStateNormal];
    }
}

+ (NSString *)bundleVersion
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
}

+ (UIImage *)imageFromView:(UIView *)view
{
    UIImage *image = nil;
    UIGraphicsBeginImageContext(view.bounds.size);
    BOOL success = [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:NO];
    if (success) {
        image = UIGraphicsGetImageFromCurrentImageContext();
    }
    UIGraphicsEndImageContext();
    return image;
}

+ (BOOL)openURLWithString:(NSString *)urlString
{
    if ([self isValidString:urlString])
    {
        NSURL *url = [NSURL URLWithString:urlString];
        
        if ([[UIApplication sharedApplication] canOpenURL:url])
        {
            [[UIApplication sharedApplication] openURL:url];
            return YES;
        }
        else
        {
            NSLog(@"Cannot not open URL %@", url);
        }
    }
    else
    {
        NSLog(@"%s, URL String not valid %@", __FUNCTION__, urlString);
    }
    
    return NO;
}

+ (UIColor *)averageColorFromImage:(UIImage *)paramImage
{
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char rgba[4];
    CGContextRef context = CGBitmapContextCreate(rgba, 1, 1, 8, 4, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    
    CGContextDrawImage(context, CGRectMake(0, 0, 1, 1), paramImage.CGImage);
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    
    if(rgba[3] > 0) {
        CGFloat alpha = ((CGFloat)rgba[3])/255.0;
        CGFloat multiplier = alpha/255.0;
        return [UIColor colorWithRed:((CGFloat)rgba[0])*multiplier
                               green:((CGFloat)rgba[1])*multiplier
                                blue:((CGFloat)rgba[2])*multiplier
                               alpha:alpha];
    }
    else {
        return [UIColor colorWithRed:((CGFloat)rgba[0])/255.0
                               green:((CGFloat)rgba[1])/255.0
                                blue:((CGFloat)rgba[2])/255.0
                               alpha:((CGFloat)rgba[3])/255.0];
    }
}


+ (void)setScreenName:(NSString*)screenName ClassName:(NSString*)className
{
    [FIRAnalytics setScreenName:screenName screenClass:className];
}

+ (void)setEventName:(NSString*)eventName  Parameters:(NSDictionary*)parameters
{
    [FIRAnalytics logEventWithName:eventName
                        parameters:parameters];
}


@end
