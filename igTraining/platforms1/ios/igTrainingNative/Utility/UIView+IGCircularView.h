//
//  UIView+IGCircularView.h
//  IGCafe
//
//  Created by Pulkit on 26/05/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (IGCircularView)

- (void) IGMakeCircularView;

- (void)roundViewWithCornerRaidus:(CGFloat)cornderRadius
                      borderWidth:(CGFloat)borderWidth
                      borderColor:(UIColor *)borderColor;

- (void)roundViewByRoundingCorners:(UIRectCorner)rectCorners
                       cornerRadii:(CGSize)cornerRadii;

@end
