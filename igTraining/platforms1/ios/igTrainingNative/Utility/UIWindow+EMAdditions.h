//
//  igTraining
//
//  Created by Upakul on 15/01/15.
//  Copyright (c) 2015 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWindow (EMAdditions)

- (UIImage *)emSnapshot;
- (UIViewController *)em_viewControllerForStatusBarStyle;
- (UIViewController *)em_viewControllerForStatusBarHidden;

@end
